defmodule Em.MixProject do
  use Mix.Project

  def project do
    [
      app: :em,
      version: "0.6.0",
      elixir: "~> 1.10",
      dialyzer: [
        plt_file: {:no_warn, "priv/plts/dialyzer.plt"}
      ],
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      releases: [
        prod: [
          include_executables_for: [:unix],
          steps: [:assemble, :tar],
          cookie: Base.url_encode64(:crypto.strong_rand_bytes(40))
        ]
      ]
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {EM.Application, []}
    ]
  end

  defp deps do
    [
      {:credo, "~> 1.5", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:jason, "~> 1.2"},
      {:libcluster, "~> 3.2"},
      {:syn, "~> 2.1"},
      {:git_hooks, "~> 0.5.2", only: [:test, :dev], runtime: false},
      {:ex_doc, "~> 0.24", only: :dev, runtime: false}
    ]
  end
end
