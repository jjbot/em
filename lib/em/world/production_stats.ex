defmodule EM.World.ProductionStats do
  @moduledoc """
  Module to help track how much stuff gets produced in the factory.
  """

  use GenServer

  alias __MODULE__, as: ProductionStats

  @impl GenServer
  def init(_) do
    {:ok, %{}}
  end

  def add(payload) do
    GenServer.cast(__MODULE__, {:add, payload})
  end

  def start_link(_) do
    GenServer.start_link(__MODULE__, nil, name: ProductionStats)
  end

  @impl GenServer
  def handle_cast({:add, {type, amount}}, state) do
    {:noreply, update_in(state[type], fn old -> (old || 0) + amount end)}
  end
end
