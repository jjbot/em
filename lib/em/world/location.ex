defmodule EM.World.Location do
  @moduledoc """
  All locations in EM are on a `{x, y}` grid. This module helps with dealing
  with these locations.
  """
  alias EM.Machines.Machine
  alias EM.World.Orientation

  @tile_size Application.compile_env!(:em, :tile_size)

  @typedoc """
  Tuple with two integers representing the x and y positions of a location.
  """
  @type t :: {integer(), integer()}

  @doc """
  Add two location together.

  ## Examples

      iex> Location.add({1, 2}, {1, 1})
      {2, 3}

  """
  @spec add(t(), t()) :: t()
  def add(location1, location2)

  def add({x1, y1}, {x2, y2}) do
    {x1 + x2, y1 + y2}
  end

  @doc """
  Generate all locations that are covered by a machine of size `size` placed on
  `Location`.

  ## Examples

      iex> Location.gen_locations({5, 5}, {3, 3})
      [{5, 5}, {5, 6}, {5, 7}, {6, 5}, {6, 6}, {6, 7}, {7, 5}, {7, 6}, {7, 7}]

  """
  @spec gen_locations(t(), {pos_integer(), pos_integer()}) :: [t()]
  def gen_locations(location, size)

  def gen_locations({lx, ly}, {dx, dy}) do
    for x <- 0..(dx - 1), y <- 0..(dy - 1), do: {lx + x, ly + y}
  end

  @doc """
  Get the bottom left tile corner of the provided `Location`.

  This is useful to determine which `EM.World.Tile` should host a machine.

  ## Example

  Here, tile_size = {10, 10}
      iex> Location.get_tile_location({15, 21})
      {10, 20}

  """
  @spec get_tile_location(t()) :: t()
  def get_tile_location(location)

  def get_tile_location({lx, ly}) do
    {x_size, y_size} = @tile_size
    bl_x = div(lx, x_size) * x_size
    bl_y = div(ly, y_size) * y_size
    {bl_x, bl_y}
  end

  @doc """
  Get the tiles around a machine that it can potentially interact with.

  This comes down to all direct neighbours of the tiles the machine occupies,
  except for the corner pieces.
  """
  @deprecated "No longer used as machines might interact over longer distances"
  @spec get_area_neighbour_locations(t(), {pos_integer(), pos_integer()}) :: [t()]
  def get_area_neighbour_locations(location, machine_size)

  def get_area_neighbour_locations({x, y}, {mx, my}) do
    left = for dy <- 0..(my - 1), do: {x - 1, y + dy}
    right = for dy <- 0..(my - 1), do: {x + mx, y + dy}
    bottom = for dx <- 0..(mx - 1), do: {x + dx, y - 1}
    top = for dx <- 0..(mx - 1), do: {x + dx, y + my}
    top ++ right ++ bottom ++ left
  end

  @doc """
  Find the bottom left of a list of locations.

  Assumes the list of locations are all enclosed in a rectangle.

  ## Example
      iex> Location.get_bottom_left([{2, 2}, {2, 3}, {3, 2}, {3, 3}])
      {2, 2}

  """
  @spec get_bottom_left([t()]) :: t()
  def get_bottom_left(locations) do
    Enum.min(locations)
  end

  @doc """
  Get the neighbour of a location given the (compass) direction given by
  `EM.World.Orientation`.

  ## Example
      iex> Location.get_neighbour_location({1, 1}, :north)
      {1, 2}

      iex> Location.get_neighbour_location({8, 5}, :east)
      {9, 5}

  """
  @spec get_neighbour_location(t(), Orientation.t()) :: t()
  def get_neighbour_location(location, orientation)

  def get_neighbour_location({x, y}, orientation) do
    case orientation do
      :north -> {x, y + 1}
      :east -> {x + 1, y}
      :south -> {x, y - 1}
      :west -> {x - 1, y}
    end
  end

  # FIXME: doesn't this do the exact same as get_tile_location?
  @spec normalise(t()) :: t()
  def normalise({x, y}) do
    {dx, dy} = @tile_size
    {normalise_axis(x, dx), normalise_axis(y, dy)}
  end

  @doc """
  Rotate a `machine` at `location` 90 degrees clockwise.
  """
  @spec rotate(t(), any()) :: t()
  def rotate(location, machine)

  def rotate({x, y}, machine) do
    {sx, _sy} = Machine.size(machine)
    {y, -x - sx}
  end

  @spec normalise_axis(integer(), pos_integer()) :: integer()
  defp normalise_axis(loc, size) do
    if loc < 0 do
      loc - rem(loc, size) - size
    else
      loc - rem(loc, size)
    end
  end
end
