defmodule EM.World.MachineMeta do
  @moduledoc """
  Struct to track metadata of a machine.
  """

  alias __MODULE__, as: MachineMeta
  alias EM.World.Location

  @enforce_keys [:name, :module, :locations]
  defstruct [:name, :module, :locations]

  @type t :: %MachineMeta{
          name: GenServer.name(),
          module: module(),
          locations: [Location.t()]
        }
end
