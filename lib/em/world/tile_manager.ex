defmodule EM.World.TileManager do
  @moduledoc """
  Module to help with Tile management.
  """
  alias __MODULE__, as: TileManager
  alias EM.World.{Location, Tile, TileServer}

  use GenServer

  require Logger

  defstruct tiles: %{}

  @doc """
  Get the :via tuple for the current node.
  """
  @spec name() :: GenServer.name()
  def name() do
    name(:erlang.node())
  end

  @doc """
  Get the :via tuple for the provided `node`.
  """
  @spec name(atom()) :: GenServer.name()
  def name(node) do
    {:via, :syn, {TileManager, node}}
  end

  @impl GenServer
  def init(_) do
    {:ok, %TileManager{}}
  end

  def start_link(_) do
    GenServer.start_link(__MODULE__, nil, name: name())
  end

  @doc """
    Create a `Tile` for the specified `location`.

    This function assumes the location has already been normalised.
  """
  @spec create_tile(Location.t()) :: GenServer.name()
  def create_tile(location) do
    GenServer.call(TileManager, {:create_tile, location})
  end

  @doc """
  Get the amount of tiles the current node is managing.
  """
  @spec get_tile_count() :: non_neg_integer()
  def get_tile_count() do
    GenServer.call(TileManager, :get_tile_count)
  end

  @deprecated "Use Tile.get_name/1 instead"
  @doc """
  Get the tile name for the provided `location`.
  """
  @spec get_tile_name(Location.t()) :: GenServer.name()
  def get_tile_name(location) do
    {:via, :syn, {:tile, location}}
  end

  @doc """
  Lookup the pid of the tile that is responsible for `location`.
  """
  def lookup(location) do
    case :syn.whereis({:tile, Location.normalise(location)}) do
      pid when is_pid(pid) -> {:ok, pid}
      :undefined -> {:error, :undefined}
    end
  end

  @impl GenServer
  def handle_call({:create_tile, location}, _from, %TileManager{tiles: tiles} = tile_manager) do
    # FIXME: Wouldn't it make more sense to ask the Registry being used
    # whethere there is already a process with the generated name?
    name = get_tile_name(location)

    case DynamicSupervisor.start_child(
           TileSupervisor,
           {TileServer, {%Tile{bl: location}, name}}
         ) do
      {:ok, _pid} ->
        :ok

      {:error, _} ->
        Logger.debug("Tile for location #{inspect(location)} has already been started")
    end

    {:reply, name, %{tile_manager | tiles: Map.put(tiles, location, name)}}
  end

  def handle_call(:get_tile_count, _from, tile_manager) do
    {:reply, Kernel.map_size(tile_manager.tiles), tile_manager}
  end
end
