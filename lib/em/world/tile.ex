defmodule EM.World.Tile do
  @moduledoc """
  Tiles are fixed size pieces of game world.

  Tiles allow the game world to be distributed across multiple nodes, as each can
  work completely independently.

  Current tile responsibilities:
  - keeping track of cells (single locations) and whether they are occupied
  - allow placing of machines, making sure they don't overlap
  - allow locations to be reserved so a machine can be placed on them, this is
    needed when a machine spans multiple tiles, and we need to make sure the cells
    across all tiles are  available.
  - allow machines to register locations which they want to observe: the machines
    will be notified when another machine is placed on one of those observed
    locations
  - communicate with neighbouring tiles when machines span across multiple tiles

  This module deals with all the state modification. There is a sibling module
  `EM.World.TileServer` that manages the external API.
  """
  alias __MODULE__, as: Tile
  alias EM.Machines.{Machine, MachineServer}
  alias EM.World
  alias EM.World.{Location, MachineMeta, TileServer}

  require Logger

  @tile_size Application.compile_env!(:em, :tile_size)

  @enforce_keys [:bl]
  defstruct machines: %{},
            locations: %{},
            monitors: %{},
            reservations: %{},
            bl: nil,
            size: @tile_size,
            machine_supervisor: nil,
            names: %{},
            references: %{}

  @type machine_name :: GenServer.name()

  @type t :: %Tile{
          machines: %{pid() => MachineMeta.t()},
          locations: %{Location.t() => {module(), pid()}},
          monitors: %{Location.t() => [GenServer.name()]},
          reservations: %{Location.t() => GenServer.name()},
          bl: Location.t() | nil,
          size: {integer(), integer()},
          machine_supervisor: {nil, Supervisor.supervisor()},
          names: %{{atom(), Location.t()} => {pid(), reference()}},
          references: %{reference() => {atom(), Location.t()}}
        }

  @doc """
  Add `machine` to `tile` on `location`.
  """
  @spec add_machine(t(), struct(), Location.t()) ::
          {{:ok, pid, {atom(), Location.t()}}, t()} | {{:error, atom()}, t()}
  def add_machine(tile, machine, location) do
    if area_available?(tile, location, Machine.size(machine)) do
      {:ok, tile, machine_pid, machine_name} = add_machine_impl(tile, machine, location)
      {{:ok, machine_pid, machine_name}, tile}
    else
      {{:error, :position_taken}, tile}
    end
  end

  @doc """
  Add a monitor for location to Tile.
  """
  def add_monitor(tile, location, ref) do
    case get_machine_at(tile, location) do
      {:ok, machine} ->
        GenServer.cast(self(), {:notify_location_taken, ref, location, machine})

      _ ->
        :ok
    end

    case Map.get(tile.monitors, location) do
      nil ->
        put_in(tile, [Access.key!(:monitors), location], [ref])

      ref_list ->
        # Don't duplicate monitors for the same process
        if ref in ref_list do
          tile
        else
          %{tile | monitors: Map.put(tile.monitors, location, [ref | ref_list])}
        end
    end
  end

  @spec convert_reservations(t(), [Location.t()], GenServer.name(), any(), module(), any()) :: t()
  def convert_reservations(tile, locations, machine_ref, machine_name, machine_mod, claim_ref) do
    tile = free_reservations!(tile, locations, claim_ref)

    tile_locations =
      Enum.reduce(locations, tile.locations, fn loc, acc ->
        Map.put(acc, loc, {machine_mod, machine_ref})
      end)

    machines = Map.put(tile.machines, machine_ref, {machine_mod, locations})
    monitor_ref = Process.monitor(machine_ref)

    names =
      Map.put(
        tile.names,
        machine_name,
        {machine_ref, monitor_ref}
      )

    references =
      Map.put(
        tile.references,
        monitor_ref,
        machine_name
      )

    %{tile | locations: tile_locations, machines: machines, names: names, references: references}
  end

  @spec reserve_locations(t(), [Location.t()], GenServer.name()) :: {:ok, t()} | {:error, atom()}
  def reserve_locations(tile, locations, reference) do
    if locations_available?(tile, locations) do
      reservations =
        Enum.reduce(
          locations,
          tile.reservations,
          fn loc, acc -> Map.put(acc, loc, reference) end
        )

      {:ok, %{tile | reservations: reservations}}
    else
      {:error, :not_available}
    end
  end

  @doc """
  Remove the provided locations from the location reservation list. Checked by
  matching against `reference`.
  """
  @spec free_reservations(t(), [Location.t()], any()) :: {:ok, t()} | {:error, atom()}
  def free_reservations(tile, locations, reference) do
    # FIXME: doesn't check if the removal is exhaustive. Should this just be done
    # by reference?
    if check_reservations(tile.reservations, locations, reference) do
      reservations =
        locations
        |> Enum.reduce(
          tile.reservations,
          fn loc, acc -> Map.delete(acc, loc) end
        )

      {:ok, %{tile | reservations: reservations}}
    else
      {:error, :reservations_dont_align}
    end
  end

  def free_reservations!(tile, locations, reference) do
    {:ok, tile} = free_reservations(tile, locations, reference)
    tile
  end

  # FIXME: probably should be private
  @doc """
  Check if all provided `locations` are held by `reference`.
  """
  @spec check_reservations(%{Location.t() => any()}, [Location.t()], any()) :: boolean()
  def check_reservations(reservations, locations, reference) do
    locations
    |> Enum.map(fn loc -> reservations[loc] == reference end)
    |> Enum.all?()
  end

  @spec remove_monitor(t(), Location.t(), any()) :: t()
  def remove_monitor(tile, location, ref) do
    monitors =
      Map.update(
        tile.monitors,
        location,
        [],
        fn ref_list -> Enum.reject(ref_list, &(&1 == ref)) end
      )

    %{tile | monitors: monitors}
  end

  @doc """
  Check if the required area is available.
  """
  @spec area_available?(t(), Location.t(), {integer(), integer()}) :: boolean()
  def area_available?(tile, location, size) do
    locations_available?(
      tile,
      Location.gen_locations(location, size)
    )
  end

  @doc """
  Check if `locations` are unoccupied in `tile`.
  """
  @spec locations_available?(t(), [Location.t()]) :: boolean()
  def locations_available?(tile, locations) do
    locations
    |> Enum.all?(fn loc -> position_available?(tile, loc) end)
  end

  @spec get_machine_at(t(), Location.t()) :: {:ok, pid()} | {:error, atom()}
  def get_machine_at(%Tile{locations: locations} = tile, location) do
    if location_is_in_tile?(tile, location) do
      case Map.get(locations, location) do
        {_module, pid} when is_pid(pid) -> {:ok, pid}
        _ -> {:error, :undefined}
      end
    else
      case World.get_tile_at(location) do
        {:ok, pid} ->
          TileServer.get_machine_at(pid, location)

        _ ->
          {:error, :undefined}
      end
    end
  end

  def get_occupied_locations_map(tile) do
    tile.machines
    |> Enum.flat_map(fn {pid, {_type, locations}} ->
      status = MachineServer.describe(pid)
      Enum.map(locations, fn loc -> {loc, status} end)
    end)
    |> Enum.into(%{})
  end

  # FIXME: what kind of output format do we expect here? A map from location to machine?
  def get_machines(tile) do
    get_machines(tile, covered_locations(tile))
  end

  @spec get_machines(t(), [Location.t()]) :: %{}
  def get_machines(tile, locations) do
    locations
    |> Enum.map(fn location -> {location, tile.locations[location]} end)
    |> Enum.group_by(fn {_loc, ref} -> ref end, fn {loc, _ref} -> loc end)
  end

  @spec get_machine_neighbours(t(), pid()) :: [{module(), pid()}]
  def get_machine_neighbours(tile, machine_pid) do
    {_machine_type, machine_locations} = tile.machines[machine_pid]

    machine_locations
    |> Location.get_bottom_left()
    |> Location.get_area_neighbour_locations(MachineServer.size(machine_pid))
    |> Enum.map(fn location -> {location, Tile.get_machine_at(tile, location)} end)
    |> Enum.filter(fn {_, location} -> location != nil end)
    |> Enum.reject(fn {_, {status, _}} -> status == :error end)
    |> Enum.uniq()
  end

  def get_name(location) do
    {:via, :syn, {:tile, location}}
  end

  @doc """
  Check if the specified location is available.
  """
  @spec position_available?(t(), Location.t()) :: boolean()
  def position_available?(tile, location) do
    Map.get(tile.locations, location) == nil && Map.get(tile.reservations, location) == nil
  end

  @doc """
  Check if a machine has been removed, if not, do so.
  """
  @spec ensure_machine_removed(t(), pid()) :: t()
  def ensure_machine_removed(tile, machine_pid) do
    case Map.get(tile.machines, machine_pid) do
      {_, _} ->
        remove_machine(tile, machine_pid)

      nil ->
        tile
    end
  end

  @spec remove_machine(t(), pid()) :: {t(), map()}
  def remove_machine(tile, machine_pid) when is_pid(machine_pid) do
    # FIXME: would make sense to demonitor the process before executing this,
    # or to split this function in two: one that calls the MachineSupervisor
    # and one that takes care of updating the Tile state.

    # FIXME: Check if you are the primary parent of this machine
    # Could this be combined with the demonitoring and keep a structure that
    # keeps the ref as an indication this Tile is the primary parent of the
    # machine?

    MachineSupervisor.terminate_child(tile.machine_supervisor, machine_pid)
    remove_machine_state(tile, machine_pid)
  end

  # def remove_machine(tile, machine_ref) when is_reference(machine_ref) do
  # end

  def remove_machine(tile, machine_name) when is_tuple(machine_name) do
    {machine_pid, monitor} = tile.names[machine_name]
    Process.demonitor(monitor)
    MachineSupervisor.terminate_child(tile.machine_supervisor, machine_pid)
    remove_machine_state(tile, machine_pid)
  end

  def remove_machine_state(tile, machine_pid) do
    # FIXME: Should not be required once we keep track of which machines
    # the tile is responsible for (keep a pid -> ref (?) map)
    # Need this case statement to make sure we only try to remove state
    # when there is state to be removed: the parent of the machine currently
    # tries to remove the machine twice: once when the user request comes in,
    # and once when it figures out the process is gone.
    Logger.warn("Removing machine state for #{inspect(machine_pid)}")

    case tile.machines[machine_pid] do
      {_state_module, locations} ->
        cleaned_locations =
          locations
          |> Enum.reduce(
            tile.locations,
            fn loc2remove, locs -> Map.delete(locs, loc2remove) end
          )

        cleaned_machines = Map.delete(tile.machines, machine_pid)

        # {updated_tile, resources_to_return}
        {%{
           tile
           | machines: cleaned_machines,
             locations: cleaned_locations
         }, %{}}

      _ ->
        {tile, %{}}
    end
  end

  def handle_crashed_machine(tile, proc_ref) do
    # The new one
    machine_name = tile.references[proc_ref]
    Logger.warn("Need to handle crash for #{inspect(machine_name)}")

    case find_machine_replacement(machine_name, 1) do
      new_pid when is_pid(new_pid) ->
        Logger.warn("PID of replacement process: #{inspect(new_pid)}")
        new_monitor_ref = Process.monitor(new_pid)
        replace_machine(tile, machine_name, new_pid, new_monitor_ref)

      {:error, _reason} ->
        Logger.warn("No replacement found, giving up and cleaning up")
        {machine_pid, _machine_ref} = tile.names[machine_name]
        remove_machine_state(tile, machine_pid)
    end
  end

  defp find_machine_replacement(_, 5), do: {:error, :no_replacement_found}

  defp find_machine_replacement(name, count) do
    Logger.warn("Searching machine replacement, attempt #{inspect(count)} / 5")
    :timer.sleep(50 * count)

    case :syn.whereis(name) do
      pid when is_pid(pid) ->
        pid

      _ ->
        find_machine_replacement(name, count + 1)
    end
  end

  defp replace_machine(tile, machine_name, new_pid, new_monitor) do
    {old_pid, old_monitor} = tile.names[machine_name]

    # update machines
    {meta, tile} = pop_in(tile.machines[old_pid])
    {module, locations} = meta
    tile = put_in(tile.machines[new_pid], meta)

    # update locations
    tile =
      Enum.reduce(
        locations,
        tile,
        fn loc, t -> put_in(t.locations[loc], {module, new_pid}) end
      )

    # update names
    tile = put_in(tile.names[machine_name], {new_pid, new_monitor})

    # update reference
    {_, tile} = pop_in(tile.references[old_monitor])
    put_in(tile.references[new_monitor], machine_name)
  end

  defp reserve_locations(locations, tile_name) do
    locations_reserved =
      locations
      |> World.get_tile_location_mapping()
      |> Enum.reject(fn {ref, _locs} -> ref == tile_name end)
      |> Enum.map(fn {ref, locs} ->
        Task.Supervisor.async(
          TaskSupervisor,
          fn -> {TileServer.reserve_locations(ref, locs, tile_name), ref, locs} end
        )
      end)
      |> Enum.map(fn t -> Task.await(t) end)

    if Enum.all?(locations_reserved, fn {status, _, _} -> status == :ok end) do
      result = locations_reserved |> Enum.map(fn {_status, ref, locs} -> {ref, locs} end)
      {:ok, result}
    else
      locations_reserved
      |> Enum.map(fn {ref, locs} ->
        Task.Supervisor.async(
          TaskSupervisor,
          fn -> TileServer.free_reservations(ref, locs, tile_name) end
        )
      end)
      # Note: using Enum.each (not map) here: we're in it for the side-effects
      # not the returned values. If free_reservations ever changes and starts
      # returning values, this needs to be changed. One reason for this would be
      # to check if any reservations failed to be released.
      |> Enum.each(fn t -> Task.await(t) end)

      {:error, :locations_not_available}
    end
  end

  defp add_machine_to_tile(
         tile,
         machine_pid,
         machine_name,
         monitor_ref,
         machine_state,
         machine_locations
       ) do
    state_module = Machine.state_module(machine_state)

    tile
    |> add_machine_locations(machine_locations, state_module, machine_pid)
    |> add_machine(state_module, machine_pid, machine_locations)
    |> add_machine_name(machine_name, machine_pid, monitor_ref)
    |> add_machine_reference(monitor_ref, machine_name)
  end

  @spec add_machine_locations(t(), [Location.t()], module(), pid()) :: t()
  defp add_machine_locations(tile, machine_locs, state_mod, machine_pid) do
    machine_locs
    |> Enum.filter(&location_is_in_tile?(tile, &1))
    |> Enum.reduce(tile, fn loc, tile -> put_in(tile.locations[loc], {state_mod, machine_pid}) end)
  end

  @spec add_machine(t(), module(), pid(), [Location.t()]) :: t()
  defp add_machine(tile, state_mod, machine_pid, machine_locs) do
    locs = Enum.filter(machine_locs, &location_is_in_tile?(tile, &1))
    put_in(tile.machines[machine_pid], {state_mod, locs})
  end

  @spec add_machine_name(t(), {:machine, Location.t()}, pid(), reference()) :: t()
  defp add_machine_name(tile, machine_name, machine_pid, monitor_ref) do
    put_in(tile.names[machine_name], {machine_pid, monitor_ref})
  end

  @spec add_machine_reference(t(), reference(), {:machine, Location.t()}) :: t()
  defp add_machine_reference(tile, monitor_ref, machine_name) do
    put_in(tile.references[monitor_ref], machine_name)
  end

  defp notify_observers(tile, machine_locations, machine_pid) do
    # Helper function to notify all machines that are observing locations in the
    # provided tile that a machine has been placed.
    machine_locations
    |> Enum.filter(&location_is_in_tile?(tile, &1))
    |> Enum.map(fn location -> {location, Map.get(tile.monitors, location)} end)
    |> Enum.reject(fn {_, ref} -> ref == nil end)
    |> Enum.flat_map(fn {loc, refs} -> Enum.map(refs, fn ref -> {loc, ref} end) end)
    |> Enum.map(fn {loc, ref} ->
      Task.Supervisor.async(
        TaskSupervisor,
        fn -> GenServer.cast(ref, {:process_location_claimed, loc, machine_pid}) end
      )
    end)
    |> Enum.map(fn t -> Task.await(t) end)
  end

  defp convert_remote_reservations(
         reservations,
         machine_pid,
         machine_name,
         machine_mod,
         claim_ref
       ) do
    reservations
    |> Enum.map(fn {ref, locs} ->
      Task.Supervisor.async(
        TaskSupervisor,
        fn ->
          TileServer.convert_reservations(
            ref,
            locs,
            machine_pid,
            machine_name,
            machine_mod,
            claim_ref
          )
        end
      )
    end)
    |> Enum.map(fn t -> Task.await(t) end)
  end

  defp add_machine_impl(tile, machine_state, location) do
    # Regardless of the machine, you should always be able to set the bottom left
    # location, as that is the main way of dealing with locations. Should this be
    # a setter function of machines?

    server_module = Machine.server_module(machine_state)
    size = Machine.size(machine_state)

    machine_state = %{
      machine_state
      | bl: location,
        backup_server: tile.machine_supervisor
    }

    machine_locations = Location.gen_locations(location, size)

    case reserve_locations(machine_locations, get_name(tile.bl)) do
      {:ok, location_reservations} ->
        # This would have to be replaced for the new supervisor
        # something like
        {:ok, machine_pid, machine_name} =
          MachineSupervisor.start_child(
            tile.machine_supervisor,
            {server_module, machine_state}
          )

        monitor_ref = Process.monitor(machine_pid)

        tile =
          add_machine_to_tile(
            tile,
            machine_pid,
            machine_name,
            monitor_ref,
            machine_state,
            machine_locations
          )

        convert_remote_reservations(
          location_reservations,
          machine_pid,
          machine_name,
          Machine.state_module(machine_state),
          get_name(tile.bl)
        )

        notify_observers(tile, machine_locations, machine_pid)

        {:ok, tile, machine_pid, machine_name}

      {:error, reason} ->
        {:error, reason}
    end
  end

  @spec covered_locations(t()) :: [Location.t()]
  defp covered_locations(tile) do
    {x, y} = tile.bl
    {sx, sy} = tile.size

    for dx <- 0..(sx - 1), dy <- 0..(sy - 1), do: {x + dx, y + dy}
  end

  defp location_is_in_tile?(%Tile{bl: {bl_x, bl_y}}, {lx, ly}) do
    {size_x, size_y} = @tile_size
    lx >= bl_x and lx < bl_x + size_x and ly >= bl_y and ly < bl_y + size_y
  end
end
