defmodule EM.World.TileServer do
  @moduledoc """
  Sibling module for `EM.World.Tile`, containing all the server code for dealing
  with Tiles.
  """
  use GenServer

  alias EM.Machines.{MachineServer, Payload}
  alias EM.World.{Location, Tile, TileServer}

  require Logger

  @tile_size Application.compile_env!(:em, :tile_size)

  @impl GenServer
  def init(%Tile{} = tile) do
    {:ok, machine_supervisor} = MachineSupervisor.start_link(strategy: :one_for_one)
    {:ok, %{tile | machine_supervisor: machine_supervisor}}
  end

  def start_link({%Tile{} = tile, name}) do
    GenServer.start_link(TileServer, tile, name: name)
  end

  @doc """
  Add a new machine to a tile.
  """
  @spec add_machine(pid() | GenServer.name(), struct(), Location.t()) ::
          {:ok, pid(), {atom(), Location.t()}} | {:error, atom()}
  def add_machine(tile_pid, machine, location) do
    GenServer.call(tile_pid, {:add_machine, machine, location})
  end

  @doc """
  Convert the location reservations into occupied cells.
  """
  @spec convert_reservations(GenServer.name(), [Location.t()], pid(), any(), module(), any()) ::
          :ok
  def convert_reservations(tile_ref, locations, machine, machine_name, machine_mod, claim_ref) do
    GenServer.call(
      tile_ref,
      {:convert_reservations, locations, machine, machine_name, machine_mod, claim_ref}
    )
  end

  @doc """
  Register interest for a location.
  """
  # @spec register_monitor(GenServer.name(), Location.t(), GenServer.name()) :: :ok
  def register_monitor(tile_ref, location, machine_ref) do
    GenServer.call(tile_ref, {:register_monitor, location, machine_ref})
  end

  @doc """
  Remove a monitor for one `location`.
  """
  @spec unregister_monitor(GenServer.name(), Location.t(), pid()) :: :ok
  def unregister_monitor(tile_ref, location, machine_ref) do
    GenServer.call(tile_ref, {:unregister_monitor, location, machine_ref})
  end

  @doc """
  Display the content of the tile on the command line.
  """
  @spec display(GenServer.name()) :: :ok
  def display(tile_pid) do
    GenServer.cast(tile_pid, :display)
  end

  @doc """
  Get the machine occupying the specified location.
  """
  @spec get_machine_at(pid(), Location.t()) :: {:ok, any()} | {:error, atom()}
  def get_machine_at(tile_pid, location) do
    GenServer.call(tile_pid, {:get_machine_at, location})
  end

  def get_machines(tile_ref) do
    GenServer.call(tile_ref, :get_machines)
  end

  @doc """
  Get the machines occupying the list of specified locations.
  """
  @spec get_machines(pid(), [Location.t()]) :: %{}
  def get_machines(tile_pid, locations) do
    GenServer.call(tile_pid, {:get_machines, locations})
  end

  @doc """
  Get a list of all the direct neighbours of a machine.

  Return value is a list of {module, pid} tuples.
  """
  @spec get_machine_neighbours(GenServer.name(), pid()) :: [{module(), pid()}]
  def get_machine_neighbours(tile_pid, machine_pid) do
    GenServer.call(tile_pid, {:get_machine_neighbours, machine_pid})
  end

  def get_occupied_locations_map(tile_pid) do
    GenServer.call(tile_pid, :get_occupied_location_map)
  end

  @doc """
  Check if all `locations` are available in this tile.
  """
  @spec locations_available?(GenServer.name(), [Location.t()]) :: boolean()
  def locations_available?(tile_ref, locations) do
    GenServer.call(tile_ref, {:locations_available?, locations})
  end

  @doc """
  Remove machine from tile by location or pid.
  """
  @spec remove_machine(pid(), Location.t() | pid() | {:machine, Location.t()}) ::
          {:ok, Payload.t()} | {:error, atom()}
  def remove_machine(tile_pid, location = {x, y}) when is_integer(x) and is_integer(y) do
    GenServer.call(tile_pid, {:remove_machine_by_loc, location})
  end

  def remove_machine(tile_pid, machine_pid) when is_pid(machine_pid) do
    GenServer.call(tile_pid, {:remove_machine_by_pid, machine_pid})
  end

  def remove_machine(tile_pid, machine_name) when is_tuple(machine_name) do
    GenServer.call(tile_pid, {:remove_machine_by_name, machine_name})
  end

  @doc """
  Remove the list of provided reservations. The reference needs to match the
  reference supplied when the reservations were created.
  """
  @spec free_reservations(GenServer.name(), [Location.t()], any()) :: :ok
  def free_reservations(tile_pid, locations, reference) do
    GenServer.call(tile_pid, {:free_reservations, locations, reference})
  end

  @doc """
  Reserve a number of locations so a machine can be placed on them later on.

  The reference is important here as it signifies ownership of the reservation.
  For now, reservations don't expire.
  """
  def reserve_locations(tile_pid, locations, reference) do
    GenServer.call(tile_pid, {:reserve_locations, locations, reference})
  end

  @impl GenServer
  def handle_call({:add_machine, machine, location}, _from, tile) do
    {response, tile} = Tile.add_machine(tile, machine, location)
    {:reply, response, tile}
  end

  def handle_call(
        {:convert_reservations, locations, machine_ref, machine_name, machine_mod, claim_ref},
        _from,
        tile
      ) do
    {:reply, :ok,
     Tile.convert_reservations(tile, locations, machine_ref, machine_name, machine_mod, claim_ref)}
  end

  def handle_call({:get_machine_at, location}, _from, tile) do
    {:reply, Tile.get_machine_at(tile, location), tile}
  end

  def handle_call(:get_machines, _from, tile) do
    machines = Tile.get_machines(tile)
    {:reply, machines, tile}
  end

  def handle_call({:get_machines, locations}, _from, tile) do
    machines = Tile.get_machines(tile, locations)
    {:reply, machines, tile}
  end

  def handle_call({:get_machine_neighbours, machine_pid}, _from, tile) do
    machines = Tile.get_machine_neighbours(tile, machine_pid)
    {:reply, machines, tile}
  end

  def handle_call(:get_occupied_location_map, _from, tile) do
    {:reply, Tile.get_occupied_locations_map(tile), tile}
  end

  def handle_call({:locations_available?, locations}, _from, tile) do
    all_available =
      locations
      |> Enum.map(&Tile.position_available?(tile, &1))
      |> Enum.all?()

    {:reply, all_available, tile}
  end

  def handle_call({:remove_machine_by_loc, location}, _from, tile) do
    case tile.locations[location] do
      nil ->
        {:reply, {:error, :cell_not_occupied}, tile}

      {_, machine_pid} when is_pid(machine_pid) ->
        {tile, to_return} = Tile.remove_machine(tile, machine_pid)
        {:reply, {:ok, to_return}, tile}
    end
  end

  def handle_call({:remove_machine_by_pid, machine_pid}, _from, tile) do
    case tile.machines[machine_pid] do
      nil ->
        {:reply, {:error, :no_such_process}, tile}

      {_, _} ->
        {tile, to_return} = Tile.remove_machine(tile, machine_pid)
        {:reply, {:ok, to_return}, tile}
    end
  end

  def handle_call({:remove_machine_by_name, machine_name}, _from, tile) do
    {tile, to_return} = Tile.remove_machine(tile, machine_name)
    {:reply, {:ok, to_return}, tile}
  end

  def handle_call({:free_reservations, locations, ref}, _from, tile) do
    case Tile.free_reservations(tile, locations, ref) do
      {:ok, tile} ->
        {:reply, :ok, tile}

      {:error, reason} ->
        {:reply, {:error, reason}, tile}
    end
  end

  def handle_call({:reserve_locations, locations, ref}, _from, tile) do
    case Tile.reserve_locations(tile, locations, ref) do
      {:ok, tile} ->
        {:reply, :ok, tile}

      {:error, :not_available} ->
        {:reply, {:error, :not_available}, tile}
    end
  end

  def handle_call({:register_monitor, location, machine_ref}, _from, tile) do
    tile = Tile.add_monitor(tile, location, machine_ref)
    {:reply, :ok, tile}
  end

  def handle_call({:unregister_monitor, location, machine_ref}, _from, tile) do
    tile = Tile.remove_monitor(tile, location, machine_ref)
    {:reply, :ok, tile}
  end

  @impl GenServer
  def handle_cast(:display, tile) do
    {x_size, y_size} = @tile_size

    for y <- 0..(y_size - 1) do
      row =
        for x <- 0..(x_size - 1) do
          case tile.locations[{x, y}] do
            nil -> " "
            _ -> "X"
          end
        end

      IO.puts(row)
    end

    {:noreply, tile}
  end

  def handle_cast({:notify_location_taken, requestor, location, target}, tile) do
    # Mostly used for internal cast: when placing a machine one of the locations
    # the new machine is interested in was already taken. This handle_cast
    # gets called to make sure it is dealt with. We can't do that in the response
    # as that would create a call loop.
    MachineServer.process_location_claimed(requestor, location, target)
    {:noreply, tile}
  end

  @impl GenServer
  def handle_info({:DOWN, _proc_ref, :process, object, :shutdown}, tile) do
    # When a machine gets shutdown gracefully, we just need to remove it from all
    # references.
    Logger.warn("Reacting to :DOWN event for #{inspect(object)}")
    {tile, _} = Tile.remove_machine_state(tile, object)
    {:noreply, tile}
  end

  def handle_info({:DOWN, proc_ref, :process, object, :killed}, tile) do
    # Machine crashed, and should have been restarted. Question is now:
    # what to do?
    Logger.warn("Process with PID #{inspect(object)} crashed, what to do?")
    tile = Tile.handle_crashed_machine(tile, proc_ref)
    {:noreply, tile}
  end
end
