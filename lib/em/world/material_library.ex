defmodule EM.World.MaterialLibrary do
  @moduledoc """
  The MaterialLibary stores generic material properties.
  """

  use GenServer

  alias __MODULE__, as: MaterialLibrary

  @impl GenServer
  def init(_) do
    # FIXME: use settings for this
    GenServer.cast(self(), {:load, "data/materials.json"})
    :ets.new(:material_lib, [:set, :named_table, :protected, read_concurrency: true])
    {:ok, %{}}
  end

  def start_link(_) do
    GenServer.start_link(MaterialLibrary, nil, name: MaterialLibrary)
  end

  @impl GenServer
  def handle_cast({:load, filename}, state) do
    filename
    |> read_materials()
    |> Enum.each(fn {material, ss} -> :ets.insert(:material_lib, {material, ss}) end)

    {:noreply, state}
  end

  @doc """
  Get the stack size of `material`.

  The stack size of a material is the amount of that material that can be stored
  in one storage slot.

  ## Examples
      iex> MaterialLibrary.get_stack_size("stone")
      {:ok, 50}

  """
  @spec get_stack_size(String.t()) :: {:ok, pos_integer()} | {:error, :unknown_material}
  def get_stack_size(material) do
    case :ets.lookup(:material_lib, material) do
      [{^material, stack_size}] -> {:ok, stack_size}
      _ -> {:error, :unknown_material}
    end
  end

  @doc """
  Get the stack size of `material`.

  Throws an error when the material is unknown.

  ## Examples
      iex> MaterialLibrary.get_stack_size!("stone")
      50

  """
  @spec get_stack_size!(String.t()) :: pos_integer()
  def get_stack_size!(material) do
    {:ok, stack_size} = get_stack_size(material)
    stack_size
  end

  @doc """
  Get the stack sizes for a list of materials.

  Returns a Map with the name of the material as keys, and the stack size as
  positive integers.

  ## Examples
      iex> MaterialLibrary.get_stack_sizes(["stone", "copper cable"])
      %{"stone" => 50, "copper cable" => 200}

  """
  @spec get_stack_sizes([String.t()]) :: %{String.t() => pos_integer()}
  def get_stack_sizes(material_names) do
    material_names
    |> Enum.reduce(
      %{},
      fn type, acc -> Map.put(acc, type, get_stack_size!(type)) end
    )
  end

  defp read_materials(filename) do
    with content <- File.read!(filename), {:ok, json} <- Jason.decode(content), do: json
  end
end
