defmodule EM.World.RecipeLibrary do
  @moduledoc """
  Module to access recipes.

  Recipes are use by Assembly machines to indicate how input materials can be
  converted into output materials.
  """
  use GenServer

  alias __MODULE__, as: RecipeLibrary
  alias EM.Machines.Utils.Recipe

  @impl GenServer
  def init(_) do
    GenServer.cast(self(), {:load, "data/recipes.json"})
    :ets.new(:recipe_lib, [:set, :named_table, :protected, read_concurrency: true])
    {:ok, %{}}
  end

  def start_link(_) do
    GenServer.start_link(RecipeLibrary, nil, name: RecipeLibrary)
  end

  @impl GenServer
  def handle_cast({:load, filename}, state) do
    filename
    |> read_recipes()
    |> Enum.map(fn {name, content} -> Recipe.from_json(name, content) end)
    |> Enum.each(fn recipe -> :ets.insert(:recipe_lib, {recipe.name, recipe}) end)

    {:noreply, state}
  end

  @doc """
  Get a recipe by its name.

  Used by Assemblers to determine how to turn input resources into outputs.

  ## Example
      iex> EM.World.RecipeLibrary.get_recipe("copper cable")
      %EM.Machines.Utils.Recipe{
          name: "copper cable",
          resource_out: {"copper cable", 2},
          resources_in: %{"copper plate" => 1},
          time: 500
      }

  """
  @spec get_recipe(String.t()) :: Recipe.t()
  def get_recipe(name) do
    [{^name, recipe}] = :ets.lookup(:recipe_lib, name)
    # FIXME: this is a hack
    output = recipe.resource_out |> Map.to_list() |> List.first()
    %{recipe | resource_out: output}
  end

  defp read_recipes(filename) do
    with content <- File.read!(filename),
         {:ok, json} <- Jason.decode(content) do
      json
    end
  end
end
