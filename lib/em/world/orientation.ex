defmodule EM.World.Orientation do
  @moduledoc """
  Orientations help with determining the relative positions of machines.
  """

  @type t() :: :north | :east | :south | :west

  @doc """
  Get the compass opposite of `orientation`.

  ## Examples
      iex> Orientation.opposite(:north)
      :south

  """
  @spec opposite(t()) :: t()
  def opposite(:north), do: :south
  def opposite(:east), do: :west
  def opposite(:south), do: :north
  def opposite(:west), do: :east

  @doc """
  Rotate the `orientation` clockwise.

  ## Examples
      iex> Orientation.rotate_cw(:south)
      :west

  """
  @spec rotate_cw(t()) :: t()
  def rotate_cw(:north), do: :east
  def rotate_cw(:east), do: :south
  def rotate_cw(:south), do: :west
  def rotate_cw(:west), do: :north

  @doc """
  Rotate the `orientation` counter clockwise.

  ## Examples
      iex> Orientation.rotate_ccw(:south)
      :east

  """
  @spec rotate_ccw(t()) :: t()
  def rotate_ccw(:north), do: :west
  def rotate_ccw(:east), do: :north
  def rotate_ccw(:south), do: :east
  def rotate_ccw(:west), do: :south
end
