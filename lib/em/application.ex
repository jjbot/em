defmodule EM.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    topologies = [
      cluster: [
        strategy: Cluster.Strategy.ErlangHosts,
        config: [timeout: 100]
      ]
    ]

    children = [
      {Cluster.Supervisor, [topologies, [name: EM.ClusterSupervisor]]},
      {EM.World.MaterialLibrary, nil},
      {EM.World.RecipeLibrary, nil},
      {DynamicSupervisor, strategy: :one_for_one, name: TileSupervisor},
      {MachineSupervisor, strategy: :one_for_one, name: MachineSupervisor},
      {Task.Supervisor, name: TaskSupervisor},
      {EM.World.TileManager, nil},
      {EM.World.ProductionStats, nil}
    ]

    opts = [strategy: :one_for_one, name: Em.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
