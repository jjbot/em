defmodule EM.World do
  @moduledoc """
  The main interface to interact with Elixir Machines.
  """

  alias EM.Machines.{Machine, MachineServer}
  alias EM.World.Location
  alias EM.World.{Tile, TileManager, TileServer}

  @doc """
  Add a machine by specifying the machine struct and location.

  This is the primary way to add new machines to the game. It takes care of
  creating a `Tile` in the case it doesn't exist yet, and requests the `Tile` to
  add the machine.
  """
  @spec add_machine(struct(), Location.t()) :: {:ok, pid()} | {:error, atom()}
  def add_machine(machine, location) do
    if locations_available?(Location.gen_locations(location, Machine.size(machine))) do
      tile_pid = get_or_create_tile_at(location)

      case TileServer.add_machine(tile_pid, machine, location) do
        {:ok, machine_pid, machine_name} ->
          MachineServer.start(machine_pid)
          {:ok, machine_pid, machine_name}

        {:error, reason} ->
          {:error, reason}
      end
    else
      {:error, :positions_not_available}
    end
  end

  @doc """
  Get the tile counts for all nodes connected to the cluster.

  This returns a list with {node_name, count} tuples.
  """
  @spec get_node_tile_counts() :: [{atom(), non_neg_integer()}]
  def get_node_tile_counts() do
    # Add own name to list of cluster nodes.
    nodes = [:erlang.node() | Node.list()]

    nodes
    |> Enum.map(fn remote -> TileManager.name(remote) end)
    |> Enum.map(fn remote ->
      Task.Supervisor.async(
        TaskSupervisor,
        fn -> GenServer.call(remote, :get_tile_count) end
      )
    end)
    |> Enum.map(fn t -> Task.await(t) end)
    |> (fn l -> [nodes, l] end).()
    |> Enum.zip()
  end

  @doc """
  Helper function to either lookup or create a Tile for the specified `location`.
  """
  @spec get_or_create_tile_at(Location.t()) :: GenServer.name()
  def get_or_create_tile_at(location) do
    n_loc = Location.normalise(location)

    case :syn.whereis({:tile, n_loc}) do
      pid when is_pid(pid) ->
        Tile.get_name(n_loc)

      :undefined ->
        create_tile(n_loc)
        # Using a bit of a hack: we create the tile, but don't return the name as :syn
        # might still be syncing the cluster.
        get_or_create_tile_at(n_loc)
    end
  end

  @doc """
  Get the `Tile` responsible for the specified `location`.

  Returns {:ok, pid} when the `Tile` exists, or {:error, :undefined} when it
  doesn't.
  """
  def get_tile_at(location) do
    n_loc = Location.normalise(location)

    case :syn.whereis({:tile, n_loc}) do
      pid when is_pid(pid) -> {:ok, pid}
      :undefined -> {:error, :undefined}
    end
  end

  @doc """
  Get the `Tile` responsible for the specified `location`.

  Raises an error when the `Tile` doesn't exist.
  """
  @spec get_tile_at!(Location.t()) :: pid()
  def get_tile_at!(location) do
    case get_tile_at(location) do
      {:ok, pid} -> pid
      _ -> raise "Tile at location #{inspect(location)} does not exist"
    end
  end

  @doc """
  Get the tiles that host the provided `locations`.
  """
  @spec get_tile_location_mapping([Location.t()]) :: %{GenServer.name() => [Location.t()]}
  def get_tile_location_mapping(locations) do
    locations
    |> Enum.map(fn loc -> {Location.normalise(loc), loc} end)
    |> Enum.map(fn {norm, loc} -> {get_or_create_tile_at(norm), loc} end)
    |> Enum.reduce(
      %{},
      fn {ref, loc}, acc ->
        Map.update(acc, ref, [loc], fn l -> [loc | l] end)
      end
    )
  end

  @doc """
  Try to get a machine form the specified `location`.

  Tries to lookup the Tile and Machine when they exists, and returns {:ok, pid}
  when they do, or {:error, :undefined} if either of them don't.
  """
  @spec get_machine_at(Location.t()) :: {:ok, pid} | {:error, atom()}
  def get_machine_at(location) do
    case get_tile_at(location) do
      {:ok, tile_pid} -> TileServer.get_machine_at(tile_pid, location)
      {:error, :undefined} -> {:error, :undefined}
    end
  end

  @doc """
  Get the number of Tiles hosted by the current node.
  """
  @spec get_tile_count() :: non_neg_integer()
  def get_tile_count() do
    GenServer.call(TileManager.name(), :get_tile_count)
  end

  @doc """
  Get the number of tiles hosted by the specified node.
  """
  @spec get_tile_count(atom()) :: non_neg_integer()
  def get_tile_count(node_name) do
    GenServer.call(TileManager.name(node_name), :get_tile_count)
  end

  @spec register_monitor(Location.t(), any()) :: :ok
  def register_monitor(location, machine_ref) do
    tile_ref = get_or_create_tile_at(location)
    TileServer.register_monitor(tile_ref, location, machine_ref)
  end

  @spec unregister_monitor(Location.t(), any()) :: :ok
  def unregister_monitor(location, machine_ref) do
    tile_ref = get_or_create_tile_at(location)
    TileServer.unregister_monitor(tile_ref, location, machine_ref)
  end

  def locations_available?(locations) do
    locations
    |> Enum.map(&{&1, elem(get_tile_at(&1), 1)})
    |> Enum.reject(fn {_loc, tile} -> tile == :undefined end)
    |> Enum.reduce(
      %{},
      fn {loc, tile_name}, acc ->
        Map.update(acc, tile_name, [loc], fn existing -> [loc | existing] end)
      end
    )
    |> Enum.map(fn {tile_name, locations} ->
      Task.Supervisor.async(
        TaskSupervisor,
        fn -> TileServer.locations_available?(tile_name, locations) end
      )
    end)
    |> Enum.map(&Task.await(&1))
    |> Enum.all?()
  end

  # FIXME: why is this here and not in TileManager?
  defp create_tile(location) do
    get_node_tile_counts()
    |> Enum.min_by(fn {_name, count} -> count end)
    |> elem(0)
    |> TileManager.name()
    |> GenServer.call({:create_tile, location})
  end
end
