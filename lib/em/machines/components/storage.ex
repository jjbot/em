defprotocol EM.Machines.Components.Storage do
  @moduledoc """
  Defenition of the functions Storage components need to provide.

  Storage components are used as part of machines, and keep track of materials
  that are stored in that machine. Storage allows resources to be put and taken,
  implementations of this protocol can restrict the amount and types of resources
  that can be present.

  Implementations: `EM.Machines.Components.SlotStorage` and
  `EM.Machines.Components.TypedStorage`.
  """
  alias EM.Machines.Payload

  @doc """
  Check if `container` is empty.
  """
  @spec is_empty?(t()) :: boolean()
  def is_empty?(container)

  @doc """
  Try to put `payload` into storage.

  This operation fails when there is not enough space available or when the offered
  resource is not part of the resource types that the storage supports.

  ## Examples
      # Adding a payload to storage with space available.
      iex> s = %SlotStorage{}
      iex> Storage.put(s, {"stone", 4})
      {:ok, %SlotStorage{slots: 8, content: %{"stone" => 4}}}

      # Adding a payload to storage that is full.
      iex> s = %TypedStorage{slots: %{"stone" => 1}, content: %{"stone" => 50}}
      iex> Storage.put(s, {"stone", 4})
      {:error, :not_enough_space}

      # Adding a payload to storage that only supports a different type of material.
      iex> s = %TypedStorage{slots: %{"iron plate" => 4}}
      iex> Storage.put(s, {"stone", 4})
      {:error, :type_not_supported}

  """
  @spec put(t(), Payload.t()) :: {:ok, t()} | {:error, atom()}
  def put(container, payload)

  @doc """
  Show how much space is available for material of type `material_type`.

  ## Examples
      # For TypedStorage it gives you the space reserved minus the amount stored.
      iex> s = %TypedStorage{slots: %{"stone" => 50}, content: %{"stone" => 14}}
      iex> Storage.space_for(s, "stone")
      36

      # For SlotStorage it gives you the left over amount of a stack and the amount
      # of empty slots times the stack size.
      iex> s = %SlotStorage{slots: 4, content: %{"iron plate" => 10, "stone" => 4}}
      iex> Storage.space_for(s, "stone")
      146

      # Storage for any material in a Map is always infinite.
      iex> Storage.space_for(%{"stone" => 1_000_000}, "stone")
      :infinity
  """
  @spec space_for(t(), String.t()) :: non_neg_integer() | :infinity
  def space_for(container, material_type)

  @doc """
  Try to take a single material from `container`.

  If there are different types of materials in Storage, one will be picked at
  random.

  ## Examples
      # Take when only one type of material is stored.
      iex> s = %SlotStorage{slots: 8, content: %{"stone" => 10}}
      iex> Storage.take(s)
      {:ok, %SlotStorage{content: %{"stone" => 9}}, {"stone", 1}}

      # Take when storage is empty.
      iex> Storage.take(%SlotStorage{slots: 8})
      {:error, :not_available}

  """
  @spec take(t()) :: {:ok, t(), Payload.t()} | {:error, atom()}
  def take(container)

  @doc """
  Try to take `payload` resources from storage.

  This operation fails when the requested resources are not available. It's up to
  the module implementing the protocol ot decide whether it wants to be specific
  as to why the resources are not available.

  ## Examples
      iex> s = %SlotStorage{slots: 8, content: %{"stone" => 10}}
      iex> Storage.take(s, {"stone", 7})
      {:ok, %SlotStorage{content: %{"stone" => 3}}, {"stone", 7}}

      iex> s = %SlotStorage{slots: 8, content: %{"stone" => 10}}
      iex> Storage.take(s, {"iron plate", 7})
      {:error, :not_available}

  """
  @spec take(t(), Payload.t()) ::
          {:ok, t(), Payload.t()} | {:error, atom()}
  def take(container, payload)

  @doc """
  Convert the container to a Map.

  This only preserves the content of the container.

  ## Examples
      iex> Storage.to_map(%SlotStorage{slots: 8, content: %{"stone" => 8, "iron plate" => 89}})
      %{"stone" => 8, "iron plate" => 89}

      iex> Storage.to_map(
      ...>  %TypedStorage{slots: %{"stone" => 100, "iron plate" => 100}, content: %{"stone" => 23}}
      ...>)
      %{"stone" => 23}

  """
  @spec to_map(t()) :: %{String.t() => pos_integer()}
  def to_map(container)
end

defmodule EM.Machines.StorageHelper do
  @moduledoc """
  Helper module for common storage operations.
  """

  @doc """
  Remove all materials from Map that have zero elements left.
  """
  @spec remove_empty_from_map(%{String.t() => non_neg_integer()}) :: %{
          String.t() => pos_integer()
        }
  def remove_empty_from_map(storage) do
    storage
    |> Enum.reject(fn {_k, v} -> v == 0 end)
    |> Enum.reduce(%{}, fn {k, v}, acc -> Map.put(acc, k, v) end)
  end

  @doc """
  Check if `type` has zero elements left. If so, remove from Map.
  """
  @spec remove_zero_element(%{String.t() => non_neg_integer()}, String.t()) ::
          %{String.t() => non_neg_integer()}
  def remove_zero_element(storage, type) do
    case storage[type] do
      amount when amount == 0 ->
        Map.delete(storage, type)

      _ ->
        storage
    end
  end
end

defimpl EM.Machines.Components.Storage, for: Map do
  @moduledoc """
  Storage implementation for Map.
  """

  alias EM.Machines.StorageHelper

  def is_empty?(container) do
    %{} == container
  end

  def put(container, {type, amount}) do
    {_, container} =
      Map.get_and_update(
        container,
        type,
        fn cur -> {cur, (cur || 0) + amount} end
      )

    {:ok, container}
  end

  def space_for(_container, _material_type) do
    # There is no limit to the amount of items that can be stored in this storage
    # type.
    :infinity
  end

  def take(container) when container == %{}, do: {:error, :not_available}

  def take(container) do
    {type, _stored_amount} = container |> Enum.random()
    take(container, {type, 1})
  end

  def take(container, {type, amount}) do
    case Map.get(container, type, nil) do
      available when is_integer(available) and available >= amount ->
        container =
          container
          |> Map.update!(type, fn s -> s - amount end)
          |> StorageHelper.remove_zero_element(type)

        {:ok, container, {type, amount}}

      _ ->
        {:error, :not_available}
    end
  end

  def to_map(container), do: container
end

defimpl EM.Machines.Components.Storage,
  for: [EM.Machines.Components.SlotStorage, EM.Machines.Components.TypedStorage] do
  defdelegate is_empty?(container), to: @for
  defdelegate put(container, payload), to: @for
  defdelegate take(container), to: @for
  defdelegate take(container, payload), to: @for
  defdelegate to_map(container), to: @for
  defdelegate space_for(container, material_type), to: @for
end
