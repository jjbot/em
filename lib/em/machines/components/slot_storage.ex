defmodule EM.Machines.Components.SlotStorage do
  @moduledoc """
  Storage implementation that mimics the basic Factorio chest.

  This storage provides n untyped slots, usable for any type of material. Useful
  for chests, train wagons and any other machine where you don't care about the
  type of the resources being stored.
  """

  alias __MODULE__, as: SlotStorage

  alias EM.Machines.Payload
  alias EM.World.MaterialLibrary

  defstruct slots: 8, content: %{}

  @type t :: %SlotStorage{
          slots: integer(),
          content: %{String.t() => integer()}
        }

  @spec is_empty?(t()) :: boolean()
  def is_empty?(%SlotStorage{content: content}) do
    content == %{}
  end

  @doc """
  Try to put `payload` into container.
  """
  @spec put(t(), {String.t(), pos_integer}) :: {:ok, t()} | {:error, :not_enough_space}
  def put(%SlotStorage{slots: slots, content: content} = container, {type, amount} = payload) do
    if get_required_slots(payload, content) == 0 do
      {:ok, %{container | content: Map.update!(content, type, &(&1 + amount))}}
    else
      stack_sizes = content |> Map.keys() |> MaterialLibrary.get_stack_sizes()
      used_slots = get_used_slots(content, stack_sizes)
      required_slots = get_required_slots(payload, content)

      if used_slots + required_slots <= slots do
        current = Map.get(content, type, 0)
        {:ok, %{container | content: Map.put(content, type, current + amount)}}
      else
        {:error, :not_enough_space}
      end
    end
  end

  @doc """
  Try to take any resource from `container`.
  """
  def take(%SlotStorage{content: content} = container) do
    if is_empty?(container) do
      {:error, :not_available}
    else
      {type, _} = content |> Enum.random()
      take(container, {type, 1})
    end
  end

  @doc """
  Try to take `payload` from container.
  """
  @spec take(t(), Payload.t()) :: {:ok, t()} | {:error, :not_available}
  def take(%SlotStorage{content: content} = container, {type, amount})
      when is_map_key(content, type) do
    if content[type] >= amount do
      content =
        content
        |> Map.update!(type, &(&1 - amount))
        |> remove_zero_element(type)

      {:ok, %{container | content: content}, {type, amount}}
    else
      {:error, :not_available}
    end
  end

  def take(_container, _payload) do
    {:error, :not_available}
  end

  @spec to_map(t()) :: %{String.t() => pos_integer()}
  def to_map(%SlotStorage{content: content}),
    do: content |> EM.Machines.StorageHelper.remove_empty_from_map()

  @spec space_for(t(), String.t()) :: non_neg_integer()
  def space_for(%SlotStorage{content: content, slots: slots}, material_type) do
    stack_sizes = content |> Map.keys() |> MaterialLibrary.get_stack_sizes()
    used_slots = get_used_slots(content, stack_sizes)
    stack_size = MaterialLibrary.get_stack_size!(material_type)

    # using a shortcut here: if the material type is not in `content`, we default
    # to `stacksize`, therefore, `left_over` will be zero if the material is not
    # present in the current `content`.
    left_over = stack_size - Map.get(content, material_type, stack_size)
    stack_size * (slots - used_slots) + left_over
  end

  defp get_required_slots({type, amount}, content) do
    stack_size = MaterialLibrary.get_stack_size!(type)

    case get_left_over_stack_space(content, type, stack_size) - amount do
      left_over when left_over >= 0 -> 0
      space_short -> get_stack_count(-space_short, stack_size)
    end
  end

  defp get_left_over_stack_space(content, type, stack_size) do
    stored = Map.get(content, type, 0)

    case rem(stored, stack_size) do
      0 -> 0
      left_over -> stack_size - left_over
    end
  end

  defp get_used_slots(content, stack_sizes) do
    get_stacks_per_type(content, stack_sizes)
    |> Enum.reduce(0, fn {_type, amount}, acc -> acc + amount end)
  end

  defp get_stacks_per_type(content, stack_sizes) do
    content
    |> Enum.reduce(%{}, fn {type, amount}, acc ->
      Map.put(acc, type, get_stack_count(amount, stack_sizes[type]))
    end)
  end

  defp get_stack_count(amount, stack_size) do
    case rem(amount, stack_size) do
      left_over when left_over == 0 -> div(amount, stack_size)
      _left_over -> div(amount, stack_size) + 1
    end
  end

  defp remove_zero_element(storage, type) do
    case storage[type] do
      amount when amount == 0 ->
        Map.delete(storage, type)

      _ ->
        storage
    end
  end
end
