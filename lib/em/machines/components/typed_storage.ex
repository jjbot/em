defmodule EM.Machines.Components.TypedStorage do
  @moduledoc """
  Typed storage implmentation.

  This mimics the behaviour of Factorio's input slots of assemblers, typed chests
  and any other machine where you need to specify the types of resources that can
  be stored.
  """

  alias __MODULE__, as: TypedStorage
  alias EM.Machines.Payload

  defstruct slots: %{}, content: %{}

  @type t() :: %TypedStorage{
          slots: %{String.t() => pos_integer()},
          content: %{String.t() => pos_integer()}
        }

  def init_empty(materials) do
    materials
    |> Enum.reduce(
      %{},
      fn {material, space}, acc ->
        Map.put(acc, material, space)
      end
    )
    |> (fn slots -> {:ok, %TypedStorage{slots: slots}} end).()
  end

  @spec is_empty?(t()) :: boolean()
  def is_empty?(%TypedStorage{content: content}) do
    content == %{}
  end

  @doc """
  Try to put `payload` into container.
  """
  @spec put(t(), {String.t(), pos_integer}) ::
          {:ok, t()} | {:error, :not_enough_space} | {:error, :type_not_supported}
  def put(%TypedStorage{content: content, slots: slots} = container, {type, amount})
      when is_map_key(slots, type) do
    max_amount = slots[type]
    stored = Map.get(content, type, 0)

    if stored + amount <= max_amount do
      {:ok, %{container | content: Map.put(content, type, stored + amount)}}
    else
      {:error, :not_enough_space}
    end
  end

  def put(_container, _payload) do
    {:error, :type_not_supported}
  end

  @doc """
  Try to take any resource from `container`.
  """
  def take(%TypedStorage{content: content} = container) do
    if is_empty?(container) do
      {:error, :not_available}
    else
      {type, _} = content |> Enum.random()
      take(container, {type, 1})
    end
  end

  @doc """
  Try to take `payload` from `container`.
  """
  @spec take(t(), Payload.t()) ::
          {:ok, t(), Payload.t()} | {:error, :not_available}
  def take(%TypedStorage{content: content} = container, {type, amount} = payload)
      when is_map_key(content, type) do
    stored = content[type]

    if stored - amount >= 0 do
      {:ok, %{container | content: Map.put(content, type, stored - amount)}, payload}
    else
      {:error, :not_available}
    end
  end

  def take(_container, _payload) do
    {:error, :not_available}
  end

  @spec to_map(t()) :: %{String.t() => pos_integer()}
  def to_map(%TypedStorage{content: content}) do
    content
  end

  @spec space_for(t(), String.t()) :: non_neg_integer()
  def space_for(%TypedStorage{content: content, slots: slots}, type) do
    Map.get(slots, type, 0) - Map.get(content, type, 0)
  end
end
