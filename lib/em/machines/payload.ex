defmodule EM.Machines.Payload do
  @moduledoc """
  Payloads are used to transfer materials from one machine to another, usually
  done by inserters.
  """

  @typedoc """
  Payloads are used to transfer materials from one machine to another, usually
  done by inserters.
  """
  @type t :: {String.t(), pos_integer()}
end
