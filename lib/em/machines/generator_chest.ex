defmodule EM.Machines.GeneratorChest do
  @moduledoc """
  A chest implementation that potentially generates infinite amounts of resources.
  """
  alias __MODULE__, as: GeneratorChest
  alias EM.Machines.Components.Storage
  alias EM.Machines.{Chest, ContainerImpl, Payload}
  alias EM.World.Location

  defstruct backup_server: nil,
            bl: {},
            consumer_q: :queue.new(),
            provider_q: :queue.new(),
            storage: %{},
            timers: %{}

  @type t :: %GeneratorChest{
          backup_server: GenServer.name() | nil,
          bl: Location.t(),
          consumer_q: :queue.queue(),
          provider_q: :queue.queue(),
          storage: %{String.t() => pos_integer()},
          timers: %{String.t() => :timer.tref() | nil}
        }

  ############################################################
  # Machine functions
  ############################################################

  def get_backup_state(chest), do: [bl: chest.bl]

  ############################################################
  # Storage functions.
  ############################################################

  @doc """
  Remove `consumer` from the queue. Returns an error when consumer is not in queue.
  """
  @spec dequeue_consumer(t(), pid()) :: {:ok, t()} | {:error, :not_queued}
  def dequeue_consumer(%GeneratorChest{consumer_q: q} = chest, consumer) do
    case ContainerImpl.dequeue(q, consumer) do
      {:ok, queue} -> {:ok, %{chest | consumer_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  @doc """
  Remove `provider` from the queue. Returns an error when consumer is not in queue.
  """
  @spec dequeue_provider(t(), pid()) :: {:ok, t()} | {:error, :not_queued}
  def dequeue_provider(%GeneratorChest{provider_q: q} = chest, consumer) do
    case ContainerImpl.dequeue(q, consumer) do
      {:ok, queue} -> {:ok, %{chest | provider_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  @doc """
  Add `consumer` to queue.
  """
  @spec enqueue_consumer(t(), pid()) :: {:ok, t()} | {:error, :already_queued}
  def enqueue_consumer(%GeneratorChest{consumer_q: q, storage: storage} = chest, consumer) do
    case ContainerImpl.enqueue_consumer(q, storage, consumer) do
      {:ok, queue} -> {:ok, %{chest | consumer_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  @doc """
  Add `provider` to queue.
  """
  @spec enqueue_provider(t(), pid(), Payload.t()) ::
          {:ok, t()} | {:error, :already_queued}
  def enqueue_provider(
        %GeneratorChest{provider_q: q, storage: storage} = chest,
        provider,
        payload
      ) do
    case ContainerImpl.enqueue_provider(q, storage, provider, payload) do
      {:ok, queue} -> {:ok, %{chest | provider_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  @spec put(t(), Payload.t()) :: {:ok, t()}
  def put(%GeneratorChest{storage: storage} = chest, payload) do
    case Storage.put(storage, payload) do
      {:ok, storage} ->
        chest =
          chest
          |> Map.put(:storage, storage)
          |> Chest.notify_queued_consumer()

        {:ok, chest}

      {:error, reason} ->
        {:error, reason}
    end
  end

  @spec take(t()) ::
          {:ok, t(), Payload.t()}
          | {:error, :not_available}
  def take(%GeneratorChest{storage: storage}) when storage == %{}, do: {:error, :not_available}

  def take(%GeneratorChest{storage: storage} = chest) do
    {type, _stored_amount} = storage |> Enum.random()
    take(chest, {type, 1})
  end

  @spec take(t(), Payload.t()) ::
          {:ok, t(), Payload.t()}
          | {:error, :not_available}
  def take(%GeneratorChest{storage: storage} = chest, payload) do
    case Storage.take(storage, payload) do
      {:ok, storage, payload} ->
        chest =
          chest
          |> Map.put(:storage, storage)
          |> Chest.notify_queued_provider()

        {:ok, chest, payload}

      {:error, message} ->
        {:error, message}
    end
  end

  ############################################################
  # GeneratorChest specific functions
  ############################################################

  def add_generator(
        %GeneratorChest{timers: timers} = generator_chest,
        {type, _} = payload,
        interval
      ) do
    {:ok, timer} = :timer.send_interval(interval, {:resources_available, payload})
    # FIXME: now can only have one timer per resource type
    timers = Map.put(timers, type, timer)
    {:ok, %{generator_chest | timers: timers}}
  end
end

defimpl EM.Machines.Machine, for: EM.Machines.GeneratorChest do
  alias EM.Machines.GeneratorChest

  def bl(%GeneratorChest{bl: bl}), do: bl

  def describe(chest) do
    %{
      "name" => "generator_chest@#{inspect(chest.bl)}",
      "type" => "generator_chest"
    }
  end

  defdelegate get_backup_state(chest), to: GeneratorChest

  def locations(%GeneratorChest{bl: bl}), do: [bl]

  def process_location_claimed(chest, _location, _remote_machine_ref), do: chest

  def rotate(chest), do: chest

  def server_module(_chest), do: EM.Machines.GeneratorChestServer

  def start(chest), do: chest

  def state_module(_chest), do: EM.Machines.GeneratorChest

  def size(_chest), do: {1, 1}

  def is_transporter(_chest), do: false
end

defimpl EM.Machines.Container, for: EM.Machines.GeneratorChest do
  alias EM.Machines.GeneratorChest

  defdelegate dequeue_consumer(chest, consumer), to: GeneratorChest

  defdelegate dequeue_provider(chest, consumer), to: GeneratorChest

  defdelegate enqueue_consumer(chest, consumer), to: GeneratorChest

  defdelegate enqueue_provider(chest, provider, payload), to: GeneratorChest

  defdelegate put(chest, payload), to: GeneratorChest

  defdelegate take(chest), to: GeneratorChest

  defdelegate take(chest, payload), to: GeneratorChest
end

defmodule EM.Machines.GeneratorChestServer do
  @moduledoc """
  Server module for GeneratorChest.
  """

  alias EM.Machines.GeneratorChest

  use GenServer

  use EM.Machines.MachineServer
  use EM.Machines.ContainerServer

  def add_generator(pid, payload, interval) do
    GenServer.call(pid, {:add_generator, payload, interval})
  end

  @impl GenServer
  def handle_call({:add_generator, payload, interval}, _from, generator_chest) do
    case GeneratorChest.add_generator(generator_chest, payload, interval) do
      {:ok, gc} -> {:reply, :ok, gc}
    end
  end

  @impl GenServer
  def handle_info({:resources_available, payload}, generator_chest) do
    {:ok, chest} = GeneratorChest.put(generator_chest, payload)
    {:noreply, chest}
  end
end
