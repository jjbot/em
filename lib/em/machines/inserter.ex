defmodule EM.Machines.Inserter do
  @moduledoc """
  Inserter implementation for Elixir Machines.

  Inserters move materials from one storage process to another.
  """

  alias __MODULE__, as: Inserter
  alias EM.Machines.{InserterServer, ContainerServer, MachineServer, Payload}
  alias EM.World
  alias EM.World.{Location, Orientation}

  require Logger

  defstruct backup_server: nil,
            bl: {},
            capacity: 1,
            current_orientation: :south,
            payload: {nil, nil},
            source: nil,
            status: :idle,
            swing_target: nil,
            swing_time: 500,
            target: nil,
            target_orientation: :north,
            timer: nil

  @typedoc """
  Payload that can be transported by an inserter.

  This can either be a tuple with the name and amount of the material carried,
  or {nil, nil}, indicating the inserter is empty.
  """
  @type status :: :idle | :moving | :waiting

  @typedoc """
  Inserter type. Fields:
  - backup_server: reference to server where state can be backed up
  - bl: bottom left corner, for inserters, the only cell it occupies
  - capacity: the amount of resources the inserter can move in one swing.
  - current_orientation: where the inserter is currently pointing to.
  - payload: the current payload of the inserter.
  - source: the process where the inserter will be taking materials from.
  - status: current status of the inserter, allowed values are defined in
    `EM.Machines.Inserter.status`.
  - swing_target: when in motion, gives where the inserter is swinging towards.
  - swing_time: amount of time it takes (in ms) for the inserter to swing from
    one orientation to another.
  - target: where the inserter will be depositing materials.
  - target_orientation: where the target of the inserter is.
  - timer: reference to the `:timer` that will be set when the inserter is in
    motion.
  """
  @type t :: %Inserter{
          backup_server: GenServer.name() | nil,
          bl: Location.t(),
          capacity: pos_integer(),
          current_orientation: Orientation.t(),
          payload: Payload.t(),
          source: pid() | nil,
          status: status(),
          swing_target: Orientation.t() | nil,
          swing_time: pos_integer(),
          target: pid() | nil,
          target_orientation: Orientation.t(),
          timer: :timer.tref() | nil
        }

  ############################################################
  # Machine functions
  ############################################################
  def get_backup_state(inserter) do
    [
      %{
        inserter
        | backup_server: nil,
          payload: nil,
          source: nil,
          status: nil,
          swing_target: nil,
          target: nil,
          timer: nil
      }
    ]
  end

  @doc """
  Let the `inserter` know one of the `locations` it asked to monitor has been
  claimed by a machine.

  The inserter will check whether the newly occupied location corresponds to
  its source or target locations, and set them up appropriately. It will also
  check if there is any action that needs to be taken, such as depositing or
  fetching a payload.
  """
  @spec process_location_claimed(t(), Location.t(), GenServer.name()) :: t()
  def process_location_claimed(inserter, location, remote_machine) do
    target_loc =
      Location.get_neighbour_location(
        inserter.bl,
        inserter.target_orientation
      )

    source_loc =
      Location.get_neighbour_location(
        inserter.bl,
        Orientation.opposite(inserter.target_orientation)
      )

    inserter =
      case location do
        ^target_loc ->
          set_target(inserter, remote_machine)

        ^source_loc ->
          set_source(inserter, remote_machine)

        _loc ->
          inserter
      end

    start(inserter)
  end

  @doc """
  Check if the `inserter` is at its target.
  """
  @spec at_target?(t()) :: boolean()
  def at_target?(%Inserter{target_orientation: to, current_orientation: co}) do
    to == co
  end

  @doc """
  Check if the `inserter` is carrying a payload.
  """
  @spec has_payload?(t()) :: boolean()
  def has_payload?(%Inserter{payload: payload}) do
    payload != {nil, nil}
  end

  @doc """
  Get the orientation where the inserter's target is.
  """
  @spec get_target_orientation(t()) :: Orientation.t()
  def get_target_orientation(inserter) do
    inserter.target_orientation
  end

  @doc """
  Get the target location for the `inserter`.
  """
  @spec get_target_location(t()) :: Location.t()
  def get_target_location(%Inserter{bl: bl, target_orientation: to}) do
    Location.get_neighbour_location(bl, to)
  end

  @doc """
  Get the source location for the `inserter`.
  """
  @spec get_source_location(t()) :: Location.t()
  def get_source_location(%Inserter{bl: bl, target_orientation: to}) do
    Location.get_neighbour_location(bl, Orientation.opposite(to))
  end

  @doc """
  Rotate the `inserter` 90 degrees clockwise.
  """
  @spec rotate(t()) :: t()
  def rotate(%Inserter{target_orientation: to} = inserter) do
    # Remove observation locations
    World.unregister_monitor(get_target_location(inserter), self())
    World.unregister_monitor(get_source_location(inserter), self())

    inserter = %{inserter | target_orientation: Orientation.rotate_cw(to)}

    # Set new observation locations
    World.register_monitor(get_target_location(inserter), self())
    World.register_monitor(get_source_location(inserter), self())

    # Remove old source and target
    inserter = %{inserter | source: nil, target: nil}

    # Setting backup is required: the rotation effects how the machine will function.
    MachineServer.create_backup(self())
    inserter
  end

  @doc """
  Set the orientation of the inserter.
  """
  @spec set_orientation(t(), Orientation.t()) :: t()
  def set_orientation(%Inserter{} = inserter, orientation) do
    %{inserter | current_orientation: orientation}
  end

  @doc """
  Set the source process of the inserter.
  """
  @spec set_source(t(), pid()) :: t()
  def set_source(%Inserter{} = inserter, source) do
    inserter = %{inserter | source: source}

    so = Orientation.opposite(inserter.target_orientation)

    case {inserter.status, inserter.current_orientation, inserter.payload} do
      {:idle, ^so, payload} when payload == nil ->
        status = try_to_queue_as_consumer(source)
        %{inserter | status: status}

      _ ->
        inserter
    end
  end

  @doc """
  Set the inserter timer.
  """
  @spec set_timer(t(), :timer.tref() | nil) :: t()
  def set_timer(%Inserter{} = inserter, timer) do
    %{inserter | timer: timer}
  end

  @doc """
  Set inserter payload.
  """
  @spec set_payload(t(), Payload.t()) :: t()
  def set_payload(inserter, payload) do
    %{inserter | payload: payload}
  end

  @doc """
  Set inserter status.
  """
  @spec set_status(t(), status()) :: t()
  def set_status(%Inserter{} = inserter, status) do
    %{inserter | status: status}
  end

  @doc """
  Set inserter swing target.
  """
  @spec set_swing_target(t(), Orientation.t() | nil) :: t()
  def set_swing_target(%Inserter{} = inserter, target) do
    %{inserter | swing_target: target}
  end

  @doc """
  Set the target process for the inserter.
  """
  @spec set_target(t(), pid()) :: t()
  def set_target(%Inserter{} = inserter, target) do
    inserter = %{inserter | target: target}

    to = inserter.target_orientation

    case {inserter.status, inserter.current_orientation, inserter.payload} do
      {:idle, ^to, payload} when payload != nil ->
        put_in_target(inserter)

      _ ->
        inserter
    end
  end

  @spec start(t()) :: t()
  def start(inserter) do
    process_swung(inserter, inserter.current_orientation)
  end

  @doc """
  Process the swing command.
  """
  @spec process_swing(t()) :: t()
  def process_swing(%Inserter{current_orientation: co} = inserter) do
    # FIXME: when the inserter is already in motion, it should return an error.
    swing_target = Orientation.opposite(co)
    {:ok, timer} = :timer.send_after(inserter.swing_time, {:swung, swing_target})

    inserter
    |> set_status(:moving)
    |> set_swing_target(swing_target)
    |> set_timer(timer)
  end

  @doc """
  Process the swung command.
  """
  @spec process_swung(t(), Orientation.t()) :: t()
  def process_swung(inserter, target_orientation) do
    inserter
    |> set_timer(nil)
    |> set_orientation(target_orientation)
    |> set_swing_target(nil)
    |> exchange_resources()
  end

  @spec process_material_available(t()) :: t()
  def process_material_available(inserter) do
    inserter
    |> take_from_source()
  end

  @spec process_space_available(t()) :: t()
  def process_space_available(inserter) do
    inserter
    |> put_in_target()
  end

  def is_transporter(), do: true

  def server_module(), do: InserterServer

  def size(), do: {1, 1}

  @spec exchange_resources(t()) :: t()
  defp exchange_resources(%Inserter{source: source} = inserter) do
    if at_target?(inserter) do
      put_in_target(inserter)
    else
      status = try_to_queue_as_consumer(source)
      %{inserter | status: status}
    end
  end

  @spec take_from_source(Inserter.t()) :: Inserter.t()
  defp take_from_source(%Inserter{source: nil} = inserter) do
    set_status(inserter, :idle)
  end

  defp take_from_source(%Inserter{source: source} = inserter) do
    case ContainerServer.take(source) do
      {:error, :not_available} ->
        set_status(inserter, try_to_queue_as_consumer(source))

      {:ok, taken} ->
        inserter
        |> set_payload(taken)
        |> set_status(:moving)
    end
  end

  @spec try_to_queue_as_consumer(pid() | GenServer.name() | nil) :: status()
  defp try_to_queue_as_consumer(nil) do
    :idle
  end

  defp try_to_queue_as_consumer(machine_pid) when is_pid(machine_pid) do
    ContainerServer.enqueue_consumer(machine_pid)
    :waiting
  end

  defp try_to_queue_as_provider(nil, _) do
    :idle
  end

  defp try_to_queue_as_provider(machine_pid, payload) when is_pid(machine_pid) do
    ContainerServer.enqueue_provider(machine_pid, payload)
    :waiting
  end

  @spec put_in_target(Inserter.t()) :: Inserter.t()
  defp put_in_target(%Inserter{target: nil} = inserter) do
    set_status(inserter, :idle)
  end

  defp put_in_target(%Inserter{payload: {nil, nil}} = inserter) do
    # The inserter somehow moved to the target without having a payload. Move
    # to the source position to pick-up a new payload.
    set_status(inserter, :moving)
  end

  defp put_in_target(%Inserter{target: target, payload: payload} = inserter) do
    case ContainerServer.put(target, payload) do
      :ok ->
        inserter
        |> set_payload({nil, nil})
        |> set_status(:moving)

      # FIXME: we no longer distinguish a real error from an indication that
      # the container is full
      {:error, _reason} ->
        set_status(inserter, try_to_queue_as_provider(target, payload))
    end
  end
end

defimpl EM.Machines.Machine, for: EM.Machines.Inserter do
  alias EM.Machines.Inserter

  def bl(%Inserter{bl: bl}), do: bl

  def describe(inserter) do
    %{
      "name" => "inserter@#{inspect(inserter.bl)}",
      "type" => "inserter"
    }
  end

  defdelegate get_backup_state(inserter), to: Inserter

  def locations(%Inserter{bl: bl}), do: [bl]

  defdelegate rotate(inserter), to: Inserter

  def server_module(_inserter), do: EM.Machines.InserterServer

  def start(inserter), do: Inserter.start(inserter)

  def state_module(_inserter), do: Inserter

  def size(_inserter), do: {1, 1}

  defdelegate process_location_claimed(inserter, location, remote_machine_ref), to: Inserter

  def is_transporter(_inserter), do: true
end

defmodule EM.Machines.InserterServer do
  @moduledoc """
  Server module for Inserters.

  This provides the public inserter API and handles the callbacks.
  """

  alias EM.Machines.{Inserter, MachineServer}
  alias EM.Machines.Payload
  alias EM.World
  alias EM.World.{Location, Orientation}

  use GenServer
  use MachineServer, without: [:init]

  require Logger

  @impl GenServer
  def init(%Inserter{} = inserter) do
    GenServer.cast(self(), :create_backup)
    GenServer.cast(self(), :register_monitors)
    {:ok, inserter}
  end

  def is_transporter(), do: false

  def start_work(inserter_pid) do
    state = :sys.get_state(inserter_pid)
    send(inserter_pid, {:swung, state.current_orientation})
  end

  @spec get_target_orientation(pid()) :: Orientation.t()
  def get_target_orientation(inserter_pid) do
    GenServer.call(inserter_pid, :get_target_orientation)
  end

  @doc """
  See if `inserter` currently has a payload.
  """
  @spec has_payload?(pid()) :: boolean()
  def has_payload?(inserter) do
    GenServer.call(inserter, :has_payload)
  end

  @doc """
  Notify the `inserter` process that materials are available for pick-up.
  """
  @spec notify_material_available(pid()) :: :ok
  def notify_material_available(inserter) do
    GenServer.cast(inserter, :material_available)
  end

  @doc """
  Notify the `inserter` process that there is space to deposit resources.
  """
  @spec notify_space_available(pid()) :: :ok
  def notify_space_available(inserter) do
    GenServer.cast(inserter, :space_available)
  end

  @spec set_source(pid(), pid()) :: :ok
  def set_source(inserter, source) do
    GenServer.call(inserter, {:set_source, source})
  end

  @spec set_target(pid(), pid()) :: :ok
  def set_target(inserter, target) do
    GenServer.call(inserter, {:set_target, target})
  end

  @spec show_payload(pid()) :: Payload.t()
  def show_payload(inserter) do
    GenServer.call(inserter, :show_payload)
  end

  @doc """
  Swing from one orientation to another.
  """
  @spec swing(pid()) :: :ok
  def swing(inserter_pid) do
    GenServer.cast(inserter_pid, :swing)
  end

  def handle_call(:get_target_orientation, _from, inserter) do
    {:reply, inserter.target_orientation, inserter}
  end

  def handle_call(:has_payload, _from, %Inserter{} = inserter) do
    {:reply, Inserter.has_payload?(inserter), inserter}
  end

  def handle_call({:set_source, source}, _from, %Inserter{status: prev_status} = inserter) do
    inserter = Inserter.set_source(inserter, source)

    case {prev_status, inserter.status} do
      {:moving, :moving} -> nil
      {_, :moving} -> swing(self())
      _ -> nil
    end

    {:reply, :ok, inserter}
  end

  def handle_call({:set_target, target}, _from, %Inserter{status: prev_status} = inserter) do
    inserter = Inserter.set_target(inserter, target)

    case {prev_status, inserter.status} do
      {:moving, :moving} ->
        inserter

      {_, :moving} ->
        swing(self())
        inserter

      _ ->
        inserter
    end

    {:reply, :ok, inserter}
  end

  def handle_call(:show_payload, _from, %Inserter{payload: payload} = inserter) do
    {:reply, payload, inserter}
  end

  @impl GenServer
  def handle_cast(:register_monitors, inserter) do
    World.register_monitor(
      Location.get_neighbour_location(inserter.bl, inserter.target_orientation),
      self()
    )

    World.register_monitor(
      Location.get_neighbour_location(
        inserter.bl,
        Orientation.opposite(inserter.target_orientation)
      ),
      self()
    )

    {:noreply, inserter}
  end

  def handle_cast(:swing, inserter) do
    inserter = Inserter.process_swing(inserter)
    {:noreply, inserter}
  end

  def handle_cast(:material_available, inserter) do
    inserter = Inserter.process_material_available(inserter)
    conditional_swing(inserter)
    {:noreply, inserter}
  end

  def handle_cast(:space_available, inserter) do
    inserter = Inserter.process_space_available(inserter)
    conditional_swing(inserter)
    {:noreply, inserter}
  end

  @impl GenServer
  def handle_info({:swung, target_orientation}, inserter) do
    inserter = Inserter.process_swung(inserter, target_orientation)
    conditional_swing(inserter)
    {:noreply, inserter}
  end

  defp conditional_swing(%Inserter{status: status}) do
    if status == :moving do
      swing(self())
    end
  end
end
