defmodule EM.Machines.ResearchLab do
  @moduledoc """
  Lab implementation for Elixir Machines.

  ResearchLabs take research packs and turn them into knowledge (tm).
  """
  alias __MODULE__, as: ResearchLab

  alias EM.Machines.InserterServer
  alias EM.Machines.Utils.Recipe

  defstruct recipe: nil,
            storage: %{},
            timer: nil,
            consumer_q: :queue.new(),
            status: :idle,
            bl: {}

  @type status :: :idle | :waiting_for_resources | :processing

  @type t :: %ResearchLab{
          consumer_q: :queue.queue(),
          recipe: Recipe.t() | nil,
          storage: %{String.t() => integer()},
          timer: :timer.tref() | nil,
          status: status()
        }

  @spec has_excess?(t()) :: boolean()
  def has_excess?(%ResearchLab{storage: storage}) do
    # FIXME: should be checking if the lab is researching, and if not, whether
    # there are resources stored that are not needed for the next round of
    # production.
    storage != %{}
  end

  @spec try_research(t()) :: t()
  def try_research(lab) do
    # FIXME: do the actual implementation
    lab
  end

  ############################################################ 
  # Required for Machine implementation
  ############################################################ 
  def start(lab) do
    try_research(lab)
  end

  ############################################################ 
  # Required for Storage implementation
  ############################################################ 
  def dequeue(%ResearchLab{consumer_q: q} = lab, consumer) do
    if :queue.member(consumer, q) do
      l = List.delete(:queue.to_list(q), consumer)
      {:ok, %{lab | consumer_q: :queue.from_list(l)}}
    else
      {:error, :not_queued}
    end
  end

  def enqueue(%ResearchLab{consumer_q: q} = lab, consumer) do
    case {:queue.member(consumer, q), :queue.is_empty(q), has_excess?(lab)} do
      {true, _, _} ->
        {:ok, lab}

      {false, true, true} ->
        InserterServer.notify_material_available(consumer)
        {:ok, lab}

      _ ->
        {:ok, %{lab | consumer_q: :queue.in(consumer, q)}}
    end
  end

  def put(%ResearchLab{recipe: nil}, _) do
    {:error, :no_recipe_set}
  end

  def put(%ResearchLab{storage: storage, recipe: recipe} = lab, {type, amount}) do
    if type in Map.keys(recipe.resources_in) do
      {_, storage} = Map.get_and_update(storage, type, fn cur -> {cur, (cur || 0) + amount} end)

      lab
      |> Map.put(:storage, storage)
      |> try_research()

      # FIXME: should we check if these resources could be given to someone else?
      # FIXME: should we limit the amount of research packs that a lab can store? (Yes)

      {:ok, lab}
    else
      {:error, :not_needed}
    end
  end

  @doc """
  Try to take left over science packs.

  The science lab will hold on to enough resources to perform one more round of
  research, everything else it will gladly give away.
  """
  def take(%ResearchLab{storage: storage}) when storage == %{} do
    {:error, :not_available}
  end

  def take(%ResearchLab{recipe: recipe, storage: storage} = lab) do
    # FIXME: the recipe for science should not be set locally, but globally.
    left_over =
      storage
      |> Enum.filter(fn {resource_type, amount} ->
        Map.get(recipe.resources_in, resource_type, 0) < amount
      end)

    if length(left_over) > 0 do
      {storage, payload} =
        left_over
        |> Enum.random()
        |> (fn {resource_type, _amount} ->
              {Map.update!(storage, resource_type, &(&1 - 1)), {resource_type, 1}}
            end).()

      {:ok, %{lab | storage: storage}, payload}
    else
      {:error, :not_available}
    end
  end

  def take(%ResearchLab{recipe: recipe, storage: storage} = lab, {resource_type, amount}) do
    if Map.get(storage, resource_type, 0) - amount >
         Map.get(recipe.resources_in, resource_type, 0) do
      storage = Map.update!(storage, resource_type, &(&1 - amount))
      {:ok, %{lab | storage: storage}, {resource_type, amount}}
    else
      {:error, :not_available}
    end
  end

  def get_backup_state(lab) do
    %{recipe: lab.recipe}
  end
end

defmodule EM.Machines.ResearchLabServer do
  @moduledoc """
  Server module for ResearchLab.
  """

  use GenServer

  use EM.Machines.MachineServer
  use EM.Machines.ContainerServer
end

defimpl EM.Machines.Machine, for: EM.Machines.ResearchLab do
  alias EM.Machines.ResearchLab
  alias EM.World.Location

  def bl(lab), do: lab.bl

  def locations(%ResearchLab{bl: bl}), do: Location.gen_locations(bl, {3, 3})

  def describe(lab) do
    %{
      "name" => "research_lab@#{inspect(lab.bl)}",
      "type" => "research_lab"
    }
  end

  def process_location_claimed(lab, _location, _remote_machine_ref), do: lab

  def rotate(lab), do: lab

  def server_module(_), do: EM.Machines.ResearchLabServer

  defdelegate start(lab), to: ResearchLab

  def state_module(_), do: ResearchLab

  def size(_), do: {3, 3}

  def is_transporter(_), do: false

  defdelegate get_backup_state(lab), to: ResearchLab
end
