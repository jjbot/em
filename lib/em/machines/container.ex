defprotocol EM.Machines.Container do
  @moduledoc """
  Protocol definition for machines that can contain resources.

  Containers store and provide access to materials. These materials are stored in
  one or more `EM.Machines.Components.Storage` implementations. Machines that
  provide basic storage capacity usually only provide access to one storage type,
  while more complicated machines might separate incoming from outgoing resources
  and store these in different storage implementations.

  The Container protocol assumes access to resources and space goes through
  queues, which enable fair scheduling when multiple processes are vying for
  either of these. In the context of Containers, consumers are processes that want
  to take resources, while providers are processes that want to add resources to
  storage.

  Processes that queue at a container must support the `notify_material_available\1`
  and `notify_space_available\1` functions.
  """

  alias EM.Machines.Payload

  @doc """
  Remove a consumer from consumer queue.
  """
  @spec dequeue_consumer(t(), pid()) :: {:ok, t()} | {:error, atom()}
  def dequeue_consumer(machine, consumer)

  @doc """
  Remove a provider from the provider queue.
  """
  @spec dequeue_provider(t(), pid()) :: {:ok, t()} | {:error, atom()}
  def dequeue_provider(machine, consumer)

  @doc """
  Add a consumer to the consumer queue.
  """
  @spec enqueue_consumer(t(), pid()) :: {:ok, t()} | {:error, atom()}
  def enqueue_consumer(machine, provider)

  @doc """
  Add a provider to the provider queue.
  """
  @spec enqueue_provider(t(), pid(), Payload.t()) :: {:ok, t()} | {:error, atom()}
  def enqueue_provider(machine, provider, payload)

  @doc """
  Put `t:EM.Machines.Payload` into container.
  """
  @spec put(t(), Payload.t()) :: {:ok, t()} | {:error, atom()}
  def put(machine, payload)

  @doc """
  Take one random material from container.
  """
  @spec take(t()) :: {:ok, t(), Payload.t()} | {:error, atom()}
  def take(machine)

  @doc """
  Take `EM.Machines.Payload` from container.
  """
  @spec take(t(), Payload.t()) :: {:ok, t(), Payload.t()} | {:error, atom()}
  def take(machine, payload)
end

defmodule EM.Machines.ContainerServer do
  @moduledoc """
  StorageServer provides the base functionality to work with the storage any
  machine might contain.

  This module contains the public interface for interacting with the storage in
  machines and the GenServer callbacks that tie the machine implementation to the
  machine server.

  ## Interacting with ContainerServers
  Although ContainerServers support taking and putting resources directly, you
  should go through the queues to make sure other processes get the chance to
  take / put resources as well. You will then be informed that space or resources
  are available through `:material_available` and `:space_available` cast messages.
  These need to be supported by any machine that wants to interact with storage.

  The regular way of interacting with for example an `EM.Machines.Chest` would be:

      iex> alias EM.Machines.{Container, Chest, ChestServer}
      [EM.Machines.Container, EM.Machines.Chest, EM.Machines.ChestServer]

      iex> {:ok, chest_pid} = ChestServer.start_link(%Chest{storage: %{"stone" => 4}})
      {:ok, #PID<0.385.0>}

      iex> Container.enqueue_consumer(chest_pid)
      :ok

  Examples of machines that support this protocol: `EM.Machines.Chest`,
  `EM.Machines.Assembler` and `EM.Machines.ResearchLab`.
  """

  alias EM.Machines.{Container, Payload}

  defmacro __using__(_) do
    quote do
      def handle_call(:dequeue_consumer, {consumer_pid, _ref}, machine) do
        case Container.dequeue_consumer(machine, consumer_pid) do
          {:ok, machine} -> {:reply, :ok, machine}
          {:error, reason} -> {:reply, {:error, reason}, machine}
        end
      end

      def handle_call(:dequeue_provider, {consumer_pid, _ref}, machine) do
        case Container.dequeue_provider(machine, consumer_pid) do
          {:ok, machine} -> {:reply, :ok, machine}
          {:error, reason} -> {:reply, {:error, reason}, machine}
        end
      end

      def handle_call(:enqueue_consumer, {consumer_pid, _ref}, machine) do
        case Container.enqueue_consumer(machine, consumer_pid) do
          {:ok, machine} -> {:reply, :ok, machine}
          {:error, reason} -> {:reply, {:error, reason}, machine}
        end
      end

      def handle_call({:enqueue_provider, payload}, {consumer_pid, _ref}, machine) do
        case Container.enqueue_provider(machine, consumer_pid, payload) do
          {:ok, machine} -> {:reply, :ok, machine}
          {:error, reason} -> {:reply, {:error, reason}, machine}
        end
      end

      def handle_call({:put, payload}, _from, machine) do
        case Container.put(machine, payload) do
          {:ok, machine} -> {:reply, :ok, machine}
          {:error, reason} -> {:reply, {:error, reason}, machine}
        end
      end

      def handle_call(:take, _from, machine) do
        case Container.take(machine) do
          {:ok, machine, payload} -> {:reply, {:ok, payload}, machine}
          {:error, reason} -> {:reply, {:error, reason}, machine}
        end
      end

      def handle_call({:take, payload_request}, _from, machine) do
        case Container.take(machine, payload_request) do
          {:ok, machine, payload} -> {:reply, {:ok, payload}, machine}
          {:error, reason} -> {:reply, {:error, reason}, machine}
        end
      end
    end
  end

  @doc """
  Dequeue the current consumer process from the machine with the provied `machine_pid`.
  """
  @spec dequeue_consumer(pid()) :: :ok | {:error, atom()}
  def dequeue_consumer(machine_pid), do: GenServer.call(machine_pid, :dequeue_consumer)

  @doc """
  Dequeue the current provider process from the machine with the provied `machine_pid`.
  """
  @spec dequeue_provider(pid()) :: :ok | {:error, atom()}
  def dequeue_provider(machine_pid), do: GenServer.call(machine_pid, :dequeue_provider)

  @doc """
  Enqueue the current consumer process in the machine with the provided `machine_pid`.
  """
  @spec enqueue_consumer(pid()) :: :ok | {:error, atom()}
  def enqueue_consumer(machine_pid), do: GenServer.call(machine_pid, :enqueue_consumer)

  @doc """
  Enqueue the current provider process in the machine with the provided `machine_pid`.
  """
  @spec enqueue_provider(pid(), Payload.t()) :: :ok | {:error, atom()}
  def enqueue_provider(machine_pid, payload),
    do: GenServer.call(machine_pid, {:enqueue_provider, payload})

  @doc """
  Put `payload` in the machine with the provided `pid`.
  """
  @spec put(pid(), Payload.t()) :: :ok | {:error, atom()}
  def put(machine_pid, payload), do: GenServer.call(machine_pid, {:put, payload})

  @doc """
  Try to take any resource from the machine with the provided `pid`.
  """
  @spec take(pid()) :: {:ok, Payload.t()} | {:error, atom()}
  def take(machine_pid), do: GenServer.call(machine_pid, :take)

  @doc """
  Try to take `payload` from the machine with the provided `pid`.
  """
  @spec take(pid(), Payload.t()) :: {:ok, Payload.t()} | {:error, atom()}
  def take(machine_pid, payload_request),
    do: GenServer.call(machine_pid, {:take, payload_request})
end

defmodule EM.Machines.ContainerImpl do
  @moduledoc """
  Helper module that gives basic implementations for Container operations.

  As machines will often rely on the same approach to enable functionality (for
  example enqueing and dequeing processes), this module gives the basic
  implementation to get this done.
  """
  alias EM.Machines.Components.Storage
  alias EM.Machines.{Container, InserterServer}
  alias EM.Machines.Payload

  @doc """
  Remove `consumer` from `queue`.
  """
  @spec dequeue(:queue.queue(), pid()) :: {:ok, :queue.queue()} | {:error, atom()}
  def dequeue(queue, consumer) do
    if :queue.member(consumer, queue) do
      l = List.delete(:queue.to_list(queue), consumer)
      {:ok, :queue.from_list(l)}
    else
      {:error, :not_queued}
    end
  end

  @doc """
  Add `consumer` to `queue`.
  """
  @spec enqueue_consumer(:queue.queue(), Container.t(), pid()) ::
          {:ok, :queue.queue()} | {:error, atom()}
  def enqueue_consumer(q, storage, consumer) do
    case {:queue.member(consumer, q), :queue.is_empty(q), Storage.is_empty?(storage)} do
      {true, _, _} ->
        # Process has already been queued, not going to add again
        {:error, :already_queued}

      {false, true, false} ->
        # Process is not queued, there are no other processes waiting and we
        # have materials available

        InserterServer.notify_material_available(consumer)
        {:ok, q}

      _ ->
        # in all other cases, add process to queue
        {:ok, :queue.in(consumer, q)}
    end
  end

  def enqueue_provider(q, storage, provider, {type, amount} = _payload) do
    case {:queue.member(provider, q), :queue.is_empty(q), Storage.space_for(storage, type)} do
      {true, _, _} ->
        {:error, :already_queued}

      {false, true, available_space} when available_space >= amount ->
        InserterServer.notify_space_available(provider)
        {:ok, q}

      _ ->
        {:ok, :queue.in(provider, q)}
    end
  end

  @doc """
  Put `payload` into `storage`.
  """
  @spec put(Container.t(), Payload.t()) :: {:ok, Container.t()} | {:error, atom()}
  def put(storage, payload) do
    Container.put(storage, payload)
  end

  @doc """
  Take `payload` from storage.
  """
  @spec take(Container.t(), Payload.t()) ::
          {:ok, Container.t(), Payload.t()}
          | {:error, :not_available}
  def take(storage, payload) do
    Container.take(storage, payload)
  end
end
