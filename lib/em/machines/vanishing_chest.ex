defmodule EM.Machines.VanishingChest do
  @moduledoc """
  Vanishing chests move resources from one chest to another. What is put in one
  ends up in the other. This only works in pairs of chests.
  """

  alias __MODULE__, as: VanishingChest
  alias EM.Machines.VanishingChestServer
  alias EM.Machines.{Chest, Container, ContainerImpl}
  alias EM.Machines.Payload
  alias EM.Machines.Components.Storage
  alias EM.World.Location

  defstruct backup_server: nil,
            bl: {},
            consumer_q: :queue.new(),
            opposite: nil,
            provider_q: :queue.new(),
            storage: %{}

  @type t :: %VanishingChest{
          backup_server: GenServer.name() | nil,
          bl: Location.t(),
          storage: Container.t(),
          consumer_q: :queue.queue(),
          provider_q: :queue.queue(),
          opposite: GenServer.name() | pid() | nil
        }

  ############################################################
  # Machine functions
  ############################################################

  def get_backup_state(chest), do: [bl: chest.bl]

  @spec dequeue_consumer(t(), pid()) :: {:ok, t()} | {:error, :not_queued}
  def dequeue_consumer(%VanishingChest{consumer_q: q} = chest, consumer) do
    case ContainerImpl.dequeue(q, consumer) do
      {:ok, queue} -> {:ok, %{chest | consumer_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  @spec dequeue_provider(t(), pid()) :: {:ok, t()} | {:error, :not_queued}
  def dequeue_provider(%VanishingChest{provider_q: q} = chest, consumer) do
    case ContainerImpl.dequeue(q, consumer) do
      {:ok, queue} -> {:ok, %{chest | provider_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  @spec enqueue_consumer(t(), pid()) :: {:ok, t()} | {:error, :already_queued}
  def enqueue_consumer(%VanishingChest{consumer_q: q, storage: storage} = chest, consumer) do
    case ContainerImpl.enqueue_consumer(q, storage, consumer) do
      {:ok, queue} -> {:ok, %{chest | consumer_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  # FIXME: the tests pass, but this does not yet take into account that it should
  # actually be queued on the other side
  @spec enqueue_provider(t(), pid(), Payload.t()) ::
          {:ok, t()} | {:error, :already_queued}
  def enqueue_provider(
        %VanishingChest{provider_q: q, storage: storage} = chest,
        provider,
        payload
      ) do
    case ContainerImpl.enqueue_provider(q, storage, provider, payload) do
      {:ok, queue} -> {:ok, %{chest | provider_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  @spec put(t(), Payload.t()) :: {:ok, t()}
  def put(%VanishingChest{opposite: opposite} = chest, payload) do
    VanishingChestServer.put_remote(opposite, payload)
    {:ok, chest}
  end

  @spec take(t()) ::
          {:ok, t(), Payload.t()}
          | {:error, :not_available}
  def take(%VanishingChest{storage: storage}) when storage == %{}, do: {:error, :not_available}

  def take(%VanishingChest{storage: storage} = chest) do
    {type, _stored_amount} = storage |> Enum.random()
    take(chest, {type, 1})
  end

  def take(%VanishingChest{storage: storage} = chest, payload) do
    case Storage.take(storage, payload) do
      {:ok, storage, payload} ->
        chest =
          chest
          |> Map.put(:storage, storage)
          |> Chest.notify_queued_provider()

        {:ok, chest, payload}

      {:error, message} ->
        {:error, message}
    end
  end

  def set_opposite(%VanishingChest{} = chest, opposite) do
    {:ok, %{chest | opposite: opposite}}
  end

  def put_remote(%VanishingChest{storage: storage} = chest, payload) do
    case Storage.put(storage, payload) do
      {:ok, storage} ->
        chest =
          chest
          |> Map.put(:storage, storage)
          |> Chest.notify_queued_consumer()

        {:ok, chest}

      {:error, reason} ->
        {:error, reason}
    end
  end
end

defimpl EM.Machines.Machine, for: EM.Machines.VanishingChest do
  alias EM.Machines.VanishingChest

  def bl(%VanishingChest{bl: bl}), do: bl

  def describe(chest) do
    %{
      "name" => "vanishing_chest@#{inspect(chest.bl)}",
      "type" => "vanishing_chest"
    }
  end

  defdelegate get_backup_state(chest), to: VanishingChest

  def locations(%VanishingChest{bl: bl}), do: [bl]

  def process_location_claimed(chest, _location, _remote_machine_ref), do: chest

  def rotate(chest), do: chest

  def server_module(_chest), do: EM.Machines.VanishingChestServer

  def start(chest), do: chest

  def state_module(_chest), do: EM.Machines.VanishingChest

  def size(_chest), do: {1, 1}

  def is_transporter(_chest), do: false
end

defimpl EM.Machines.Container, for: EM.Machines.VanishingChest do
  alias EM.Machines.VanishingChest

  defdelegate dequeue_consumer(chest, consumer), to: VanishingChest

  defdelegate dequeue_provider(chest, consumer), to: VanishingChest

  defdelegate enqueue_consumer(chest, consumer), to: VanishingChest

  defdelegate enqueue_provider(chest, provider, payload), to: VanishingChest

  defdelegate put(chest, payload), to: VanishingChest

  defdelegate take(chest), to: VanishingChest

  defdelegate take(chest, payload), to: VanishingChest
end

defmodule EM.Machines.VanishingChestServer do
  @moduledoc """
  Server module for VanishingChest, provides public API and handles callbacks.
  """
  use GenServer

  use EM.Machines.MachineServer
  use EM.Machines.ContainerServer

  alias EM.Machines.VanishingChest

  def set_opposite(pid, opposite) do
    GenServer.call(pid, {:set_opposite, opposite})
  end

  def put_remote(pid, payload) do
    GenServer.call(pid, {:put_remote, payload})
  end

  def handle_call({:set_opposite, opposite}, _from, state) do
    {:ok, state} = VanishingChest.set_opposite(state, opposite)
    {:reply, :ok, state}
  end

  def handle_call({:put_remote, payload}, _from, state) do
    {:ok, state} = VanishingChest.put_remote(state, payload)
    {:reply, :ok, state}
  end
end
