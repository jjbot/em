defmodule TestTube do
  @moduledoc false

  use GenServer

  @impl GenServer
  def init(_) do
    {:ok, %{}}
  end

  def add_monitor(tt_pid, to_monitor_pid) do
    GenServer.call(tt_pid, {:add_monitor, to_monitor_pid})
  end

  def start_link(name) do
    GenServer.start_link(__MODULE__, nil, name: name)
  end

  @impl GenServer
  def handle_call({:add_monitor, to_monitor_pid}, _from, state) do
    ref = Process.monitor(to_monitor_pid)
    {:reply, :ok, Map.put(state, ref, to_monitor_pid)}
  end

  @impl GenServer
  def handle_info({:DOWN, ref, type, pid, status}, state) do
    IO.puts("Heard you're down at: #{inspect(Time.utc_now())}")
    IO.puts("Ref: #{inspect(ref)}")
    IO.puts("Type: #{inspect(type)}")
    IO.puts("pid: #{inspect(pid)}")
    IO.puts("ref to pid: #{inspect(state[ref])}")
    IO.puts("Status: #{inspect(status)}")
    {:noreply, state}
  end
end
