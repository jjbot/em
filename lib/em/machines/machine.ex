defprotocol EM.Machines.Machine do
  @moduledoc """
  Basic functions every machine needs to support.

  Machines need to expose a number of functions relating to basic functionality:
  - their location and orientation
  - implemented functionality
  - providing backup state that gets used in the event of a crash

  """

  alias EM.World.Location

  @doc """
  Get the bottom left corner of a machines' position.
  """
  @spec bl(t()) :: Location.t()
  def bl(machine)

  @spec describe(t()) :: %{String.t() => any()}
  def describe(machine)

  @doc """
  Get the state to back-up.

  This state needs to be compatible with how a machine gets started. It's up to
  the machine to implement this in such a way that reboot works appropriately.
  """
  @spec get_backup_state(t()) :: any()
  def get_backup_state(machine)

  @doc """
  Get the list of locations the machine occupies.

  ## Examples
      # Simulate the creation of an Assembler on location {2, 3}, assemblers
      # occupy 3x3 cells.  Keep in mind that machines should normally be created
      # through the World module.
      iex> m = %Assembler{bl: {2, 3}}
      iex> Machine.locations(m)
      [{2, 3}, {2, 4}, {2, 5}, {3, 3}, {3, 4}, {3, 5}, {4, 3}, {4, 4}, {4, 5}]

  """
  @spec locations(t) :: [Location.t()]
  def locations(machine)

  @doc """
  Rotate the machine clockwise.
  """
  @spec rotate(t()) :: t()
  def rotate(machine)

  @doc """
  Get the server module associated with the machine.
  """
  @spec server_module(t()) :: module()
  def server_module(machine)

  @doc """
  Start the machine.
  """
  @spec start(t()) :: t()
  def start(machine)

  @doc """
  Get the state module associated with the machine.
  """
  @spec state_module(t()) :: module()
  def state_module(machine)

  @doc """
  Get the size of the machine.
  """
  @spec size(t()) :: {integer(), integer()}
  def size(machine)

  @doc """
  A new machine was placed on a location this machine requested to be monitored.
  """
  @spec process_location_claimed(t(), Location.t(), GenServer.name()) :: t()
  def process_location_claimed(machine, location, remote_machine_ref)

  @doc """
  Check if the machine is a transporter.
  """
  @spec is_transporter(t()) :: boolean()
  def is_transporter(machine)
end

defmodule EM.Machines.MachineServer do
  @moduledoc """
  MachineServer provides the basic machine interaction functions.

  Each machine server module should `use` this to provide the basic `handle_call`
  and `handle_cast` functions every machine is expected to support. This also
  requires the `EM.Machines.Machine` protocol to be implemented for the
  corresponding machine state module.

  MachineServer supports not associating all functions with the server module that
  uses it by adding the `without` parameter. This excludes one or more of the
  required functions, leaving it up to the server to provide its own
  implementation. For now, the `init` function is the only one that can be left
  out.

      defmodule WithoutInit do
        use EM.Machines.MachineServer, without: [:init]
        ...
      end

  All functions in this module expect to get a PID or a `t:GenServer.name`.
  """

  alias EM.Machines.Machine

  defmacro __using__(args) do
    without = Keyword.get(args, :without, [])

    quote do
      if unquote(:init not in without) do
        @impl true
        def init(machine) do
          {:ok, machine}
        end
      end

      def start_link(machine) do
        name = {:machine, machine.bl}
        {:ok, pid} = GenServer.start_link(__MODULE__, machine, name: {:via, :syn, name})
        {:ok, pid, name}
      end

      @impl true
      def handle_call(:bl, _from, machine) do
        {:reply, Machine.bl(machine), machine}
      end

      def handle_call(:describe, _from, machine) do
        {:reply, Machine.describe(machine), machine}
      end

      def handle_call(:get_backup_state, _from, machine) do
        {:reply, Machine.get_backup_state(machine), machine}
      end

      def handle_call(:locations, _from, machine) do
        {:reply, Machine.locations(machine), machine}
      end

      def handle_call(:rotate, _from, machine) do
        {:reply, :ok, Machine.rotate(machine)}
      end

      def handle_call(:start, _from, machine) do
        {:reply, :ok, Machine.start(machine)}
      end

      def handle_call(:size, _from, machine) do
        {:reply, Machine.size(machine), machine}
      end

      def handle_call(:is_transporter, _from, machine) do
        {:reply, Machine.is_transporter(machine), machine}
      end

      def handle_call({:process_location_claimed, location, machine_ref}, _from, machine) do
        machine = Machine.process_location_claimed(machine, location, machine_ref)
        {:reply, :ok, machine}
      end

      @impl true
      def handle_cast(:create_backup, machine) do
        case machine.backup_server do
          nil ->
            :ok

          reference ->
            MachineSupervisor.update_restart_arguments(
              reference,
              Machine.get_backup_state(machine)
            )

            # TileServer.set_recovery_state(reference, Machine.get_backup_state(machine))
        end

        {:noreply, machine}
      end

      def handle_cast({:process_location_claimed, location, machine_ref}, machine) do
        machine = Machine.process_location_claimed(machine, location, machine_ref)
        {:noreply, machine}
      end
    end
  end

  @doc """
  Get the bottom left corner of a machine.
  """
  def bl(machine_pid), do: GenServer.call(machine_pid, :bl)

  def describe(machine_pid), do: GenServer.call(machine_pid, :describe)

  @doc """
  Request the machine to back-up its vital information.

  As this is implemented using a cast to the machine, the function returns
  immediately. However, the action might not complete. This can for example happen
  when the machine doesn't have a backup server set.
  """
  def create_backup(machine_pid), do: GenServer.cast(machine_pid, :create_backup)

  @doc """
  Get the important bits of the machine state so it can be stored somewhere else.
  """
  def get_backup_state(machine_pid), do: GenServer.call(machine_pid, :get_backup_state)

  @doc """
  Get the locations (cells) the machine is occupying.
  """
  def locations(machine_pid), do: GenServer.call(machine_pid, :locations)

  @doc """
  Check the provided machine is a transporter.
  """
  def is_transporter(machine_pid), do: GenServer.call(machine_pid, :is_transporter)

  def process_location_claimed(machine_ref, location, remote_machine_ref) do
    GenServer.cast(machine_ref, {:process_location_claimed, location, remote_machine_ref})
  end

  @doc """
  Rotate the machine clockwise.
  """
  def rotate(machine_pid), do: GenServer.call(machine_pid, :rotate)

  @doc """
  Get the size of the machine.
  """
  def size(machine_pid), do: GenServer.call(machine_pid, :size)

  @doc """
  Start the machine.
  """
  def start(machine_pid), do: GenServer.call(machine_pid, :start)
end
