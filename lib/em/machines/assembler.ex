defmodule EM.Machines.Assembler do
  @moduledoc """
  Assembler implementation for Elixir Machines.

  Assemblers take input materials and convert them into output materials.
  """

  alias __MODULE__, as: Assembler
  alias EM.Machines.Utils.Recipe
  alias EM.Machines.Components.{Payload, Storage, TypedStorage}
  alias EM.Machines.{InserterServer, ContainerImpl}
  alias EM.Machines.Payload
  alias EM.World.{Location, MaterialLibrary, ProductionStats}

  require Logger

  defstruct backup_server: nil,
            bl: {},
            consumer_q: :queue.new(),
            provider_q: :queue.new(),
            recipe: nil,
            status: :idle,
            storage_in: %TypedStorage{},
            storage_out: %TypedStorage{},
            timer: nil

  @typedoc """
  An assembler can be in a fixed number of states, these are:
  - :idle: the machine does not have a recipe and therefore nothing to do.
  - :waiting_for_resources: the assembler currently does not have the required
    resources to begin processing, and is wating for more.
  - :processing: input materials are being processed into output materials.
  - :full: the storage of the assembler is full, it can therefore not continue
    producing more.
  """
  @type status :: :idle | :waiting_for_resources | :processing | :full

  @typedoc """
  The Assembler type. Fields:
  - bl: bottom left corner of the assembler.
  - consumer_q: a `:queue.queue` keeping track of processes that want to take
    resources from the assembler.
  - provider_q: a `:queue.queue` keeping track of processes that want to deposit
    resource in the assembler.
  - recipe: the `EM.Machines.Utils.Recipe` the assembler will be working no.
  - status: the current status of the assembler. Possible values are given
    by `EM.Machines.Assembler.status`.
  - storage_in: the input materials currently stored in the assembler, these
    can only be materials required by the currently set recipe. Gets set to
    appropriate resource types once a recipe gets set.
  - storage_out: currently stored output resources. Gets set to the appropriate
    resource types when a recipe gets set.
  - timer: the timer for anything the assembler is currently producing. Nil
    when the assembler is not producing anything.
  """
  @type t :: %Assembler{
          backup_server: GenServer.name() | nil,
          bl: Location.t(),
          consumer_q: :queue.queue(),
          provider_q: :queue.queue(),
          recipe: Recipe.t() | nil,
          status: status(),
          storage_in: TypedStorage.t(),
          storage_out: TypedStorage.t(),
          timer: :timer.tref() | nil
        }

  ############################################################
  # Machine functions
  ############################################################
  def get_backup_state(%Assembler{} = assembler) do
    [
      bl: assembler.bl,
      recipe: assembler.recipe
    ]
  end

  @doc """
  Remove `consumer` from `assembler`'s consumer queue. If the consumer is not in
  the queue, {:error, :not_queued} will be returned.
  """
  @spec dequeue_consumer(t(), pid()) :: {:ok, t()} | {:error, :not_queued}
  def dequeue_consumer(%Assembler{consumer_q: q} = assembler, consumer) do
    case ContainerImpl.dequeue(q, consumer) do
      {:ok, queue} -> {:ok, %{assembler | consumer_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  @doc """
  Remove `provider` from `assembler`'s provider queue. If the consumer is not in
  the queue, {:error, :not_queued} will be returned.
  """
  @spec dequeue_provider(t(), pid()) :: {:ok, t()} | {:error, :not_queued}
  def dequeue_provider(%Assembler{provider_q: q} = assembler, consumer) do
    case ContainerImpl.dequeue(q, consumer) do
      {:ok, queue} -> {:ok, %{assembler | provider_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  @doc """
  Add a `consumer` to the `assembler`'s queue. If the assembler has resources
  available and the queue is empty, the consumer will be notified immediately
  that it can take resources. If the `consumer` is already in the queue, the
  request will be ignored.
  """
  @spec enqueue_consumer(t(), pid()) :: {:ok, t()}
  def enqueue_consumer(%Assembler{consumer_q: q} = assembler, consumer) do
    # FIXME: this should be using StorageImpl.enqueue, which currently is difficult
    # because storage_out is a tuple, not a map.
    case {:queue.member(consumer, q), :queue.is_empty(q), product_available?(assembler)} do
      {true, _, _} ->
        {:ok, assembler}

      {false, true, true} ->
        InserterServer.notify_material_available(consumer)
        {:ok, assembler}

      _ ->
        {:ok, %{assembler | consumer_q: :queue.in(consumer, q)}}
    end
  end

  @spec enqueue_provider(t(), pid(), Payload.t()) ::
          {:ok, t()} | {:error, :already_queued}
  def enqueue_provider(
        %Assembler{provider_q: q, storage_in: storage} = assembler,
        provider,
        payload
      ) do
    case ContainerImpl.enqueue_provider(q, storage, provider, payload) do
      {:ok, queue} -> {:ok, %{assembler | provider_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  def is_transporter(), do: false

  def size(), do: {3, 3}

  @doc """
  Put an `amount` of `type` in the `assembler's` storage. Fails when either the
  resource is not needed or the provided amount cannot be stored.
  """
  @spec put(t(), Payload.t()) :: {:ok, t()} | {:error, atom()}
  def put(%Assembler{recipe: nil}, _) do
    {:error, :no_recipe_set}
  end

  def put(%Assembler{storage_in: storage_in} = assembler, payload) do
    case Storage.put(storage_in, payload) do
      {:ok, storage_in} ->
        assembler =
          assembler
          |> Map.put(:storage_in, storage_in)
          |> try_production()

        {:ok, assembler}

      {:error, reason} ->
        {:error, reason}
    end
  end

  @doc """
  Add `payload` to `assembler`.
  """
  @spec put!(t(), Payload.t()) :: t()
  def put!(%Assembler{} = assembler, to_put) do
    {:ok, assembler} = put(assembler, to_put)
    assembler
  end

  @doc """
  Add materials to the Assembler output storage.
  """
  @spec put_output(t(), Payload.t()) :: {:ok, t()} | {:error, atom()}
  def put_output(%Assembler{storage_out: storage_out} = assembler, payload) do
    case Storage.put(storage_out, payload) do
      {:ok, storage_out} ->
        assembler =
          assembler
          |> Map.put(:storage_out, storage_out)
          |> notify_queued_consumer()

        {:ok, assembler}

      # FIXME: not sure if this is the right behaviour; we're now basically
      # throwing away the created output, should we add a small material buffer
      # that would allow us to keep these resources around?
      {:error, :not_enough_space} ->
        assembler =
          assembler
          |> Map.put(:status, :full)
          |> notify_queued_consumer()

        {:ok, assembler}

      {:error, reason} ->
        {:error, reason}
    end
  end

  @spec put_output!(t(), Payload.t()) :: t()
  def put_output!(%Assembler{} = assembler, to_put) do
    case put_output(assembler, to_put) do
      {:ok, assembler} ->
        assembler

      # FIXME: This should deal with the case that the resources can't be added
      # because the storage is already full
      # Also, does the :resource_mismatch ever occur?

      {:error, _reason} ->
        raise "Trying to add incorrect resources to assembler output"
    end
  end

  @spec start(t()) :: t()
  def start(assembler) do
    try_production(assembler)
  end

  @doc """
  Take resources from the assembler output storage.
  """
  @spec take(t()) :: {:ok, t(), Payload.t()} | {:error, atom()}
  def take(%Assembler{storage_out: storage_out} = assembler) do
    case Storage.take(storage_out) do
      {:ok, container, payload} ->
        {:ok, %{assembler | storage_out: container}, payload}

      {:error, reason} ->
        {:error, reason}
    end
  end

  @doc """
  Try to take a specific type from the assembler output storage.
  """
  @spec take(t(), Payload.t()) ::
          {:ok, t(), Payload.t()}
          | {:error, :not_available}
  def take(%Assembler{storage_out: storage_out} = assembler, payload) do
    case Storage.take(storage_out, payload) do
      {:ok, storage_out, payload} ->
        assembler =
          assembler
          |> Map.put(:storage_out, storage_out)
          # This doesn't make sense... why should anybody but the assembler
          # itself be notified? The only thing that could happen is that there
          # is a production run that wasn't able to store resources. These might
          # now be added, and a new production run can be started.
          |> notify_queued_consumer()

        {:ok, assembler, payload}

      {:error, reason} ->
        {:error, reason}
    end
  end

  @doc """
  Check if `assembler` has output materials available.
  """
  @spec product_available?(t()) :: boolean()
  def product_available?(%Assembler{storage_out: storage_out}) do
    !Storage.is_empty?(storage_out)
  end

  @doc """
  Check if the `assembler` has everything it needs to start production.
  """
  @spec ready_for_production?(t()) :: boolean | {boolean(), atom()}
  def ready_for_production?(%Assembler{status: status} = assembler) do
    case {status, sufficient_resources_present?(assembler)} do
      {:idle, true} -> true
      {:waiting_for_resources, true} -> true
      {:full, _} -> {false, :full}
      {:processing, _} -> {false, :busy}
      {_, false} -> {false, :no_resources}
    end
  end

  @doc """
  Check if the `assembler` has input resources available.
  """
  @spec sufficient_resources_present?(t()) :: boolean() | {:error, atom()}
  def sufficient_resources_present?(%Assembler{recipe: nil}) do
    {:error, :no_recipe_set}
  end

  def sufficient_resources_present?(%Assembler{recipe: recipe, storage_in: storage_in}) do
    storage_in_map = Storage.to_map(storage_in)

    recipe.resources_in
    |> Enum.map(fn {type, amount} -> Map.get(storage_in_map, type, 0) >= amount end)
    |> Enum.all?()
  end

  @spec set_status!(t(), status()) :: t()
  def set_status!(%Assembler{} = assembler, status) do
    %{assembler | status: status}
  end

  @spec set_recipe(t(), Recipe.t() | nil) ::
          {:ok, t(), %{String.t() => pos_integer()}} | {:error, atom()}
  def set_recipe(%Assembler{recipe: %Recipe{name: name}} = assembler, %Recipe{name: name}) do
    {:ok, assembler, %{}}
  end

  def set_recipe(%Assembler{} = assembler, recipe) do
    {assembler, left_over_resources} = reset_resources(assembler)

    assembler =
      assembler
      |> cancel_timer()
      |> Map.put(:recipe, recipe)
      |> init_storage_in()
      |> init_storage_out()

    {:ok, assembler, left_over_resources}
  end

  @spec try_production(t()) :: t()
  def try_production(%Assembler{} = assembler) do
    case try_production_impl(assembler) do
      {:ok, assembler} -> assembler
      {:error, _reason, assembler} -> assembler
    end
  end

  @spec finish_production!(t()) :: t()
  def finish_production!(%Assembler{recipe: recipe} = assembler) do
    ProductionStats.add(recipe.resource_out)

    assembler
    |> put_output!(recipe.resource_out)
    |> Map.put(:status, :idle)
    |> Map.put(:timer, nil)
    |> notify_queued_consumer()
    |> try_production()
  end

  defp notify_queued_consumer(%Assembler{consumer_q: q, storage_out: storage_out} = assembler) do
    if Storage.is_empty?(storage_out) do
      assembler
    else
      case :queue.out(q) do
        {{:value, consumer}, q} ->
          InserterServer.notify_material_available(consumer)
          %{assembler | consumer_q: q}

        {:empty, _q} ->
          assembler
      end
    end
  end

  defp notify_queued_provider(assembler) do
    # FIXME: this is the exact same implementation as in Chest. Should move this
    # to a shared library
    case :queue.out(assembler.provider_q) do
      {{:value, provider}, q} ->
        InserterServer.notify_space_available(provider)
        %{assembler | provider_q: q}

      {:empty, _q} ->
        assembler
    end
  end

  @spec try_production_impl(t()) :: {:ok, t()} | {:error, atom(), t()}
  defp try_production_impl(%Assembler{status: :processing} = assembler) do
    {:ok, assembler}
  end

  defp try_production_impl(%Assembler{status: :full} = assembler) do
    {:error, :full, assembler}
  end

  defp try_production_impl(%Assembler{recipe: nil} = assembler) do
    {:error, :no_recipe_set, assembler}
  end

  defp try_production_impl(%Assembler{} = assembler) do
    if sufficient_resources_present?(assembler) do
      {:ok, start_production(assembler)}
    else
      {:error, :insufficient_resources, %{assembler | status: :waiting_for_resources}}
    end
  end

  @spec cancel_timer(t()) :: t()
  defp cancel_timer(%Assembler{} = assembler) do
    case assembler.timer do
      nil ->
        assembler

      timer ->
        :timer.cancel(timer)
        %{assembler | timer: nil}
    end
  end

  @spec reset_resources(t()) :: {t(), %{String.t() => pos_integer()}}
  defp reset_resources(assembler) do
    left_over_resources =
      Map.merge(
        Storage.to_map(assembler.storage_in),
        Storage.to_map(assembler.storage_out),
        fn _k, v1, v2 -> v1 + v2 end
      )
      |> Enum.reject(fn {_k, v} -> v == 0 end)
      |> Enum.reduce(%{}, fn {k, v}, acc -> Map.put(acc, k, v) end)

    {%{assembler | storage_in: %TypedStorage{}, storage_out: %TypedStorage{}},
     left_over_resources}
  end

  defp init_storage_in(%Assembler{recipe: nil} = assembler) do
    %{assembler | storage_in: %TypedStorage{}}
  end

  defp init_storage_in(%Assembler{recipe: recipe} = assembler) do
    {:ok, container} =
      recipe.resources_in
      |> Map.keys()
      |> MaterialLibrary.get_stack_sizes()
      |> TypedStorage.init_empty()

    %{assembler | storage_in: container}
  end

  @spec init_storage_out(t()) :: t()
  defp init_storage_out(%Assembler{recipe: nil} = assembler) do
    %{assembler | storage_out: %TypedStorage{}}
  end

  defp init_storage_out(%Assembler{recipe: %Recipe{resource_out: {resource_type, _}}} = assembler) do
    {:ok, stack_size} = MaterialLibrary.get_stack_size(resource_type)
    {:ok, storage_out} = TypedStorage.init_empty(%{resource_type => stack_size})
    %{assembler | storage_out: storage_out}
  end

  @spec start_production(t()) :: t()
  defp start_production(%Assembler{recipe: recipe, storage_in: storage_in} = assembler) do
    storage_in =
      Enum.reduce(
        recipe.resources_in,
        storage_in,
        # FIXME: there must be an easier way to do this. Introduce a "remove" function?
        fn to_take, acc ->
          {:ok, acc, _} = Storage.take(acc, to_take)
          acc
        end
      )

    assembler = notify_queued_provider(assembler)
    {:ok, timer} = :timer.send_after(recipe.time, :production_finished)
    %{assembler | storage_in: storage_in, status: :processing, timer: timer}
  end
end

defmodule EM.Machines.AssemblerServer do
  @moduledoc """
  Server module for Assembler.
  """

  alias EM.Machines.Assembler

  use GenServer

  use EM.Machines.MachineServer
  use EM.Machines.ContainerServer
  use EM.Machines.ConstructorServer

  @impl GenServer
  def handle_info(:production_finished, %Assembler{} = assembler) do
    assembler =
      assembler
      |> Assembler.finish_production!()
      |> Assembler.try_production()

    {:noreply, assembler}
  end
end

defimpl EM.Machines.Machine, for: EM.Machines.Assembler do
  alias EM.Machines.Assembler
  alias EM.World.Location

  def bl(%Assembler{bl: bl}), do: bl

  def describe(assembler) do
    %{
      "name" => "assembler@#{inspect(assembler.bl)}",
      "status" => assembler.status,
      "type" => "assembler"
    }
  end

  defdelegate get_backup_state(assembler), to: Assembler

  def locations(%Assembler{bl: bl}), do: Location.gen_locations(bl, {3, 3})

  def process_location_claimed(assembler, _location, _remote_machine_ref), do: assembler

  def rotate(assembler), do: assembler

  def server_module(_assembler), do: EM.Machines.AssemblerServer

  defdelegate start(assembler), to: Assembler

  def state_module(_assembler), do: Assembler

  def size(_assembler), do: {3, 3}

  def is_transporter(_assembler), do: false
end

defimpl EM.Machines.Container, for: EM.Machines.Assembler do
  alias EM.Machines.Assembler

  defdelegate dequeue_consumer(assembler, consumer), to: Assembler

  defdelegate dequeue_provider(assembler, consumer), to: Assembler

  defdelegate enqueue_consumer(assembler, consumer), to: Assembler

  defdelegate enqueue_provider(assembler, consumer, payload), to: Assembler

  defdelegate put(assembler, payload), to: Assembler

  defdelegate take(assembler), to: Assembler

  defdelegate take(assembler, payload), to: Assembler
end

defimpl EM.Machines.Constructor, for: EM.Machines.Assembler do
  alias EM.Machines.Assembler

  defdelegate set_recipe(assembler, recipe), to: Assembler
end
