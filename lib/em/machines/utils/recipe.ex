defmodule EM.Machines.Utils.Recipe do
  @moduledoc """
  Recipes indicate how input resources can be turned into output resources.

  Recipes are used by `EM.Machines.Assembler` to know how to produce materials.
  """

  alias __MODULE__, as: Recipe

  # FIXME: leaving out resources_out as the specification for the json and
  # the struct at this moment differ.
  @enforce_keys [:resources_in, :name, :time]
  defstruct resources_in: %{}, resource_out: {nil, nil}, name: nil, time: 60

  @typedoc """
  Recipes contain all the information needed to show producers how to turn input
  resources into output resources. Fields:
  - resoures_in: the resource types and amounts required
  - resource_out: the resource type and amount that will be produced
  - name: the name of the recipe
  - time: the amount of time it takes to produce the outputs
  """
  @type t :: %Recipe{
          resources_in: %{String.t() => integer()},
          resource_out: {String.t(), integer()},
          name: String.t(),
          time: pos_integer()
        }

  @doc """
  Create a recipe with `name` from a Map.

  This is used for translating the game's JSON files into objects that can be
  stored in :ets.
  """
  def from_json(name, json) do
    %Recipe{
      name: name,
      resources_in: json["inputs"],
      resource_out: json["outputs"],
      time: json["construction time"]
    }
  end
end
