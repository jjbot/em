defmodule EM.World.Utils.Blueprint do
  @moduledoc """
  Blueprints provide a convenient way of grouping together machines that together
  produce a material.
  """

  alias __MODULE__, as: Blueprint

  alias EM.Machines.Machine
  alias EM.World.Location

  defstruct machines: %{}

  @type t :: %Blueprint{
          machines: %{Location.t() => any()}
        }

  @spec from_file(String.t()) :: {:ok, t()}
  def from_file(_file) do
    {:ok, %Blueprint{}}
  end

  @spec rotate(t()) :: t()
  def rotate(%Blueprint{machines: machines} = blueprint) do
    {_, machines} =
      Enum.map_reduce(
        machines,
        %{},
        fn {location, machine}, acc ->
          loc = Location.rotate(location, machine)
          mac = Machine.rotate(machine)
          {{loc, mac}, Map.put(acc, loc, mac)}
        end
      )

    %{blueprint | machines: machines}
  end
end
