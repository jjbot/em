defmodule EM.Machines.Chest do
  @moduledoc """
  Chest implementation for Elixir Machines. Chests support storing and retrieving
  resources through a simple API.

  This module contains the basic operations that can be performed on Chests like
  taking and putting resources.

  Chests support the `EM.Machines.Machine` and `EM.Machines.Container` protocols.

  ## Storage types
  Chests can be outfitted with various storage types:
  * `Map`: This gives the chest infinite storage capacity
  * `EM.Machines.Components.SlotStorage`: This gives the chest a limited set of
    slots to contain stacks of resources. Different resources can be mixed in a
    chest.
  * `EM.Machines.Components.TypedStorage`: Indicates exactly what types of
    recources can be stored in a chest, and their amounts.

  To specify the storage type, just add it to the Chest struct.

      iex> %Chest{storage: %SlotStorage{}}
      %Chest{storage: %SlotStorage{content: %{}, slots: 8}}

  For more information on interacting with the storage functionality of Chests, see
  `EM.Machines.Container`.

  """

  alias __MODULE__, as: Chest
  alias EM.Machines.{Container, ContainerImpl}
  alias EM.Machines.InserterServer
  alias EM.Machines.Payload
  alias EM.Machines.Components.{Storage, SlotStorage}
  alias EM.World.Location

  defstruct backup_server: nil,
            bl: {},
            consumer_q: :queue.new(),
            provider_q: :queue.new(),
            storage: %SlotStorage{slots: 8}

  @typedoc """
  The Chest type. Fields:
  * backup_server: reference to the server that is responsible for storing the
    backup state of a chest.
  * bl: bottom left corner, in this case that's actually just the posistion of
    the chest.
  * consumer_q: a `:queue.queue` keeping track of processes that want resources.
  * provider_q: a `:queue.queue` that keeps track of processes that want to
    deposite resources.
  * storage: a storage container to keep track of resources, defaults to
    `SlotStorage` with 8 slots.
  """
  @type t :: %Chest{
          backup_server: GenServer.name() | nil,
          bl: Location.t(),
          storage: Container.t(),
          consumer_q: :queue.queue(),
          provider_q: :queue.queue()
        }

  ############################################################
  # Machine functions
  ############################################################

  def get_backup_state(chest), do: [bl: chest.bl]

  def is_transporter(), do: false

  ############################################################
  # Container functions
  ############################################################

  @doc """
  Remove `consumer` from the consumer queue of `chest`.

  Returns an error when consumer is not in queue.
  """
  @spec dequeue_consumer(t(), pid()) :: {:ok, t()} | {:error, :not_queued}
  def dequeue_consumer(%Chest{consumer_q: q} = chest, consumer) do
    case ContainerImpl.dequeue(q, consumer) do
      {:ok, queue} -> {:ok, %{chest | consumer_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  @doc """
  Remove `provider` from the provider queue of `chest`.

  Returns an error when provider is not in queue.
  """
  @spec dequeue_provider(t(), pid()) :: {:ok, t()} | {:error, :not_queued}
  def dequeue_provider(%Chest{provider_q: q} = chest, provider) do
    case ContainerImpl.dequeue(q, provider) do
      {:ok, queue} -> {:ok, %{chest | provider_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  @doc """
  Add `consumer` to consumer queue.
  """
  @spec enqueue_consumer(t(), pid()) :: {:ok, t()} | {:error, :already_queued}
  def enqueue_consumer(%Chest{consumer_q: q, storage: storage} = chest, consumer) do
    case ContainerImpl.enqueue_consumer(q, storage, consumer) do
      {:ok, queue} -> {:ok, %{chest | consumer_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  @doc """
  Add `provider` to provider queue.
  """
  @spec enqueue_provider(t(), pid(), Payload.t()) ::
          {:ok, t()} | {:error, :already_queued}
  def enqueue_provider(%Chest{provider_q: q, storage: storage} = chest, provider, payload) do
    case ContainerImpl.enqueue_provider(q, storage, provider, payload) do
      {:ok, queue} -> {:ok, %{chest | provider_q: queue}}
      {:error, reason} -> {:error, reason}
    end
  end

  @doc """
  Put `payload` materials in a `chest`.

  ## Examples
      iex> Chest.put(%Chest{storage: %{}}, {"stone", 10})
      {:ok, %Chest{storage: %{"stone" => 10}}}

  """
  @spec put(t(), Payload.t()) :: {:ok, t()} | {:error, atom()}
  def put(%Chest{storage: storage} = chest, payload) do
    case Storage.put(storage, payload) do
      {:ok, storage} ->
        chest =
          chest
          |> Map.put(:storage, storage)
          |> notify_queued_consumer()

        {:ok, chest}

      {:error, reason} ->
        {:error, reason}
    end
  end

  @doc """
  Put `payload` materials in a `chest`. Raises an exception when the provided
  resources don't fit.

  ## Examples
      iex> Chest.put!(%Chest{storage: %{}}, {"stone", 10})
      %Chest{storage: %{"stone" => 10}}

  """
  @spec put!(t(), Payload.t()) :: t()
  def put!(%Chest{} = chest, payload) do
    {:ok, chest} = put(chest, payload)
    chest
  end

  @doc """
  Take a single material from the `chest`. The resouce type is randomly selected
  from the materials currently stored in the chest. Will return {:error,
  :not_available} when the chest is empty.
  """
  def take(%Chest{storage: storage} = chest) do
    if Storage.is_empty?(storage) do
      {:error, :not_available}
    else
      {type, _stored_amount} = Storage.to_map(storage) |> Enum.random()
      take(chest, {type, 1})
    end
  end

  @doc """
  Take `to_take` from the `chest`. Returns {:error, :not_available} when the
  chest does not contain these resources.
  """
  @spec take(t(), Payload.t()) ::
          {:ok, t(), Payload.t()}
          | {:error, :not_available}
  def take(%Chest{storage: storage} = chest, payload) do
    case Storage.take(storage, payload) do
      {:ok, storage, payload} ->
        chest =
          chest
          |> Map.put(:storage, storage)
          |> notify_queued_provider()
          |> notify_queued_consumer()

        {:ok, chest, payload}

      {:error, message} ->
        {:error, message}
    end
  end

  def notify_queued_consumer(chest) do
    case {Storage.is_empty?(chest.storage), :queue.out(chest.consumer_q)} do
      {false, {{:value, consumer}, q}} ->
        InserterServer.notify_material_available(consumer)
        %{chest | consumer_q: q}

      _ ->
        chest
    end
  end

  # Should probably keep track of what the inserter wants to store in the queue
  # Need some mechanism to go through the queue and inform all processes that are
  # queued and whose resources we can store.
  def notify_queued_provider(chest) do
    case :queue.out(chest.provider_q) do
      {{:value, provider}, q} ->
        InserterServer.notify_space_available(provider)
        %{chest | provider_q: q}

      {:empty, _q} ->
        chest
    end
  end
end

defimpl EM.Machines.Machine, for: EM.Machines.Chest do
  alias EM.Machines.Chest

  def bl(%Chest{bl: bl}), do: bl

  def describe(chest) do
    %{
      "name" => "chest@#{inspect(chest.bl)}",
      "type" => "chest"
    }
  end

  defdelegate get_backup_state(chest), to: Chest

  def locations(%Chest{bl: bl}), do: [bl]

  def process_location_claimed(chest, _location, _remote_machine_ref), do: chest

  def rotate(chest), do: chest

  def server_module(_chest), do: EM.Machines.ChestServer

  def start(chest), do: chest

  def state_module(_chest), do: EM.Machines.Chest

  def size(_chest), do: {1, 1}

  def is_transporter(_chest), do: false
end

defimpl EM.Machines.Container, for: EM.Machines.Chest do
  alias EM.Machines.Chest

  defdelegate dequeue_consumer(chest, consumer), to: Chest

  defdelegate dequeue_provider(chest, consumer), to: Chest

  defdelegate enqueue_consumer(chest, consumer), to: Chest

  defdelegate enqueue_provider(chest, consumer, payload), to: Chest

  defdelegate put(chest, payload), to: Chest

  defdelegate take(chest), to: Chest

  defdelegate take(chest, payload), to: Chest
end

defmodule EM.Machines.ChestServer do
  @moduledoc """
  Server module for Chest.
  """

  use GenServer

  use EM.Machines.MachineServer
  use EM.Machines.ContainerServer

  def update_bu_state(chest_pid, ms) do
    GenServer.call(chest_pid, {:update_state, ms})
  end

  def handle_call({:update_state, ms}, _from, state) do
    MachineSupervisor.update_restart_arguments(ms, [state])
    {:reply, :ok, state}
  end
end
