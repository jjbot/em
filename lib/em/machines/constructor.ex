defprotocol EM.Machines.Constructor do
  @moduledoc """
  Interface functions for creating a Constructor. Constructors are machines
  that convert input resources into output resources.
  """

  alias EM.Machines.Utils.Recipe

  @doc """
  Set the `constructor`'s `recipe`: it defines what is being produced.
  """
  @spec set_recipe(t(), Recipe.t() | nil) ::
          {:ok, t(), %{String.t() => pos_integer()}} | {:error, atom()}
  def set_recipe(constructor, recipe)
end

defmodule EM.Machines.ConstructorServer do
  @moduledoc """
  Server module for Constructor.
  """

  alias EM.Machines.Constructor
  alias EM.Machines.Utils.Recipe

  defmacro __using__(_) do
    quote do
      def handle_call({:set_recipe, recipe}, _from, constructor) do
        case Constructor.set_recipe(constructor, recipe) do
          {:ok, constructor, resources_left} -> {:reply, {:ok, resources_left}, constructor}
          {:error, reason} -> {:reply, {:error, reason}, constructor}
        end
      end
    end
  end

  @spec set_recipe(pid(), Recipe.t()) :: {:ok, Map} | {:error, atom()}
  def set_recipe(constructor_pid, recipe) do
    # FIXME: check if resources are actually returned. Do we have a test for this?
    GenServer.call(constructor_pid, {:set_recipe, recipe})
  end
end
