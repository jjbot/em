# EM: Elixir Machines

A playground for building a Factorio like application in Elixir.

The goal of this project is to play around with Elixir concepts to gain a deeper
understanding of the Elixir programming language and OTP libraries. This is done
by building a [Factorio](https://www.factorio.com/) style game engine.

If you are looking for a fun game to play, Factorio is definetely the better
option. If, however, you are here to learn more about Elixir, read on!

## Implementation goals
What this project set out to implement, and whether it's actually there:
- [x] Have Factorio style machines that act independently
- [x] Investigate the usage of Protocols and how they can be used with GenServer
      to do functional composition
- [x] Be able to recover from machine failure while retaining (part of) the
      machine state
- [x] Split the "game world" into pieces that are supervised independently
- [x] Be able to split the game world across multiple nodes, which are hosted on
      different (physical) machines
- [ ] Do load-balancing across nodes
- [ ] Recover the game state when nodes crash or get removed rom the network
- [ ] Implement "science" as a single service that is unique within a cluster

**Please note** fancy graphics (or any graphics at all) are **not** part of the
goals set for this project. Occasionally, the app might print parts of the world
to the terminal, but other than that there are no plans for visually displaying
what is going on.

Known shortcomings
- Process naming is not done consistently: sometimes PIDs are used where :via
  tuples would be better. Also, there is no functionality yet to switch between
  cluster aware (`:syn`) and local only (`Registry`) name services.

## Application overview

### Factorio like assests
Only a small subset of the machines found in Factorio have been implemented, but
they are enough to build interesting factories. The goal here is not to be feature
complete, but to provide a playground to work with Elixir. Some of the entities
that make Factorio fun to play will be difficult to implement using the approach
chosen for this project. Think for example about anything to do with fluids.

#### Machines
Machines are implemented using GenServer and do their work using timers and
message passing. The implementation leverages protocols and macros to do much of
the heavy lifting. Most functionality is accessed through behaviour modules that
work on a subset of machines.

Machines that are currently implemented:
- [x] Assemblers
- [ ] Belts
- [x] Chests
- [ ] Furnaces
- [ ] Miners
- [x] Inserters
- [ ] Oil refineries
- [ ] Power generators
- [ ] Pumpjacks
- [ ] Rocket silo
- [ ] Science labs

Other game assets needed to create a factory:
- [ ] Blueprints
- [x] Materials
- [x] Recipes

#### Behaviour modules
In stead of accessing the GenServer modules directly, we use behaviour modules
that expose the required functionality. This enables functional composition. To
give an example: both Chests and Assemblers store resources, but the way they do
is quite different. You can put and take resources from chests indiscriminently,
while for assemblers you can only put the resources required by the current
recipe, and take resources that have been produced by the assembler. However, the
external interface (`put` and `take`) should be the same as the machine tasked
with transporting materials doesn't care about the internal workings of the
machines.

Currently, the following behaviour modules have been implemented:
- **Machine**: everything to do with basic operation of a machine, including their
  position, starting the machine, which modules are involved, etc.
- **Container**: machines that support storage operations like `take` and `put`.
  Machines that support this are the different types of chests (plain, vanishing,
  and generator) and assemblers.
- **Constructor**: machines that turn input resources into output resources. Currently
  implemented by assembler.

## Supervision
The game world is split into Tiles, which are each responsible for a n * m area.
Tiles are implemented as supervisors, and are therefor also responsible for
restarting machines when they crash. Currently, machines can request their parent
tile to update the backup state of the machine. This basically means that when the
process crashes, it won't be restarted with its original starting state, but with
the latest backup that was made. This is needed because the state of in game
entities might greatly affect the way the factory runs. To give an example: the
orientation of an inserter can be changed, and the way it faces determines whether
the right materials get transported from the intended source to the intended
target.

## How to use
Easiest way to start playing around with this project is to clone the repo,
install the dependencies and do `iex -S mix`. This drops you into an empty world,
to which you can add machines.

To get something up and running quickly, have a look at the `gc.exs` module in the
`test/manual` directory.  You can compile it in `iex` using
`c "test/manual/gc.exs"`. This gives you access to the `GreenCircuits` module.
Place a that "blueprint" by calling `GreenCircuits.construct({0, 0})`, where
`{0, 0}` is the bottom left location where the blueprint will be placed. It
returns a PID with the Chest that will store the produced GreenCircuits when they
become available. Use `:observer` to look at the overall structure of the
supervision tree, and check the process state of all machines involved.
