alias EM.Machines.{Assembler, AssemblerServer}
alias EM.Machines.{Chest, ChestServer}
alias ConstructorServer
alias EM.Machines.{Container, ContainerServer, SlotContainer, TypedContainer}
alias EM.Machines.{Inserter, InserterServer}
alias EM.Machines.{Machine, MachineServer}
alias EM.Machines.{GeneratorChest, GeneratorChestServer}
alias EM.Machines.{Storage, StorageServer}
alias EM.Machines.{VanishingChest, VanishingChestServer}
alias EM.Machines.Utils.Recipe
alias EM.World
alias EM.World.{Location, Orientation}
alias EM.World.MachineMeta
alias EM.World.MaterialLibrary
alias EM.World.ProductionStats
alias EM.World.{Recipe, RecipeLibrary}
alias EM.World.{Tile, TileManager, TileServer}
