defmodule EM.WorldTest do
  use ExUnit.Case, async: false

  alias EM.Machines.Chest
  alias EM.World

  describe "in an exising world" do
    setup _ do
      tile_pid = World.get_or_create_tile_at({0, 0})
      %{tile: tile_pid}
    end

    test "creating tile on existing location gives tile pid", %{tile: tile_pid} do
      assert tile_pid == World.get_or_create_tile_at({5, 4})
    end

    test "creating tile on new location gives new pid", %{} do
      {:via, :syn, new_tile_name} = World.get_or_create_tile_at({100, 100})
      assert is_pid(:syn.whereis(new_tile_name))
    end

    test "asking for a non-existing tile raises an error", %{} do
      assert_raise RuntimeError, fn -> World.get_tile_at!({99_990, 99_990}) end
    end

    test "asking for an exiting tile works just fine", %{tile: tile_ref} do
      {:via, _, tile_name} = tile_ref
      tile_pid = World.get_tile_at!({0, 0})
      assert tile_pid == :syn.whereis(tile_name)
    end

    test "existing machine gets found", %{} do
      {:ok, chest_ref, _chest_name} = World.add_machine(%Chest{}, {5, 9})
      {:ok, new_ref} = World.get_machine_at({5, 9})
      assert new_ref == chest_ref
    end

    test "non existing machine gives undefined", %{} do
      assert World.get_machine_at({0, 9}) == {:error, :undefined}
    end

    test "tiles are counted properly", %{} do
      # FIXME: not sure what to do here to make it more useful
      assert World.get_tile_count() > 0
    end

    test "tiles are counted properly when specifying own node name", %{} do
      assert World.get_tile_count(node()) > 0
    end
  end
end
