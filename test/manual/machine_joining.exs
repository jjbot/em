defmodule MachineJoining do
  alias EM.Machines.{Assembler, Chest, Inserter}
  alias EM.World
  alias EM.World.Location

  def construct(offset \\ {0, 0}) do
    {:ok, chest1} = World.add_machine(%Chest{}, Location.add(offset, {1, 0}))
    {:ok, chest2} = World.add_machine(%Chest{}, Location.add(offset, {2, 0}))
    {:ok, chest3} = World.add_machine(%Chest{}, Location.add(offset, {3, 0}))

    {:ok, ins1} = World.add_machine(%Inserter{}, Location.add(offset, {1, 1}))
    {:ok, ins2} = World.add_machine(%Inserter{}, Location.add(offset, {2, 1}))
    {:ok, ins3} = World.add_machine(%Inserter{}, Location.add(offset, {3, 1}))

    {:ok, assembler} = World.add_machine(%Assembler{}, Location.add(offset, {1, 2}))

    %{
      chests: [chest1, chest2, chest3],
      inserters: [ins1, ins2, ins3],
      assembler: assembler
    }
  end
end
