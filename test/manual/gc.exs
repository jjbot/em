defmodule GreenCircuits do
  alias EM.Machines.Assembler
  alias EM.Machines.Chest
  alias EM.Machines.ConstructorServer
  alias EM.Machines.{GeneratorChest, GeneratorChestServer}
  alias EM.Machines.Inserter
  alias EM.Machines.MachineServer
  alias EM.World.Location
  alias EM.World.RecipeLibrary
  alias EM.World

  @spec construct(Location.t()) :: pid()
  def construct(offset \\ {0, 0}) do
    {:ok, copper_plate_generator, _name} =
      World.add_machine(
        %GeneratorChest{},
        Location.add(offset, {1, 1})
      )

    {:ok, copper_plate_inserter, _name} =
      World.add_machine(
        %Inserter{
          target_orientation: :east,
          current_orientation: :west
        },
        Location.add(offset, {2, 1})
      )

    {:ok, copper_cable_assembler, _name} =
      World.add_machine(
        %Assembler{},
        Location.add(offset, {3, 0})
      )

    {:ok, copper_cable_inserter, _name} =
      World.add_machine(
        %Inserter{
          target_orientation: :east,
          current_orientation: :west
        },
        Location.add(offset, {6, 2})
      )

    {:ok, iron_plate_generator, _name} =
      World.add_machine(
        %GeneratorChest{},
        Location.add(offset, {5, 4})
      )

    {:ok, iron_plate_inserter, _name} =
      World.add_machine(
        %Inserter{
          target_orientation: :east,
          current_orientation: :west
        },
        Location.add(offset, {6, 4})
      )

    {:ok, gc_assembler, _name} =
      World.add_machine(
        %Assembler{},
        Location.add(offset, {7, 2})
      )

    {:ok, gc_inserter, _name} =
      World.add_machine(
        %Inserter{
          target_orientation: :east,
          current_orientation: :west
        },
        Location.add(offset, {10, 3})
      )

    {:ok, chest, _name} =
      World.add_machine(
        %Chest{},
        Location.add(offset, {11, 3})
      )

    GeneratorChestServer.add_generator(copper_plate_generator, {"copper plate", 1}, 500)
    MachineServer.start(copper_plate_inserter)
    ConstructorServer.set_recipe(copper_cable_assembler, RecipeLibrary.get_recipe("copper cable"))
    MachineServer.start(copper_cable_assembler)
    MachineServer.start(copper_cable_inserter)
    GeneratorChestServer.add_generator(iron_plate_generator, {"iron plate", 1}, 500)
    ConstructorServer.set_recipe(gc_assembler, RecipeLibrary.get_recipe("electronic circuit"))
    MachineServer.start(gc_assembler)
    MachineServer.start(gc_inserter)

    chest
  end

  def construct_many(bl, amount, {dx, dy}) do
    Enum.map(
      0..(amount - 1),
      fn i ->
        Location.add(bl, {dx * i, dy * i})
        |> construct()
      end
    )
  end
end
