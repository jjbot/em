defmodule AutomationScience do
  alias EM.Machines.Assembler
  alias EM.Machines.Chest
  alias EM.Machines.ConstructorServer
  alias EM.Machines.{GeneratorChest, GeneratorChestServer}
  alias EM.Machines.Inserter
  alias EM.Machines.MachineServer
  alias EM.World
  alias EM.World.{Location, RecipeLibrary}

  @spec construct(Location.t()) :: pid()
  def construct(offset \\ {0, 0}) do
    {:ok, iron_plate_generator} =
      World.add_machine(
        %GeneratorChest{},
        Location.add(offset, {1, 1})
      )

    {:ok, iron_plate_inserter} =
      World.add_machine(
        %Inserter{
          target_orientation: :east,
          current_orientation: :west
        },
        Location.add(offset, {2, 1})
      )

    {:ok, gear_assembler} =
      World.add_machine(
        %Assembler{},
        Location.add(offset, {3, 0})
      )

    {:ok, gear_inserter} =
      World.add_machine(
        %Inserter{
          target_orientation: :east,
          current_orientation: :west
        },
        Location.add(offset, {6, 2})
      )

    {:ok, copper_plate_generator} =
      World.add_machine(%GeneratorChest{}, Location.add(offset, {5, 4}))

    {:ok, copper_plate_inserter} =
      World.add_machine(
        %Inserter{
          target_orientation: :east,
          current_orientation: :west
        },
        Location.add(offset, {6, 4})
      )

    {:ok, automation_assembler} =
      World.add_machine(
        %Assembler{},
        Location.add(offset, {7, 2})
      )

    {:ok, automation_inserter} =
      World.add_machine(
        %Inserter{
          target_orientation: :east,
          current_orientation: :west
        },
        Location.add(offset, {10, 3})
      )

    {:ok, chest} =
      World.add_machine(
        %Chest{},
        Location.add(offset, {11, 3})
      )

    GeneratorChestServer.add_generator(iron_plate_generator, {"iron plate", 1}, 500)
    MachineServer.start(iron_plate_inserter)

    ConstructorServer.set_recipe(
      gear_assembler,
      RecipeLibrary.get_recipe("iron gear wheel")
    )

    MachineServer.start(gear_assembler)
    MachineServer.start(gear_inserter)
    GeneratorChestServer.add_generator(copper_plate_generator, {"copper plate", 1}, 500)
    MachineServer.start(copper_plate_inserter)

    ConstructorServer.set_recipe(
      automation_assembler,
      RecipeLibrary.get_recipe("automation science pack")
    )

    MachineServer.start(automation_inserter)

    chest
  end
end
