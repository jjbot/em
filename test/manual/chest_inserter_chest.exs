defmodule ChestInserterChest do
  alias EM.Machines.{Chest, Inserter}
  alias EM.World
  alias EM.World.{Tile, TileServer}
  alias EM.World.Location

  def construct(bl) do
    tile_pid = World.get_or_create_tile_at(bl)

    {:ok, chest_pid} =
      World.add_machine(
        %Chest{storage: %{"stone" => 100}},
        Location.add(bl, {3, 3})
      )

    {:ok, source_pid} =
      World.add_machine(
        %Chest{storage: %{"iron plate" => 100}},
        Location.add(bl, {1, 3})
      )

    {:ok, i_pid} =
      World.add_machine(
        %Inserter{
          swing_time: 1000,
          target_orientation: :east,
          current_orientation: :west
        },
        Location.add(bl, {2, 3})
      )
  end
end
