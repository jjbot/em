defmodule EM.World.TileServerTest do
  use ExUnit.Case, async: false

  alias EM.Machines.{Assembler, Chest, Inserter}
  alias EM.World
  alias EM.World.{Location, TileManager, TileServer}

  # FIXME: Tiles are now created by hardcoding their name in the TileServer
  # start_link/1 calls. This should be fixed by creating modules that create
  # the name depending on whether the application runs in cluster mode or not.

  @tile_offset 100

  def get_empty_tile() do
    bl = {:rand.uniform(@tile_offset) * 10, :rand.uniform(@tile_offset) * 10}

    case TileManager.lookup(bl) do
      {:error, :undefined} ->
        tile_pid = World.get_or_create_tile_at(bl)
        {bl, tile_pid}

      _ ->
        get_empty_tile()
    end
  end

  describe "with an existing tile" do
    setup _ do
      {bl, tile_pid} = get_empty_tile()

      {:ok, i_pid, _machine_name} =
        TileServer.add_machine(
          tile_pid,
          %Inserter{},
          Location.add(bl, {5, 5})
        )

      :timer.sleep(250)

      %{tile: tile_pid, tile_bl: bl, inserter: i_pid}
    end

    test "adding machine with overlap fails", %{tile: tile_pid, tile_bl: tile_bl} do
      assembler = %Assembler{}
      assembler_loc = Location.add(tile_bl, {4, 4})
      response = TileServer.add_machine(tile_pid, assembler, assembler_loc)
      assert {:error, :position_taken} == response
    end

    test "adding machine without overlap succeeds", %{tile: tile_pid, tile_bl: tile_bl} do
      chest = %Chest{}
      chest_loc = Location.add(tile_bl, {4, 4})
      {:ok, pid, name} = TileServer.add_machine(tile_pid, chest, chest_loc)
      assert is_pid(pid)
      assert pid == :syn.whereis(name)
    end

    # test "removing inserter by location works", %{tile: tile_pid, tile_bl: tile_bl} do
    #   TileServer.remove_machine(tile_pid, Location.add(tile_bl, {5, 5}))
    #   tile_state = :sys.get_state(tile_pid)
    #   assert tile_state.machines == %{}
    #   assert tile_state.locations == %{}
    # end

    # test "removing inserter by pid works", %{tile: tile_pid, inserter: i_pid} do
    #   TileServer.remove_machine(tile_pid, i_pid)
    #   tile_state = :sys.get_state(tile_pid)
    #   assert tile_state.machines == %{}
    #   assert tile_state.locations == %{}
    # end

    # test "removing a non registered pid gives an error", %{tile: tile_pid} do
    #   assert {:error, :no_such_process} == TileServer.remove_machine(tile_pid, self())
    # end

    # test "removing a machine from an unoccupied location gives an error", %{tile: tile_pid} do
    #   assert {:error, :cell_not_occupied} == TileServer.remove_machine(tile_pid, {4, 4})
    # end

    test "asking for neighbours when none are present gives empty list", %{
      tile: tile_pid,
      inserter: i_pid
    } do
      assert [] == TileServer.get_machine_neighbours(tile_pid, i_pid)
    end
  end

  describe "with a larger set of machines" do
    setup _ do
      # bl = {:random.uniform(@tile_offset) * 10, :random.uniform(@tile_offset) * 10}
      {bl, tile_pid} = get_empty_tile()
      # {:ok, tile_pid} = TileServer.start_link({%Tile{bl: bl}, Tile.get_name(bl)})

      for x <- 0..2, do: TileServer.add_machine(tile_pid, %Inserter{}, Location.add(bl, {x, 0}))

      {:ok, assembler_pid, _machine_name} =
        TileServer.add_machine(
          tile_pid,
          %Assembler{},
          Location.add(bl, {3, 3})
        )

      :timer.sleep(250)

      %{tile: tile_pid, tile_bl: bl, assembler: assembler_pid}
    end

    test "all inserters are found", %{tile: tile_pid, tile_bl: tile_bl} do
      locs = Enum.map([{0, 0}, {1, 0}, {2, 0}], fn loc -> Location.add(tile_bl, loc) end)
      machines = TileServer.get_machines(tile_pid, locs)
      assert length(Map.keys(machines)) == 3
    end

    test "assembler is found as one entity", %{tile: tile_pid} do
      machines = TileServer.get_machines(tile_pid, [{3, 3}, {3, 4}, {4, 3}])
      assert length(Map.keys(machines)) == 1
    end

    # This doesn't work anymore, the output changed, and the machines we are looking up
    # are different...
    # test "get_machine_neighbours reports unique machines", %{tile: tile_pid, assembler: a_pid} do
    #   {:ok, pid} = TileServer.add_machine(tile_pid, %Assembler{}, {3, 6})
    #   assert [{Assembler, a_pid}] == TileServer.get_machine_neighbours(tile_pid, pid)
    # end
  end

  describe "with a chest present" do
    setup _ do
      {bl, tile_pid} = get_empty_tile()

      {:ok, chest_pid, _chest_name} = World.add_machine(%Chest{}, Location.add(bl, {3, 3}))

      %{tile: tile_pid, tile_bl: bl, chest: chest_pid}
    end

    test "adding a chest and inserter moves all available resouces", %{
      chest: target_pid,
      tile_bl: tile_bl
    } do
      {:ok, source_pid, _} =
        World.add_machine(
          %Chest{storage: %{"stone" => 5}},
          Location.add(tile_bl, {1, 3})
        )

      {:ok, i_pid, _} =
        World.add_machine(
          %Inserter{
            swing_time: 10,
            target_orientation: :east,
            current_orientation: :west
          },
          Location.add(tile_bl, {2, 3})
        )

      i_state = :sys.get_state(i_pid)
      assert i_state.source == source_pid
      assert i_state.target == target_pid

      :timer.sleep(130)

      source_state = :sys.get_state(source_pid)
      target_state = :sys.get_state(target_pid)
      assert source_state.storage == %{}
      assert target_state.storage.content == %{"stone" => 5}
    end
  end
end
