defmodule EM.World.TestOrientation do
  use ExUnit.Case, async: true

  alias EM.World.Orientation

  doctest EM.World.Orientation

  test "opposite gives right orientation" do
    orientations = [
      {:north, :south},
      {:east, :west},
      {:south, :north},
      {:west, :east}
    ]

    Enum.each(
      orientations,
      fn {source, opposite} -> assert opposite == Orientation.opposite(source) end
    )
  end
end
