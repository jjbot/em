defmodule EM.World.TestLocation do
  use ExUnit.Case, async: true

  alias EM.Machines.Assembler
  alias EM.World.Location

  doctest EM.World.Location

  test "Generating locations works" do
    locs = Location.gen_locations({1, 1}, {3, 3})

    assert locs == [
             {1, 1},
             {1, 2},
             {1, 3},
             {2, 1},
             {2, 2},
             {2, 3},
             {3, 1},
             {3, 2},
             {3, 3}
           ]
  end

  test "Getting tile bottom left works" do
    assert {0, 0} == Location.get_tile_location({5, 4})
    assert {10, 10} == Location.get_tile_location({12, 15})
  end

  test "normalise works" do
    assert {-10, 0} == Location.normalise({-1, 5})
  end

  test "rotating location works" do
    assembler = %Assembler{}
    assert {2, -5} = Location.rotate({2, 2}, assembler)
  end
end
