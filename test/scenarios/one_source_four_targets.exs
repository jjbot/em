defmodule OneSourceFourTargets do
  @moduledoc """
  A more elaborate test for chests and inserters: we have one source chest in the
  middle, and four inserters pointing away from it, towards four target chests.
  When we give the source chest 4 resources, we assume each of the target chests
  will get one.
  """

  use ExUnit.Case, async: true

  alias EM.Machines.{Chest, ContainerServer, Inserter}
  alias EM.World

  setup_all do
    {:ok, source} = World.add_machine(%Chest{}, {2, 2})

    {:ok, target1} = World.add_machine(%Chest{}, {2, 4})
    {:ok, target2} = World.add_machine(%Chest{}, {4, 2})
    {:ok, target3} = World.add_machine(%Chest{}, {2, 0})
    {:ok, target4} = World.add_machine(%Chest{}, {0, 2})

    {:ok, ins1} =
      World.add_machine(
        %Inserter{target_orientation: :north, current_orientation: :south, swing_time: 10},
        {2, 3}
      )

    {:ok, ins2} =
      World.add_machine(
        %Inserter{target_orientation: :east, current_orientation: :west, swing_time: 10},
        {3, 2}
      )

    {:ok, ins3} =
      World.add_machine(
        %Inserter{target_orientation: :south, current_orientation: :north, swing_time: 10},
        {2, 1}
      )

    {:ok, ins4} =
      World.add_machine(
        %Inserter{target_orientation: :west, current_orientation: :east, swing_time: 10},
        {1, 2}
      )

    {
      :ok,
      %{
        source: source,
        targets: [target1, target2, target3, target4],
        inserters: [ins1, ins2, ins3, ins4]
      }
    }
  end

  describe "with four starting resources" do
    setup arg do
      {:ok, arg}
    end

    test "each chest gets one resource", %{inserters: inserters, source: source, targets: targets} do
      ContainerServer.put(source, {"stone", 4})

      :timer.sleep(100)

      out =
        targets
        |> Enum.map(fn target -> :sys.get_state(target) end)
        |> Enum.map(fn state -> Map.get(state, :storage) end)
        |> Enum.map(fn container -> container.content == %{"stone" => 1} end)

      assert Enum.all?(out)
    end
  end
end
