defmodule EM.Machines.StorageTest do
  use ExUnit.Case, async: true

  alias EM.Machines.Components.{Storage, TypedStorage, SlotStorage}
  alias EM.World.MaterialLibrary

  doctest EM.Machines.Components.Storage

  describe "with a TypedStorage" do
    setup _ do
      {:ok, tc} = TypedStorage.init_empty(%{"stone" => 50})
      %{container: tc}
    end

    test "container with content is not empty", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 10})
      refute Storage.is_empty?(container)
    end

    test "container without content is empty", %{container: container} do
      assert Storage.is_empty?(container)
    end

    test "available space for contained material is correct", %{container: container} do
      assert 50 == Storage.space_for(container, "stone")
    end

    test "available space for non-contained material is correct", %{container: container} do
      assert 0 == Storage.space_for(container, "iron plate")
    end

    test "after adding contained material, available space is correct", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 10})
      assert 40 == Storage.space_for(container, "stone")
    end

    test "putting non-contained material fails", %{container: container} do
      assert {:error, :type_not_supported} == Storage.put(container, {"iron plate", 10})
    end

    test "taking existing resources works", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 10})

      assert {:ok, %TypedStorage{content: %{"stone" => 4}, slots: %{"stone" => 50}}, {"stone", 6}} ==
               Storage.take(container, {"stone", 6})
    end

    test "taking non-existing resources fails", %{container: container} do
      assert {:error, :not_available} == Storage.take(container, {"iron plate", 10})
    end

    test "taking from an empty container fails", %{container: container} do
      assert {:error, :not_available} == Storage.take(container)
    end

    test "taking from a container with resources succeeds", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 10})
      {:ok, container, payload} = Storage.take(container)
      assert Storage.to_map(container) == %{"stone" => 9}
      assert payload == {"stone", 1}
    end

    test "to_map returns right map", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 2})
      assert %{"stone" => 2} == Storage.to_map(container)
    end
  end

  describe "with SlotStorage" do
    setup _ do
      %{container: %SlotStorage{slots: 2}}
    end

    test "container with content is not empty", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 10})
      refute Storage.is_empty?(container)
    end

    test "container without content is empty", %{container: container} do
      assert Storage.is_empty?(container)
    end

    test "available space for new material is correct", %{container: container} do
      stack_size = MaterialLibrary.get_stack_size!("iron plate")
      assert 2 * stack_size == Storage.space_for(container, "iron plate")
    end

    test "available space when slot occupied is correct", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 10})
      assert 90 == Storage.space_for(container, "stone")

      stack_size = MaterialLibrary.get_stack_size!("iron plate")
      assert stack_size == Storage.space_for(container, "iron plate")
    end

    test "taking existing resources works", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 10})

      assert {:ok, %SlotStorage{content: %{"stone" => 2}, slots: 2}, {"stone", 8}} ==
               Storage.take(container, {"stone", 8})
    end

    test "to_map returns right map", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 2})
      assert Storage.to_map(container) == %{"stone" => 2}
    end

    test "removing all resources also removes claimed slot", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 10})
      {:ok, container, _} = Storage.take(container, {"stone", 10})
      assert Storage.to_map(container) == %{}
    end

    test "storing max resources gives right amount of space", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 50})
      assert Storage.space_for(container, "stone") == 50
    end

    test "taking from an empty container fails", %{container: container} do
      assert {:error, :not_available} == Storage.take(container)
    end

    test "taking from a container with resources succeeds", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 10})
      {:ok, container, payload} = Storage.take(container)
      assert Storage.to_map(container) == %{"stone" => 9}
      assert payload == {"stone", 1}
    end
  end

  describe "with MapStorage" do
    setup _ do
      %{container: %{}}
    end

    test "container with content is not empty", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 10})
      refute Storage.is_empty?(container)
    end

    test "container without content is empty", %{container: container} do
      assert Storage.is_empty?(container)
    end

    test "available space for new material is infinite", %{container: container} do
      assert :infinity == Storage.space_for(container, "iron plate")
    end

    test "available space when slot occupied is infinite", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 10})
      assert :infinity == Storage.space_for(container, "stone")
      assert :infinity == Storage.space_for(container, "iron plate")
    end

    test "taking existing resources works", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 10})

      assert {:ok, %{"stone" => 2}, {"stone", 8}} == Storage.take(container, {"stone", 8})
    end

    test "to_map returns right map", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 2})
      assert Storage.to_map(container) == %{"stone" => 2}
    end

    test "removing all resources also removes claimed slot", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 10})
      {:ok, container, _} = Storage.take(container, {"stone", 10})
      assert Storage.to_map(container) == %{}
    end

    test "storing max resources gives right amount of space", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 50})
      assert Storage.space_for(container, "stone") == :infinity
    end

    test "taking from an empty container fails", %{container: container} do
      assert {:error, :not_available} == Storage.take(container)
    end

    test "taking from a container with resources succeeds", %{container: container} do
      {:ok, container} = Storage.put(container, {"stone", 10})
      {:ok, container, payload} = Storage.take(container)
      assert Storage.to_map(container) == %{"stone" => 9}
      assert payload == {"stone", 1}
    end
  end
end
