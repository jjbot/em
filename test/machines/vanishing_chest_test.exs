defmodule EM.Machines.VanishingChestTest do
  alias EM.Machines.VanishingChest
  alias EM.Machines.{Container, Machine}

  use ExUnit.Case, async: true

  describe "basic functions:" do
    setup(_) do
      chest = %VanishingChest{bl: {0, 0}, opposite: self()}
      %{chest: chest}
    end

    test "backing up works", %{chest: chest} do
      assert Machine.get_backup_state(chest) == [bl: chest.bl]
    end

    test "enqueue consumer", %{chest: chest} do
      {:ok, chest} = Container.enqueue_consumer(chest, self())
      assert :queue.member(self(), chest.consumer_q)
    end

    test "dequeue consumer", %{chest: chest} do
      assert {:error, :not_queued} == Container.dequeue_consumer(chest, self())
    end

    test "locations are correct", %{chest: chest} do
      assert Machine.locations(chest) == [{0, 0}]
    end

    test "processing locations claimed does nothing", %{chest: chest} do
      assert Machine.process_location_claimed(chest, {1, 0}, self()) == chest
    end

    test "start", %{chest: chest} do
      assert chest == Machine.start(chest)
    end

    test "take when nothing is stored", %{chest: chest} do
      assert {:error, :not_available} == Container.take(chest)
    end

    test "take when something is stored gives set amount", %{chest: chest} do
      chest = %{chest | storage: %{"stone" => 2}}
      {:ok, chest, payload} = Container.take(chest)
      assert %{"stone" => 1} == chest.storage
      assert {"stone", 1} == payload
    end

    test "take with payload gives requested amount", %{chest: chest} do
      chest = %{chest | storage: %{"stone" => 2}}
      {:ok, chest, payload} = Container.take(chest, {"stone", 2})
      assert %{} == chest.storage
      assert {"stone", 2} == payload
    end

    test "rotation", %{chest: chest} do
      assert chest == Machine.rotate(chest)
    end
  end
end
