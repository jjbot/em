defmodule EM.Machines.InserterTest do
  use ExUnit.Case, async: true

  alias EM.Machines.Components.Storage
  alias EM.Machines.{Inserter, Machine}
  alias EM.Machines.{Chest, ChestServer}

  doctest EM.Machines.Inserter

  setup_all do
    x = :rand.uniform(1_000_000)
    y = :rand.uniform(1_000_000)

    {:ok, %{inserter: %Inserter{bl: {x + 2, y + 2}}}}
  end

  describe "when just created" do
    setup arg do
      {:ok, arg}
    end

    test "rotation moves target to east", %{inserter: inserter} do
      rotated_inserter = Machine.rotate(inserter)
      assert rotated_inserter.target_orientation == :east
      rotated_inserter = Machine.rotate(rotated_inserter)
      assert rotated_inserter.target_orientation == :south
      rotated_inserter = Machine.rotate(rotated_inserter)
      assert rotated_inserter.target_orientation == :west
      rotated_inserter = Machine.rotate(rotated_inserter)
      assert rotated_inserter.target_orientation == :north
    end

    test "size is one", %{inserter: inserter} do
      assert Machine.size(inserter) == {1, 1}
    end

    test "server module is InserterServer", %{inserter: inserter} do
      assert Machine.server_module(inserter) == EM.Machines.InserterServer
    end

    test "state module is Inserter", %{inserter: inserter} do
      assert Machine.state_module(inserter) == Inserter
    end

    test "inserter is a transporter", %{inserter: inserter} do
      assert Machine.is_transporter(inserter)
    end

    test "starting doesn't trigger anything", %{inserter: inserter} do
      assert Machine.start(inserter) == inserter
    end

    test "inserter is not at target", %{inserter: inserter} do
      assert Inserter.at_target?(inserter) == false
    end

    test "inserter has no payload", %{inserter: inserter} do
      assert Inserter.has_payload?(inserter) == false
    end

    test "target orienation is :north", %{inserter: inserter} do
      assert Inserter.get_target_orientation(inserter) == :north
    end

    test "setting orientation works", %{inserter: inserter} do
      inserter = Inserter.set_orientation(inserter, :north)
      assert inserter.current_orientation == :north
      assert Inserter.at_target?(inserter)
    end

    test "setting source works", %{inserter: inserter} do
      inserter = Inserter.set_source(inserter, self())
      assert inserter.source == self()
    end

    test "setting target works", %{inserter: inserter} do
      inserter = Inserter.set_target(inserter, self())
      assert inserter.target == self()
    end

    test "setting status works", %{inserter: inserter} do
      inserter = Inserter.set_status(inserter, :moving)
      assert inserter.status == :moving
    end

    test "swing target is set to nil", %{inserter: inserter} do
      assert inserter.swing_target == nil
    end

    test "setting swing target works", %{inserter: inserter} do
      inserter = Inserter.set_swing_target(inserter, :east)
      assert inserter.swing_target == :east
    end

    test "setting timer works", %{inserter: inserter} do
      {:ok, timer} = :timer.send_after(1000, ":ok")
      inserter = Inserter.set_timer(inserter, timer)
      assert inserter.timer == timer
      :timer.cancel(timer)
    end

    test "setting payload works", %{inserter: inserter} do
      inserter = Inserter.set_payload(inserter, {"stone", 10})
      assert inserter.payload == {"stone", 10}
    end

    test "taking resource causes idle state", %{inserter: inserter} do
      inserter = Inserter.process_material_available(inserter)
      assert inserter.status == :idle
    end

    test "is a transporter", %{inserter: inserter} do
      assert EM.Machines.Machine.is_transporter(inserter)
    end

    test "tries to queue when at source location", %{inserter: inserter} do
      inserter = %{
        inserter
        | current_orientation: :south,
          target_orientation: :north,
          payload: nil
      }

      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, chest_pid, _name} = ChestServer.start_link(%Chest{bl: {x, y}})
      Inserter.set_source(inserter, chest_pid)
      chest_state = :sys.get_state(chest_pid)
      assert :queue.member(self(), chest_state.consumer_q)
    end

    test "tries to queue when at target location", %{inserter: inserter} do
      inserter = %{
        inserter
        | current_orientation: :north,
          target_orientation: :north,
          payload: {"stone", 10}
      }

      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, chest_pid, _name} = ChestServer.start_link(%Chest{bl: {x, y}})
      Inserter.set_target(inserter, chest_pid)
      chest_state = :sys.get_state(chest_pid)
      assert Storage.to_map(chest_state.storage) == %{"stone" => 10}
    end

    test "processing space available works", %{inserter: inserter} do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, chest_pid, _name} = ChestServer.start_link(%Chest{bl: {x, y}})

      inserter = %{
        inserter
        | current_orientation: :north,
          target_orientation: :north,
          payload: {"stone", 10},
          target: chest_pid
      }

      Inserter.process_space_available(inserter)
      chest_state = :sys.get_state(chest_pid)
      assert Storage.to_map(chest_state.storage) == %{"stone" => 10}
    end
  end
end
