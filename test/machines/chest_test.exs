defmodule EM.Machines.ChestTest do
  alias EM.Machines.Container
  alias EM.Machines.{Chest, Machine}
  alias EM.Machines.Components.{Storage, SlotStorage}

  use ExUnit.Case, async: true
  doctest EM.Machines.Chest

  # FIXME: not all code paths for enqueue and dequeue are actually hit

  setup_all do
    {:ok, %{chest: %Chest{bl: {0, 0}}}}
  end

  describe "when empty" do
    setup arg do
      {:ok, arg}
    end

    test "take without arguments gives error empty", %{chest: chest} do
      assert {:error, :not_available} = Chest.take(chest)
    end

    test "take with arguments gives error empty", %{chest: chest} do
      assert {:error, :not_available} = Chest.take(chest, {"stone", 10})
    end

    test "putting new resource works", %{chest: chest} do
      c = Chest.put!(chest, {"stone", 1})
      assert Storage.to_map(c.storage) == %{"stone" => 1}
    end

    test "dequeue_consumer gives error", %{chest: chest} do
      assert {:error, :not_queued} = Chest.dequeue_consumer(chest, self())
    end

    test "enqueue_consumer add process to the queue", %{chest: chest} do
      {:ok, chest} = Chest.enqueue_consumer(chest, self())
      assert :queue.member(self(), chest.consumer_q)
    end

    test "enqueue provider gets callback", %{chest: chest} do
      {:ok, _chest} = Chest.enqueue_provider(chest, self(), {"stone", 10})
      assert_receive {:"$gen_cast", :space_available}
    end

    test "rotation doesn't change anything", %{chest: chest} do
      assert Machine.rotate(chest) == chest
    end

    test "is not a transporter", %{chest: chest} do
      refute EM.Machines.Machine.is_transporter(chest)
    end

    test "has empty backup state", %{chest: chest} do
      assert [bl: {0, 0}] == Chest.get_backup_state(chest)
    end
  end

  describe "when full" do
    setup _ do
      c = %Chest{storage: %SlotStorage{slots: 1, content: %{"stone" => 50}}}
      {:ok, %{chest: c}}
    end

    test "container is_empty? returns false", %{chest: c} do
      assert Storage.is_empty?(c.storage) == false
    end

    test "adding more resources of the same type gives an error", %{chest: c} do
      assert Container.put(c, {"stone", 1}) == {:error, :not_enough_space}
    end

    test "adding resources of a different type gives an error", %{chest: c} do
      assert Container.put(c, {"iron plate", 1}) == {:error, :not_enough_space}
    end

    test "enqueing provider gets provider in queue", %{chest: c} do
      {:ok, c} = Container.enqueue_provider(c, self(), {"stone", 1})
      assert :queue.member(self(), c.provider_q)
    end

    test "removing an enqueued provider works", %{chest: c} do
      {:ok, c} = Container.enqueue_provider(c, self(), {"stone", 1})
      assert :queue.member(self(), c.provider_q)

      {:ok, c} = Container.dequeue_provider(c, self())
      refute :queue.member(self(), c.provider_q)
    end
  end

  describe "when storing some resources" do
    setup %{chest: chest} do
      {:ok, %{chest: Chest.put!(chest, {"stone", 3})}}
    end

    test "take gives one resource", %{chest: chest} do
      {:ok, _chest, taken} = Chest.take(chest)
      assert {"stone", 1} == taken
    end

    test "taking too many gives error", %{chest: chest} do
      assert Chest.take(chest, {"stone", 4}) == {:error, :not_available}
    end

    test "taking all resources removed the type", %{chest: chest} do
      {:ok, chest, taken} = Chest.take(chest, {"stone", 3})
      assert chest == %Chest{bl: {0, 0}}
      assert taken == {"stone", 3}
    end

    test "putting additional resources to known type works", %{chest: chest} do
      c = Chest.put!(chest, {"stone", 1})
      assert Storage.to_map(c.storage) == %{"stone" => 4}
    end

    test "removing some resources causes queued process to be called", %{chest: chest} do
      chest = %{chest | consumer_q: :queue.in(self(), chest.consumer_q)}
      {:ok, chest, _} = Container.take(chest, {"stone", 1})
      assert_receive {:"$gen_cast", :material_available}
      assert Storage.to_map(chest.storage) == %{"stone" => 2}
    end

    test "removing all resources stops queued from being called", %{chest: chest} do
      chest = %{chest | consumer_q: :queue.in(self(), chest.consumer_q)}
      {:ok, chest, _} = Container.take(chest, {"stone", 3})
      refute_receive :matrial_available
      assert Storage.to_map(chest.storage) == %{}
    end
  end

  describe "with a process queued" do
    setup %{chest: chest} do
      {:ok, chest} = Chest.enqueue_consumer(chest, self())
      {:ok, %{chest: chest}}
    end

    test "dequeue_consumer removes process", %{chest: chest} do
      {:ok, chest} = Chest.dequeue_consumer(chest, self())
      refute :queue.member(self(), chest.consumer_q)
    end
  end
end
