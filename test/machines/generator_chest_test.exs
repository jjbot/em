defmodule EM.Machines.GeneratorChestTest do
  alias EM.Machines.Container
  alias EM.Machines.GeneratorChest
  alias EM.Machines.Machine

  use ExUnit.Case, async: true

  describe "basic functions:" do
    setup _ do
      %{chest: %GeneratorChest{bl: {0, 0}}}
    end

    test "backing up works", %{chest: chest} do
      assert Machine.get_backup_state(chest) == [bl: chest.bl]
    end

    test "enqueue consumer", %{chest: chest} do
      {:ok, chest} = Container.enqueue_consumer(chest, self())
      assert :queue.member(self(), chest.consumer_q)
    end

    test "dequeue consumer", %{chest: chest} do
      assert {:error, :not_queued} == Container.dequeue_consumer(chest, self())
    end

    test "locations are correct", %{chest: chest} do
      assert Machine.locations(chest) == [{0, 0}]
    end

    test "processing locations claimed does nothing", %{chest: chest} do
      assert Machine.process_location_claimed(chest, {1, 0}, self()) == chest
    end

    test "rotate", %{chest: chest} do
      assert Machine.rotate(chest) == chest
    end

    test "start", %{chest: chest} do
      assert chest == Machine.start(chest)
    end

    test "put", %{chest: chest} do
      {:ok, chest} = Container.put(chest, {"stone", 2})
      assert %{"stone" => 2} == chest.storage
    end

    test "take", %{chest: chest} do
      assert {:error, :not_available} == Container.take(chest)
    end

    test "take with payload", %{chest: chest} do
      {:ok, chest} = Container.put(chest, {"stone", 2})
      {:ok, chest, payload} = Container.take(chest, {"stone", 2})
      assert %{} == chest.storage
      assert {"stone", 2} == payload
    end
  end
end
