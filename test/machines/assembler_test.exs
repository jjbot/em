defmodule EM.Machines.AssemblerTest do
  alias EM.Machines.{Assembler, Container, Machine}
  alias EM.Machines.Components.{Storage, TypedStorage}
  alias EM.Machines.Utils.Recipe

  use ExUnit.Case, async: true
  doctest EM.Machines.Assembler

  setup_all do
    {:ok, %{assembler: %Assembler{bl: {707, 707}}}}
  end

  describe "when just initialized" do
    setup arg do
      {:ok, arg}
    end

    test "defaults make sense", %{assembler: assembler} do
      assert assembler.recipe == nil
      assert assembler.status == :idle
      assert Storage.to_map(assembler.storage_in) == %{}
      assert Storage.to_map(assembler.storage_out) == %{}
      assert assembler.timer == nil
    end

    test "backup state is privided correctly", %{assembler: assembler} do
      assert [bl: {707, 707}, recipe: nil] == Machine.get_backup_state(assembler)
    end

    test "putting resources gives :no_recipe_set error", %{assembler: assembler} do
      assert Container.put(assembler, {"stone", 2}) == {:error, :no_recipe_set}
    end

    test "taking output gives :not_available error", %{assembler: assembler} do
      assert Container.take(assembler) == {:error, :not_available}
    end

    test "dequeue consumer gives error", %{assembler: assembler} do
      assert {:error, :not_queued} = Container.dequeue_consumer(assembler, self())
    end

    test "enqueue consumer add process to the queue", %{assembler: assembler} do
      {:ok, assembler} = Container.enqueue_consumer(assembler, self())
      assert :queue.member(self(), assembler.consumer_q)
    end

    test "checking if sufficient resources are present gives error", %{assembler: assembler} do
      assert Assembler.sufficient_resources_present?(assembler) == {:error, :no_recipe_set}
    end

    test "setting a recipe gives back empty resources", %{assembler: assembler} do
      recipe = %Recipe{
        resources_in: %{"iron plate" => 2},
        resource_out: {"iron gear wheel", 1},
        name: "gears",
        time: 500
      }

      {:ok, assembler, resources} = Assembler.set_recipe(assembler, recipe)
      assert resources == %{}
      assert assembler.recipe == recipe
    end

    test "doesn't have output available", %{assembler: assembler} do
      refute Assembler.product_available?(assembler)
    end

    test "rotating doesn't change anything", %{assembler: assembler} do
      assert Machine.rotate(assembler) == assembler
    end

    test "setting status works", %{assembler: assembler} do
      assembler = Assembler.set_status!(assembler, :waiting_for_resources)
      assert assembler.status == :waiting_for_resources
    end

    test "trying production gives no_recipe error", %{assembler: assembler} do
      assert Assembler.try_production(assembler) == assembler
    end

    test "process location claimed does nothing", %{assembler: assembler} do
      assert assembler == Machine.process_location_claimed(assembler, {10, 10}, self())
    end
  end

  describe "after setting a recipe" do
    setup %{assembler: assembler} do
      recipe = %Recipe{
        resources_in: %{"copper cable" => 3, "iron plate" => 1},
        resource_out: {"electronic circuit", 1},
        name: "electronic circuit",
        time: 500
      }

      {:ok, assembler, _resources} = Assembler.set_recipe(assembler, recipe)
      {:ok, %{assembler: assembler, recipe: recipe}}
    end

    test "checking if sufficient resources are present returns false", %{assembler: assembler} do
      refute Assembler.sufficient_resources_present?(assembler)
    end

    test "setting unneeded resources gives error", %{assembler: assembler} do
      assert Assembler.put(assembler, {"stone", 1}) == {:error, :type_not_supported}
    end

    test "setting needed resources succeeds", %{assembler: assembler} do
      {:ok, assembler} = Assembler.put(assembler, {"iron plate", 1})
      assert Storage.to_map(assembler.storage_in) == %{"iron plate" => 1}
    end

    test "setting needed resources triggers production", %{assembler: assembler} do
      {:ok, assembler} = Assembler.put(assembler, {"iron plate", 1})
      {:ok, assembler} = Assembler.put(assembler, {"iron plate", 1})
      assert Storage.to_map(assembler.storage_in) == %{"iron plate" => 2}
    end

    test "force putting resources works", %{assembler: assembler} do
      assembler = Assembler.put!(assembler, {"iron plate", 1})
      assert Storage.to_map(assembler.storage_in) == %{"iron plate" => 1}
    end

    test "adding sufficient resources makes machine ready for production", %{assembler: assembler} do
      {:ok, assembler} = Assembler.put(assembler, {"iron plate", 1})
      {:ok, assembler} = Assembler.put(assembler, {"copper cable", 3})
      assert assembler.status == :processing
    end

    test "is not ready for production", %{assembler: assembler} do
      assert {false, :no_resources} == Assembler.ready_for_production?(assembler)
    end

    test "putting the wrong type of output type in put_output! raises error", %{
      assembler: assembler
    } do
      assert_raise RuntimeError, fn -> Assembler.put_output!(assembler, {"stone", 1}) end
    end

    test "putting the wrong type of output type in put_output returns error", %{
      assembler: assembler
    } do
      assert {:error, :type_not_supported} == Assembler.put_output(assembler, {"stone", 1})
    end

    test "is not a transporter", %{assembler: assembler} do
      refute EM.Machines.Machine.is_transporter(assembler)
    end
  end

  describe "with a process queued" do
    setup %{assembler: assembler} do
      {:ok, assembler} = Assembler.enqueue_consumer(assembler, self())
      {:ok, %{assembler: assembler}}
    end

    test "dequeue removes process", %{assembler: assembler} do
      {:ok, assembler} = Assembler.dequeue_consumer(assembler, self())
      refute :queue.member(self(), assembler.consumer_q)
    end
  end

  describe "when assembler is full" do
    setup _ do
      recipe = %Recipe{
        resources_in: %{"iron plate" => 1},
        resource_out: {"iron stick", 1},
        name: "iron stick",
        time: 10
      }

      storage_in = %TypedStorage{slots: %{"iron plate" => 100}, content: %{"iron plate" => 100}}
      storage_out = %TypedStorage{slots: %{"iron stick" => 100}, content: %{"iron stick" => 100}}

      assembler = %Assembler{
        recipe: recipe,
        status: :full,
        storage_in: storage_in,
        storage_out: storage_out
      }

      %{assembler: assembler}
    end

    test "production does not start when asked", %{assembler: assembler} do
      assembler = Assembler.try_production(assembler)
      assert assembler.timer == nil
      assert assembler.status == :full
    end

    test "enqueing to provide works", %{assembler: assembler} do
      assembler =
        assembler
        |> Container.enqueue_provider(self(), {"iron plate", 1})
        |> elem(1)

      assert :queue.member(self(), assembler.provider_q)
    end

    test "dequeing to provide works", %{assembler: assembler} do
      assembler =
        assembler
        |> Container.enqueue_provider(self(), {"iron plate", 1})
        |> elem(1)
        |> Container.dequeue_provider(self())
        |> elem(1)

      refute :queue.member(self(), assembler.provider_q)
    end

    test "putting output does not work", %{assembler: assembler} do
      {:ok, assembler} = Assembler.put_output(assembler, {"iron stick", 2})
      assert assembler.status == :full
    end
  end

  describe "when an assembler is constructing" do
    setup _ do
      recipe = %Recipe{
        resources_in: %{"iron plate" => 1},
        resource_out: {"iron stick", 1},
        name: "iron stick",
        time: 10_000
      }

      storage_in = %TypedStorage{slots: %{"iron plate" => 100}, content: %{"iron plate" => 100}}
      storage_out = %TypedStorage{slots: %{"iron stick" => 100}, content: %{"iron stick" => 0}}

      assembler = %Assembler{
        recipe: recipe,
        status: :idle,
        storage_in: storage_in,
        storage_out: storage_out
      }

      %{assembler: assembler}
    end

    test "changing the recipe resets the timer", %{assembler: assembler} do
      assembler = Assembler.start(assembler)
      assert assembler.timer != nil

      {:ok, assembler, _} = Assembler.set_recipe(assembler, nil)
      assert assembler.timer == nil
    end
  end
end
