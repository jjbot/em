defmodule EM.Machines.GeneratorChestServerTest do
  @moduledoc false

  use ExUnit.Case, async: true

  alias EM.Machines.ContainerServer
  alias EM.Machines.Components.SlotStorage
  alias EM.Machines.{GeneratorChest, GeneratorChestServer}
  alias EM.Machines.MachineServer

  describe "after init" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, pid, _name} = GeneratorChestServer.start_link(%GeneratorChest{bl: {x, y}})
      %{chest: pid}
    end

    test "contains all the elements we expect", %{chest: chest} do
      state = :sys.get_state(chest)
      assert state.storage == %{}
      assert :queue.is_empty(state.consumer_q)
      assert state.timers == %{}
    end

    test "returns the right locations", %{chest: chest} do
      state = :sys.get_state(chest)
      assert MachineServer.locations(chest) == [state.bl]
    end

    test "returns the right bottom left corner", %{chest: chest} do
      state = :sys.get_state(chest)
      assert MachineServer.bl(chest) == state.bl
    end

    test "adding a timer gives input at the right interval", %{chest: chest} do
      # Generate 2 stone every half a second
      GeneratorChestServer.add_generator(chest, {"stone", 2}, 500)

      :timer.sleep(600)
      state = :sys.get_state(chest)
      assert state.storage == %{"stone" => 2}
    end

    test "check if storage server is implemented properly", %{chest: chest} do
      # Generate 2 stone every half a second
      GeneratorChestServer.add_generator(chest, {"stone", 2}, 500)

      :timer.sleep(600)
      assert {:ok, {"stone", 1}} == ContainerServer.take(chest)
    end

    test "check if machine server is implemented properly", %{chest: chest} do
      assert {1, 1} == MachineServer.size(chest)
    end
  end

  describe "when full" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      gc = %GeneratorChest{bl: {x, y}, storage: %SlotStorage{slots: 1, content: %{"stone" => 50}}}
      {:ok, pid, _name} = GeneratorChestServer.start_link(gc)
      %{chest: pid}
    end

    test "enqueuing provider works", %{chest: chest} do
      ContainerServer.enqueue_provider(chest, {"stone", 10})
      state = :sys.get_state(chest)
      assert :queue.member(self(), state.provider_q)
    end

    test "dequeueing an enqueued provider works", %{chest: chest} do
      ContainerServer.enqueue_provider(chest, {"stone", 10})
      ContainerServer.dequeue_provider(chest)
      state = :sys.get_state(chest)
      refute :queue.member(self(), state.provider_q)
    end
  end
end
