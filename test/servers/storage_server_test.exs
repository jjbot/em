defmodule EM.Servers.StorageServerTest do
  use ExUnit.Case, async: true

  alias EM.Machines.{
    Assembler,
    AssemblerServer,
    Chest,
    ChestServer,
    ContainerServer,
    ConstructorServer
  }

  alias EM.Machines.Utils.Recipe

  describe "with an assembler" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)
      {:ok, assembler_pid, _name} = AssemblerServer.start_link(%Assembler{bl: {x, y}})

      recipe = %Recipe{
        resources_in: %{"iron plate" => 2},
        resource_out: {"iron gear wheel", 1},
        time: 5,
        name: "gear"
      }

      ConstructorServer.set_recipe(assembler_pid, recipe)
      ContainerServer.put(assembler_pid, {"iron plate", 10})
      :timer.sleep(25)

      %{assembler: assembler_pid}
    end

    test "putting right resources works", %{assembler: assembler} do
      assert :ok = ContainerServer.put(assembler, {"iron plate", 1})
    end

    test "putting wrong resources gives error", %{assembler: assembler} do
      assert {:error, :type_not_supported} = ContainerServer.put(assembler, {"stone", 1})
    end

    test "taking blindly gives right resources", %{assembler: assembler} do
      {:ok, {type, amount}} = ContainerServer.take(assembler)
      assert type == "iron gear wheel"
      assert amount == 1
    end

    test "taking a specific amount of resources works", %{assembler: assembler} do
      :timer.sleep(12)
      {:ok, {type, amount}} = ContainerServer.take(assembler, {"iron gear wheel", 2})
      assert type == "iron gear wheel"
      assert amount == 2
    end

    test "dequeing consumer gives error", %{assembler: assembler_pid} do
      assert {:error, :not_queued} = ContainerServer.dequeue_consumer(assembler_pid)
    end

    test "enqueueing consumer causes notify_material_available call", %{assembler: assembler_pid} do
      :ok = ContainerServer.enqueue_consumer(assembler_pid)
      state = :sys.get_state(assembler_pid)
      assert :queue.is_empty(state.consumer_q)
    end
  end

  describe "with a chest" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, chest_pid, _name} =
        ChestServer.start_link(%Chest{storage: %{"iron plate" => 2}, bl: {x, y}})

      %{chest: chest_pid}
    end

    test "putting new resources works", %{chest: chest} do
      ContainerServer.put(chest, {"stone", 2})
      state = :sys.get_state(chest)
      assert state.storage == %{"stone" => 2, "iron plate" => 2}
    end

    test "putting the same resources adds them to the pile", %{chest: chest} do
      ContainerServer.put(chest, {"iron plate", 2})
      state = :sys.get_state(chest)
      assert state.storage == %{"iron plate" => 4}
    end

    test "taking blindly gives right resources", %{chest: chest} do
      {:ok, {type, amount}} = ContainerServer.take(chest)
      assert type == "iron plate"
      assert amount == 1
    end

    test "taking a specific amount of storage resources works", %{chest: chest} do
      {:ok, {type, amount}} = ContainerServer.take(chest, {"iron plate", 2})
      assert type == "iron plate"
      assert amount == 2
    end

    test "taking non stored resources gives error", %{chest: chest} do
      assert {:error, :not_available} = ContainerServer.take(chest, {"stone", 4})
    end

    test "dequeing consumer gives error", %{chest: chest_pid} do
      assert {:error, :not_queued} = ContainerServer.dequeue_consumer(chest_pid)
    end

    test "enqueueing consumer causes notify_material_available call", %{chest: chest_pid} do
      # FIXME: currently not implemented
      :ok = ContainerServer.enqueue_consumer(chest_pid)
      state = :sys.get_state(chest_pid)
      assert :queue.is_empty(state.consumer_q)
    end

    test "enqueueing consumer on empty chest queues process", %{chest: chest_pid} do
      ContainerServer.take(chest_pid, {"iron plate", 2})
      :ok = ContainerServer.enqueue_consumer(chest_pid)
      state = :sys.get_state(chest_pid)
      assert :queue.member(self(), state.consumer_q)
    end

    test "dequeueing when in consumer queue works", %{chest: chest_pid} do
      ContainerServer.take(chest_pid, {"iron plate", 2})
      :ok = ContainerServer.enqueue_consumer(chest_pid)
      :ok = ContainerServer.dequeue_consumer(chest_pid)
      state = :sys.get_state(chest_pid)
      refute :queue.member(self(), state.consumer_q)
    end
  end

  describe "with an empty assembler" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, assembler_pid, _name} = AssemblerServer.start_link(%Assembler{bl: {x, y}})

      recipe = %Recipe{
        resources_in: %{"iron plate" => 2},
        resource_out: {"iron gear wheel", 1},
        time: 5,
        name: "gear"
      }

      ConstructorServer.set_recipe(assembler_pid, recipe)

      %{assembler: assembler_pid}
    end

    test "enqueueing consumer on empty assembler queues process", %{assembler: assembler_pid} do
      :ok = ContainerServer.enqueue_consumer(assembler_pid)
      state = :sys.get_state(assembler_pid)
      assert :queue.member(self(), state.consumer_q)
    end

    test "dequeueing consumer when in queue works", %{assembler: assembler_pid} do
      ContainerServer.take(assembler_pid)
      :ok = ContainerServer.enqueue_consumer(assembler_pid)
      :ok = ContainerServer.dequeue_consumer(assembler_pid)
      state = :sys.get_state(assembler_pid)
      refute :queue.member(self(), state.consumer_q)
    end
  end
end
