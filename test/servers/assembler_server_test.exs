defmodule EM.Servers.AssemblerServerTest do
  use ExUnit.Case, async: true

  alias EM.Machines.{
    Assembler,
    AssemblerServer,
    Chest,
    ChestServer,
    ConstructorServer,
    ContainerServer,
    Inserter,
    InserterServer
  }

  alias EM.Machines.MachineServer
  alias EM.Machines.Components.Storage
  alias EM.Machines.Utils.Recipe

  describe "after init" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, assembler_pid, _assembler_name} = AssemblerServer.start_link(%Assembler{bl: {x, y}})

      recipe = %Recipe{
        resources_in: %{"iron plate" => 2},
        resource_out: {"iron gear wheel", 1},
        time: 10,
        name: "gear"
      }

      %{assembler: assembler_pid, recipe: recipe}
    end

    test "adding resources without blueprint fails", %{assembler: assembler} do
      assert ContainerServer.put(assembler, {"stone", 1}) == {:error, :no_recipe_set}
    end

    test "taking resources fails as expected", %{assembler: assembler} do
      assert ContainerServer.take(assembler) == {:error, :not_available}
    end

    test "taking particular resources fails as expected", %{assembler: assembler} do
      assert ContainerServer.take(assembler, {"stone", 1}) == {:error, :not_available}
    end

    test "setting recipe works and sets correct type in storage_out", %{
      assembler: assembler,
      recipe: recipe
    } do
      assert ConstructorServer.set_recipe(assembler, recipe) == {:ok, %{}}

      state = :sys.get_state(assembler)
      assert Storage.to_map(state.storage_out) == %{}
    end

    test "starting without recipe doesn't do anything", %{assembler: assembler} do
      MachineServer.start(assembler)
      state = :sys.get_state(assembler)
      assert state.status == :idle
    end
  end

  describe "after setting recipe" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, assembler_pid, _assembler_name} = AssemblerServer.start_link(%Assembler{bl: {x, y}})

      recipe = %Recipe{
        resources_in: %{"iron plate" => 2},
        resource_out: {"iron gear wheel", 1},
        time: 10,
        name: "gear"
      }

      ConstructorServer.set_recipe(assembler_pid, recipe)

      %{assembler: assembler_pid, recipe: recipe}
    end

    test "adding correct resources works", %{assembler: assembler} do
      assert ContainerServer.put(assembler, {"iron plate", 1}) == :ok
    end

    test "adding incorrect resources fails", %{assembler: assembler} do
      assert ContainerServer.put(assembler, {"stone", 1}) == {:error, :type_not_supported}
    end

    test "taking resources indicates non are present", %{assembler: assembler} do
      assert ContainerServer.take(assembler) == {:error, :not_available}
    end

    test "changing recipe gives back stored resources", %{assembler: assembler} do
      ContainerServer.put(assembler, {"iron plate", 1})

      cc_recipe = %Recipe{
        resources_in: %{"copper plate" => 1},
        resource_out: {"copper cable", 2},
        time: 10,
        name: "copper cable"
      }

      assert ConstructorServer.set_recipe(assembler, cc_recipe) == {:ok, %{"iron plate" => 1}}

      state = :sys.get_state(assembler)
      assert Storage.to_map(state.storage_out) == %{}
    end

    test "setting the same recipe changes nothing", %{assembler: assembler, recipe: recipe} do
      assert {:ok, %{}} == ConstructorServer.set_recipe(assembler, recipe)
    end

    test "adding sufficient resources starts production", %{assembler: assembler} do
      ContainerServer.put(assembler, {"iron plate", 2})
      state = :sys.get_state(assembler)
      assert state.status == :processing
      :timer.sleep(20)
      state = :sys.get_state(assembler)
      assert state.status == :waiting_for_resources
      assert Storage.to_map(state.storage_out) == %{"iron gear wheel" => 1}
    end

    test "adding more initial resources gives more production", %{assembler: assembler} do
      ContainerServer.put(assembler, {"iron plate", 6})
      :timer.sleep(100)
      state = :sys.get_state(assembler)
      assert state.status == :waiting_for_resources
      assert Storage.to_map(state.storage_out) == %{"iron gear wheel" => 3}
    end

    test "adding resoures while producing gives more production", %{assembler: assembler} do
      ContainerServer.put(assembler, {"iron plate", 2})
      :timer.sleep(50)
      ContainerServer.put(assembler, {"iron plate", 2})
      :timer.sleep(200)
      state = :sys.get_state(assembler)
      assert state.status == :waiting_for_resources
      assert Storage.to_map(state.storage_out) == %{"iron gear wheel" => 2}
    end

    test "after production taking recources by type works", %{assembler: assembler} do
      ContainerServer.put(assembler, {"iron plate", 4})
      :timer.sleep(60)

      assert {:ok, {"iron gear wheel", 2}} ==
               ContainerServer.take(assembler, {"iron gear wheel", 2})
    end
  end

  describe "basic gear production" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, assembler_pid, _name} = AssemblerServer.start_link(%Assembler{bl: {x + 2, y}})

      recipe = %Recipe{
        resources_in: %{"iron plate" => 2},
        resource_out: {"iron gear wheel", 1},
        time: 10,
        name: "gear"
      }

      ConstructorServer.set_recipe(assembler_pid, recipe)

      {:ok, source_chest_pid, _name} =
        ChestServer.start_link(%Chest{bl: {x, y + 1}, storage: %{"iron plate" => 10}})

      {:ok, target_chest_pid, _name} = ChestServer.start_link(%Chest{bl: {x + 6, y + 1}})

      source_inserter =
        %Inserter{bl: {x + 1, y + 1}, swing_time: 5}
        |> Inserter.set_source(source_chest_pid)
        |> Inserter.set_target(assembler_pid)
        |> Inserter.set_orientation(:east)
        |> Map.put(:target_orientation, :west)

      target_inserter =
        %Inserter{bl: {x + 5, y + 1}, swing_time: 5}
        |> Inserter.set_source(assembler_pid)
        |> Inserter.set_target(target_chest_pid)
        |> Inserter.set_orientation(:east)
        |> Map.put(:target_orientation, :west)

      {:ok, source_inserter_pid, _name} = InserterServer.start_link(source_inserter)
      {:ok, target_inserter_pid, _name} = InserterServer.start_link(target_inserter)

      machines = %{
        assembler: assembler_pid,
        source_chest: source_chest_pid,
        target_chest: target_chest_pid,
        source_inserter: source_inserter_pid,
        target_inserter: target_inserter_pid
      }

      machines
    end

    test "works", machines do
      InserterServer.swing(machines.source_inserter)

      :timer.sleep(200)

      assembler_state = :sys.get_state(machines.assembler)
      assert Storage.to_map(assembler_state.storage_out) == %{"iron gear wheel" => 5}

      InserterServer.swing(machines.target_inserter)

      :timer.sleep(200)
      target_chest_state = :sys.get_state(machines.target_chest)

      assert Storage.to_map(target_chest_state.storage) == %{"iron gear wheel" => 5}
    end

    test "setting recipe to nil cancels timer", %{assembler: assembler} do
      :ok = ContainerServer.put(assembler, {"iron plate", 4})
      :timer.sleep(50)
      {:ok, resources} = ConstructorServer.set_recipe(assembler, nil)
      assert resources == %{"iron gear wheel" => 2}

      state = :sys.get_state(assembler)
      assert state.timer == nil
    end
  end

  describe "gear production with queued inserters" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, source_chest_pid, _name} = ChestServer.start_link(%Chest{bl: {x, y + 1}})
      {:ok, target_chest_pid, _name} = ChestServer.start_link(%Chest{bl: {x + 6, y + 1}})

      recipe = %Recipe{
        resources_in: %{"iron plate" => 2},
        resource_out: {"iron gear wheel", 1},
        name: "gear",
        time: 10
      }

      {:ok, assembler_pid, _name} = AssemblerServer.start_link(%Assembler{bl: {x + 2, y}})
      ConstructorServer.set_recipe(assembler_pid, recipe)

      {:ok, source_inserter_pid, _name} =
        InserterServer.start_link(%Inserter{
          bl: {x + 1, y + 1},
          source: source_chest_pid,
          target: assembler_pid,
          swing_time: 10
        })

      {:ok, target_inserter_pid, _name} =
        InserterServer.start_link(%Inserter{
          bl: {x + 5, y + 1},
          source: assembler_pid,
          target: target_chest_pid,
          swing_time: 10
        })

      {:ok,
       %{
         source_chest: source_chest_pid,
         target_chest: target_chest_pid,
         assembler: assembler_pid,
         source_inserter: source_inserter_pid,
         target_inserter: target_inserter_pid
       }}
    end

    test "works", state do
      send(state.source_inserter, {:swung, :south})
      send(state.target_inserter, {:swung, :south})

      :timer.sleep(50)
      ContainerServer.put(state.source_chest, {"iron plate", 4})
      :timer.sleep(400)

      source_chest_state = :sys.get_state(state.source_chest)
      assert Storage.to_map(source_chest_state.storage) == %{}

      target_chest_state = :sys.get_state(state.target_chest)
      assert Storage.to_map(target_chest_state.storage) == %{"iron gear wheel" => 2}
    end
  end
end
