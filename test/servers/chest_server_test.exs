defmodule EM.Machines.ChestServerTest do
  @moduledoc false

  use ExUnit.Case, async: true

  require Logger

  alias EM.Machines.ContainerServer
  alias EM.Machines.Components.{Storage, SlotStorage}
  alias EM.Machines.{Chest, ChestServer}

  describe "when initializing" do
    test "new by default gives empty chest" do
      {:ok, chest_pid} = GenServer.start(ChestServer, %Chest{})
      assert Storage.to_map(:sys.get_state(chest_pid).storage) == %{}
    end

    test "new with storage sets storage correctly" do
      storage = %{"gear" => 10, "metal_plate" => 100}
      {:ok, chest_pid} = GenServer.start(ChestServer, %Chest{storage: storage})
      assert :sys.get_state(chest_pid).storage == storage
    end
  end

  describe "when chest is empty" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, chest_pid, _name} = ChestServer.start_link(%Chest{bl: {x, y}})
      %{chest: chest_pid}
    end

    test "taking without specifying type gives :emtpy", %{chest: chest} do
      response = ContainerServer.take(chest)
      assert response == {:error, :not_available}
    end

    test "taking with specifying type and amount gives :empty", %{chest: chest} do
      response = ContainerServer.take(chest, {"gear", 100})
      assert response == {:error, :not_available}
    end
  end

  describe "basic interaction" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, chest_pid, _name} =
        ChestServer.start_link(%Chest{bl: {x, y}, storage: %{"gear" => 5}})

      %{chest: chest_pid}
    end

    test "adding new material gives new entry in storage", %{chest: chest_pid} do
      :ok = ContainerServer.put(chest_pid, {"metal_plate", 10})
      assert :sys.get_state(chest_pid).storage == %{"gear" => 5, "metal_plate" => 10}
    end

    test "adding existing material adds to the pile", %{chest: chest_pid} do
      :ok = ContainerServer.put(chest_pid, {"gear", 10})
      assert :sys.get_state(chest_pid).storage == %{"gear" => 15}
    end

    test "taking without specifying type gives one from first available", %{chest: chest_pid} do
      response = ContainerServer.take(chest_pid)
      assert response == {:ok, {"gear", 1}}
      assert :sys.get_state(chest_pid).storage["gear"] == 4
    end

    test "taking specific available amount works", %{chest: chest_pid} do
      taken = ContainerServer.take(chest_pid, {"gear", 3})
      assert taken == {:ok, {"gear", 3}}
      assert :sys.get_state(chest_pid).storage["gear"] == 2
    end
  end

  describe "when full" do
    setup _ do
      {:ok, chest_pid, _name} = ChestServer.start_link(%Chest{storage: %SlotStorage{slots: 1}})
      ContainerServer.put(chest_pid, {"iron plate", 100})
      %{chest: chest_pid}
    end

    test "add more resources gives error", %{chest: chest} do
      assert {:error, :not_enough_space} == ContainerServer.put(chest, {"iron plate", 1})
    end

    test "enqueing provider works", %{chest: chest} do
      ContainerServer.enqueue_provider(chest, {"iron plate", 2})
      state = :sys.get_state(chest)
      assert :queue.member(self(), state.provider_q)
    end

    test "dequeuing an enqueued provider works", %{chest: chest} do
      ContainerServer.enqueue_provider(chest, {"iron plate", 2})
      ContainerServer.dequeue_provider(chest)
      state = :sys.get_state(chest)
      refute :queue.member(self(), state.provider_q)
    end

    test "when space becomes available, queued process is notified", %{chest: chest} do
      ContainerServer.enqueue_provider(chest, {"iron plate", 2})
      ContainerServer.take(chest, {"iron plate", 10})
      assert_receive {:"$gen_cast", :space_available}
    end
  end
end
