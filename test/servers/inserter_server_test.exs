defmodule EM.Machines.InserterServerTest do
  use ExUnit.Case, async: true

  require Logger

  alias EM.Machines.Components.{Storage, SlotStorage}
  alias EM.Machines.MachineServer
  alias EM.Machines.ContainerServer
  alias EM.Machines.{Chest, Inserter, InserterServer, ChestServer}

  describe "after init" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, inserter_pid, _name} =
        InserterServer.start_link(%Inserter{bl: {x + 1, y + 1}, swing_time: 10})

      %{inserter: inserter_pid}
    end

    test "defaults are correct", %{inserter: inserter} do
      state = :sys.get_state(inserter)

      assert state.source == nil
      assert state.target == nil
      assert state.capacity == 1
      assert state.payload == {nil, nil}
      assert state.target_orientation == :north
      assert state.current_orientation == :south
      assert state.status == :idle
      assert state.swing_target == nil
      assert state.swing_time == 10
      assert state.timer == nil
    end

    test "starting work keeps orientation", %{inserter: inserter} do
      InserterServer.start_work(inserter)
      state = :sys.get_state(inserter)
      assert state.current_orientation == :south
    end

    test "inserter has no payload", %{inserter: inserter} do
      refute InserterServer.has_payload?(inserter)
    end

    test "target orientation is default", %{inserter: inserter} do
      assert InserterServer.get_target_orientation(inserter) == :north
    end

    test "setting source works", %{inserter: inserter} do
      InserterServer.set_source(inserter, self())
      state = :sys.get_state(inserter)
      assert state.source == self()
    end

    test "setting target works", %{inserter: inserter} do
      InserterServer.set_target(inserter, self())
      state = :sys.get_state(inserter)
      assert state.target == self()
    end

    test "rotating removes set source and target", %{inserter: inserter} do
      InserterServer.set_source(inserter, self())
      InserterServer.set_target(inserter, self())
      MachineServer.rotate(inserter)
      state = :sys.get_state(inserter)
      assert state.source == nil
      assert state.target == nil
    end

    test "show payload gives correct result", %{inserter: inserter} do
      assert InserterServer.show_payload(inserter) == {nil, nil}
    end

    test "swing gets it into opposite orientation", %{inserter: inserter} do
      InserterServer.swing(inserter)
      :timer.sleep(40)
      state = :sys.get_state(inserter)
      assert state.current_orientation == :north
    end

    test "arriving at unset source sets status to idle", %{inserter: inserter} do
      send(inserter, {:swung, :south})
      state = :sys.get_state(inserter)
      assert state.status == :idle
    end

    test "is a transporter", %{inserter: inserter} do
      assert MachineServer.is_transporter(inserter)
    end
  end

  describe "chest inserter chest" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, source, _name} =
        ChestServer.start_link(%Chest{bl: {x, y + 1}, storage: %{"stone" => 2}})

      {:ok, target, _name} = ChestServer.start_link(%Chest{bl: {x + 2, y + 1}})

      {:ok, inserter_pid, _name} =
        InserterServer.start_link(%Inserter{bl: {x + 1, y + 1}, swing_time: 10})

      InserterServer.set_source(inserter_pid, source)
      InserterServer.set_target(inserter_pid, target)
      %{source: source, target: target, inserter: inserter_pid}
    end

    test "works as expected", %{source: source, target: target, inserter: inserter} do
      InserterServer.start_work(inserter)
      :timer.sleep(100)
      source_state = :sys.get_state(source)
      target_state = :sys.get_state(target)
      assert source_state.storage == %{}
      assert Storage.to_map(target_state.storage) == %{"stone" => 2}
    end

    test "when someone else takes resources, functions correctly", %{
      source: source,
      inserter: inserter
    } do
      # Empty the input storage
      ContainerServer.take(source, {"stone", 2})
      GenServer.cast(inserter, :material_available)
      inserter_state = :sys.get_state(inserter)
      assert inserter_state.status == :waiting
    end
  end

  describe "fill chest until full" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, source, _name} =
        ChestServer.start_link(%Chest{
          bl: {x, y + 1},
          storage: %SlotStorage{slots: 2, content: %{"stone" => 100}}
        })

      {:ok, target, _name} =
        ChestServer.start_link(%Chest{
          bl: {x + 2, y + 1},
          storage: %SlotStorage{slots: 1, content: %{"stone" => 49}}
        })

      {:ok, inserter, _name} =
        InserterServer.start_link(%Inserter{bl: {x + 1, y + 1}, swing_time: 10})

      InserterServer.set_source(inserter, source)
      InserterServer.set_target(inserter, target)
      %{source: source, target: target, inserter: inserter}
    end

    test "works as expected", %{source: source, target: target, inserter: inserter} do
      InserterServer.start_work(inserter)
      :timer.sleep(100)

      source_state = :sys.get_state(source)
      target_state = :sys.get_state(target)
      inserter_state = :sys.get_state(inserter)

      # Inserter should have taken 2 resources
      assert Storage.to_map(source_state.storage) == %{"stone" => 98}

      # One material should have been added
      assert Storage.to_map(target_state.storage) == %{"stone" => 50}

      # Inserter should now be waiting
      assert inserter_state.status == :waiting

      # Inserter should be holding one stone as payload
      assert inserter_state.payload == {"stone", 1}
    end
  end
end
