defmodule EM.Machines.VanishingChestServerTest do
  @moduledoc false

  use ExUnit.Case, async: true

  alias EM.Machines.ContainerServer
  alias EM.Machines.{VanishingChest, VanishingChestServer}
  alias EM.Machines.{MachineServer, ContainerServer}
  alias EM.Machines.Components.SlotStorage

  describe "after init" do
    setup(_) do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, pid1, _name} = VanishingChestServer.start_link(%VanishingChest{bl: {x, y}})
      {:ok, pid2, _name} = VanishingChestServer.start_link(%VanishingChest{bl: {x + 9, y + 9}})

      VanishingChestServer.set_opposite(pid1, pid2)
      VanishingChestServer.set_opposite(pid2, pid1)

      %{chest1: pid1, chest2: pid2}
    end

    test "contains all the elements we expect", %{chest1: chest1, chest2: chest2} do
      state1 = :sys.get_state(chest1)

      assert %{} == state1.storage
      assert :queue.is_empty(state1.consumer_q)
      assert chest2 == state1.opposite
    end

    test "returns the right locations", %{chest1: chest} do
      state = :sys.get_state(chest)
      assert MachineServer.locations(chest) == [state.bl]
    end

    test "returns the right bottom left corner", %{chest1: chest} do
      state = :sys.get_state(chest)
      assert MachineServer.bl(chest) == state.bl
    end

    test "adding resources in one adds them to the other", %{chest1: chest1, chest2: chest2} do
      ContainerServer.put(chest1, {"stone", 1})
      :timer.sleep(50)
      state2 = :sys.get_state(chest2)
      assert %{"stone" => 1} == state2.storage

      ContainerServer.put(chest2, {"steel", 1})
      :timer.sleep(50)
      state1 = :sys.get_state(chest1)
      assert %{"steel" => 1} == state1.storage
    end

    test "check if take without resources works", %{chest1: chest1} do
      assert {:error, :not_available} == ContainerServer.take(chest1)
    end

    test "check if take with resources works", %{chest1: chest1, chest2: chest2} do
      ContainerServer.put(chest1, {"stone", 1})
      :timer.sleep(50)
      assert {:ok, {"stone", 1}} == ContainerServer.take(chest2, {"stone", 1})
    end

    test "check if machine server is implemented properly", %{chest1: chest} do
      assert {1, 1} == MachineServer.size(chest)
    end
  end

  # FIXME: test pass, but does not take into account it should actually be queued
  # on the opposite side.
  describe "when full" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      gc1 = %VanishingChest{
        bl: {x, y},
        storage: %SlotStorage{slots: 1, content: %{"stone" => 50}}
      }

      gc2 = %VanishingChest{
        bl: {x + 1, y + 1},
        storage: %SlotStorage{slots: 1, content: %{"stone" => 50}}
      }

      {:ok, pid1, _name} = VanishingChestServer.start_link(gc1)
      {:ok, pid2, _name} = VanishingChestServer.start_link(gc2)
      VanishingChestServer.set_opposite(pid1, pid2)
      VanishingChestServer.set_opposite(pid2, pid1)
      %{chest1: pid1, chest2: pid2}
    end

    test "enqueuing provider works", %{chest1: chest} do
      ContainerServer.enqueue_provider(chest, {"stone", 10})
      state = :sys.get_state(chest)
      assert :queue.member(self(), state.provider_q)
    end

    test "dequeueing an enqueued provider works", %{chest1: chest} do
      ContainerServer.enqueue_provider(chest, {"stone", 10})
      ContainerServer.dequeue_provider(chest)
      state = :sys.get_state(chest)
      refute :queue.member(self(), state.provider_q)
    end
  end
end
