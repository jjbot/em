defmodule EM.Servers.TestMachineServer do
  use ExUnit.Case, async: true

  alias EM.Machines.{
    Assembler,
    AssemblerServer,
    Chest,
    ChestServer,
    Inserter,
    InserterServer,
    MachineServer
  }

  alias EM.World.Location
  alias EM.Machines.Machine

  doctest EM.Machines.Machine

  describe "with all machines" do
    setup _ do
      x = :rand.uniform(1_000_000)
      y = :rand.uniform(1_000_000)

      {:ok, assembler, _name} = AssemblerServer.start_link(%Assembler{bl: {x - 3, y - 3}})
      {:ok, chest, _name} = ChestServer.start_link(%Chest{bl: {x, y + 2}})
      {:ok, inserter, _name} = InserterServer.start_link(%Inserter{bl: {x, y + 3}})

      %{assembler: assembler, chest: chest, inserter: inserter}
    end

    test "size works for assembler", %{assembler: assembler} do
      assert {3, 3} == MachineServer.size(assembler)
    end

    test "size works for chest", %{chest: chest} do
      assert {1, 1} == MachineServer.size(chest)
    end

    test "size works for inserter", %{inserter: inserter} do
      assert {1, 1} == MachineServer.size(inserter)
    end

    test "rotate works for inserter", %{inserter: inserter} do
      MachineServer.rotate(inserter)
      state = :sys.get_state(inserter)
      assert state.target_orientation == :east
    end

    test "bl works for assembler", %{assembler: assembler} do
      state = :sys.get_state(assembler)
      assert state.bl == MachineServer.bl(assembler)
    end

    test "bl works for chest", %{chest: chest} do
      state = :sys.get_state(chest)
      assert state.bl == MachineServer.bl(chest)
    end

    test "bl works for inserer", %{inserter: inserter} do
      state = :sys.get_state(inserter)
      assert state.bl == MachineServer.bl(inserter)
    end

    test "locations works for assembler", %{assembler: assembler} do
      state = :sys.get_state(assembler)

      assert MachineServer.locations(assembler) == [
               Location.add({0, 0}, state.bl),
               Location.add({0, 1}, state.bl),
               Location.add({0, 2}, state.bl),
               Location.add({1, 0}, state.bl),
               Location.add({1, 1}, state.bl),
               Location.add({1, 2}, state.bl),
               Location.add({2, 0}, state.bl),
               Location.add({2, 1}, state.bl),
               Location.add({2, 2}, state.bl)
             ]
    end

    test "locations works for chest", %{chest: chest} do
      state = :sys.get_state(chest)
      assert MachineServer.locations(chest) == [state.bl]
    end

    test "locations works for inserter", %{inserter: inserter} do
      state = :sys.get_state(inserter)
      assert MachineServer.locations(inserter) == [state.bl]
    end
  end
end
