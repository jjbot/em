FROM fedora:latest

run dnf -y upgrade --refresh
RUN dnf -y install ncurses ncurses-libs ncurses-compat-libs zlib glibc-common glibc-locale-source glibc-langpack-en

ENV LANG=en_US.UTF-8
RUN localedef --quiet --force -i en_US -f UTF-8 en_US.UTF-8

COPY _build/prod/prod-0.6.0.tar.gz /

RUN tar xvzf prod-0.6.0.tar.gz

CMD ["bin/prod", "start_iex"]

