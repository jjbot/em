import Config

config :em, tile_size: {10, 10}

if Mix.env() != :prod do
  config :git_hooks,
    verbose: true,
    hooks: [
      pre_push: [
        tasks: [
          "mix clean",
          # "mix compile --warnings-as-errors",
          "mix format --check-formatted",
          # "mix credo --strict",
          # "mix doctor --summary",
          "mix test"
        ]
      ]
    ]
end
